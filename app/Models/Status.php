<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 03/11/17
 * Time: 09:35 ص
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Collection;

class Status extends BaseModel
{
    protected $table = "status";

    const active = "active";
    const deactivate = "deactivate";
    const canceled = "canceled";
    const approved = "approved";
    const pending = "pending";

    /**
     * @return Collection
     */
    public static function asList()
    {
        return self::pluck("name", "id");
    }

    public static function getIdForStatus($name)
    {
        return @self::findByName($name)->id;
    }

    public function getStatusForId($id)
    {
        return @self::find($id)->name;
    }

    public function findByName($name)
    {
        return @self::whereName($name)->first();
    }

    public function getById($id)
    {

    }
}