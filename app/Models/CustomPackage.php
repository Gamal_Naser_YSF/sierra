<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/12/17
 * Time: 10:58 م
 */

namespace App\Models;


use App\Services\Notify;
use Illuminate\Http\Request;

class CustomPackage extends BaseModel
{
    protected $table = "custom_packages";

    public static function createNewOne(Request $request, $getUser)
    {
        $data = $request->only("departure_date", "hotel_id", "airlines", "adults", "Children", "Infant");
        $data["user_id"] = $getUser->id;
        $row = self::quickSave($data);
        Notify::notifyAdminAboutNewCuastomPackageCreated($row);
        return $row;
    }

    public function makeItDone()
    {
        $this->is_done = 1;
        $this->save();
    }
  public function changeStatus($s)
    {
        $this->is_done = $s;
        $this->save();
    }

    public static function filter(Request $request)
    {
        $self = (new static);
        if (isset($request->i))
            $self = $self->where("id", $request->i);

        return $self->with("user")->with("hotel")->with("getAirlines")->orderBy("created_at", "desc");

    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotels::class);
    }

    public function getAirlines()
    {
        return $this->belongsTo(Airlines::class, "airlines");
    }
}