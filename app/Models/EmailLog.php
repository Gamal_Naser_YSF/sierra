<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/03/18
 * Time: 09:07 م
 */

namespace App\Models;

use App\Services\Email;


/**
 * @property mixed subject
 * @property mixed user_email
 * @property mixed user_name
 * @property mixed data
 * @property mixed status
 * @property mixed template
 */
class EmailLog extends BaseModel
{
    const fails = 0;
    const sent = 1;
    protected $table = "emails";
    private static $statuses = ["0" => "fail", "1" => "sent"];

    public static function filter($request)
    {
        $self = self::orderBy("created_at", "desc");

        return $self;
    }

    public static function createLog($status, $template, $subject, $data, $userEmail, $userName)
    {
        self::quickSave([
            "data" => $data,
            "status" => $status,
            "subject" => $subject,
            "template" => $template,
            "user_name" => $userName,
            "user_email" => $userEmail,
        ]);
    }


    public function getSubject()
    {
        return $this->subject;
    }

    public function getUserEmail()
    {
        return $this->user_email;
    }

    public function getUserName()
    {
        return $this->user_name;
    }

    public function getSendData()
    {
        return json_decode($this->data);
    }

    public function getStatusName()
    {
        return self::$statuses[$this->status];
    }

    public function reSend()
    {
        $email = new Email($this->template, $this->subject, $this->getSendData(), $this->getUserEmail(), $this->getUserName());
        $email->send();
        if ($email->getStatus()) {
            $this->status = $email->getStatus();
            $this->save();
        }
    }

}