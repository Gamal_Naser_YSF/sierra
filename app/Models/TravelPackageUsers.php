<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/12/17
 * Time: 08:55 م
 */

namespace App\Models;


class TravelPackageUsers extends BaseModel
{
    protected $table = "travel_package_users";

    public static function rate($userId, $packageId, $rate)
    {
        self::upsart(["user_id" => $userId, "travel_package_id" => $packageId], ["rate" => $rate]);
        $avRate = ceil(self::where("travel_package_id", $packageId)->avg("rate"));

        TravelPackage::find($packageId)->updateRate($avRate);

    }
}