<?php

namespace App\Models\Traits;

use App\Models\Media;
use Illuminate\Http\Request;
use File;
use Jlib\Configs\Vars;
use Jlib\Upload\UploadFacad;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 11:05 م
 */
trait MediaAble
{
    private static $medisClass = Media::class;

    public function deleteImageFile()
    {

        foreach ($this->getMediaFiles() as $img => $type) {
            self::deleteOldOne($this->{$img});
        }
        return $this;
    }

    private static function deleteOldOne($file)
    {
        if (File::exists($filePath = Vars::getUploadPath() . "/" . $file)) File::delete($filePath);
    }

    private static function uploadFile(Request $request, $file, $type = "single")
    {
//dd($request, $file, $type,$request->file($file));
        if ($request->file($file))
            if ($type == "multi")
                $name = UploadFacad::justUpload($request->file($file))->all();
            else
                $name = UploadFacad::justUpload($request->file($file))->first();

        return $name;
    }

    public function updateMedia(Request $request)
    {
        $fields = $this->getMediaFiles();

        foreach ($fields as $name => $type) {

            if ($request->file($name)) {
                if ($type == "multi") {
                    self::$medisClass::insertNewMedia($this, self::uploadFile($request, $name, $type));
                } else {
                    self::deleteOldOne($this->{$name});
                    $this->{$name} = self::uploadFile($request, $name);
                }
            }
        }

        $this->save();

        return $this;
    }
//    public function uploadMedia(Request $request,$filename)
}