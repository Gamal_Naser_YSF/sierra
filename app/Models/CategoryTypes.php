<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/11/17
 * Time: 07:02 م
 */

namespace App\Models;


class CategoryTypes extends BaseModel
{
    protected $table = "category_types";


    public static function getTraveler()
    {
        return self::find(1);
    }

    public static function getBusiness()
    {
        return self::find(2);
    }
}