<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;
use Carbon\Carbon;

/**
 * Description of PermissionsRoles
 *
 * @author jooAziz
 */
class PermissionsRoles extends BaseModel
{

    public function role()
    {
        return $this->belongsTo(Role::class, "role_id");
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, "permission_id");
    }

    public function updatePermissionsForRole($id, array $permissions)
    {

        // get old permission ids
        $oldPermissionsObj = $this->where("role_id", $id);
        $oldPermissionsIds = $oldPermissionsObj->pluck("id")->toArray();

//        get old user permissiions ids
        $oldAdminPermissionsIds = AdminsRoles::whereIn("permission_id", $oldPermissionsIds);
        $oldAdminIds = $oldAdminPermissionsIds->pluck("admin_id")->toArray();

//        remve old $permissions
        $oldPermissionsObj->delete();
//        create new pwemissions
        $newIds = self::insertAndGetIds($permissions);
//        remove old permissions user
        $oldAdminPermissionsIds->delete();
//        create new permissions user
        self::createNewAdminsPermissions(array_unique($oldAdminIds), $newIds);

    }

    private static function insertAndGetIds(array $data)
    {
        $ids = [];
        foreach ($data as $item) {
            $ids[] = self::quickSave($item)->id;
        }
        return $ids;
    }

    private static function createNewAdminsPermissions(array $adminIds, array $newIds)
    {
        $now = Carbon::now();
        $items = [];
        foreach ($adminIds as $adminId) {
            foreach ($newIds as $id) {
                $items[] = [
                    "admin_id" => $adminId,
                    "permission_id" => $id,
                    "updated_at" => $now,
                    "created_at" => $now,
                ];
            }
        }

        AdminsRoles::insert($items);
    }
}

