<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 09:06 ص
 */

namespace App\Models;


use Illuminate\Http\Request;

class TravelPackagesBooking extends BaseModel
{
    protected $table = "travel_packages_booking";

    public static function filter(Request $request)
    {
        $self = (new static)->orderBy("created_at","desc");

        return $self->with("user")->with("package");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function package()
    {
        return $this->belongsTo(TravelPackage::class);
    }

    public function setSeen()
    {
        $this->seen = 1;
        $this->save();
    }

    public function isSeen()
    {
        return ($this->seen == 1) ? true : false;
    }

    public static function bookForUser(User $user, TravelPackage $package)
    {
        return self::upsart(["user_id" => $user->id, "package_id" => $package->id]);
    }
}