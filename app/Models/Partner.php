<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18/03/18
 * Time: 08:53 م
 */

namespace App\Models;


use App\Models\InterFaces\MediaAble;
use App\Models\Traits\MediaAble as MediaAbleTrait;
use Illuminate\Http\Request;

class Partner extends BaseModel implements MediaAble
{
    use MediaAbleTrait;

    /**
     * @param Request $request
     * @return static
     */
    public static function filter(Request $request)
    {
        $self = new  static;

        return $self;
    }

    public function getMediaFiles(): array
    {
        return ["logo" => "single"];
    }
}