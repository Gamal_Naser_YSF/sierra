<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 03/12/17
 * Time: 11:37 م
 */

namespace App\Models;


class Hotels extends BaseModel
{

    public static function asList($addplaceHolder = true, $index = "id")
    {
        $data = self::pluck("name", $index)->sort();

        if ($addplaceHolder)
            $data->prepend("Please select an Hotel", "0");


        return $data;
    }
}