<?php
namespace App\Models\InterFaces;

use App\Http\Controllers\UserInfo;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: server
 * Date: 05/06/17
 * Time: 02:04 م
 */
interface HasFillter
{
    public function fillter(Request $request);
}