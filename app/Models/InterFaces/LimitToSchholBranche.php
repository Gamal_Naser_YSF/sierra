<?php
/**
 * Created by PhpStorm.
 * User: server
 * Date: 05/06/17
 * Time: 08:25 م
 */

namespace App\Models\InterFaces;


use App\Http\Controllers\UserInfo;

interface LimitToSchholBranche
{
    public static function toSchool(UserInfo $userInfo);

    public static function toBranch(UserInfo $userInfo);

    public static function toSchoolAndBranch(UserInfo $userInfo);
}