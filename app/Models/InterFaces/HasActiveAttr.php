<?php
/**
 * Created by PhpStorm.
 * User: server
 * Date: 05/06/17
 * Time: 04:38 م
 */

namespace App\Models\InterFaces;


interface HasActiveAttr
{
    public function deactive();

    public function active();
}