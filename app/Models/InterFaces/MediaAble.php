<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 11:02 م
 */

namespace App\Models\InterFaces;


use Illuminate\Http\Request;

interface MediaAble
{
    public function getMediaFiles(): array;

    public function updateMedia(Request $request);
}