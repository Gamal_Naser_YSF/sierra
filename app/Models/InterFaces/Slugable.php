<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 20/04/18
 * Time: 11:14 ص
 */

namespace App\Models\InterFaces;


interface Slugable
{
    public function getSlug();
}