<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 31/03/18
 * Time: 11:19 ص
 */

namespace App\Models\InterFaces;


interface Payable
{
    public function getModuleName(): string;

    public function getPrice(): string;

    public function getModuleId(): int;

    public function getModuleInformation(): array;
}