<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:33 م
 */

namespace App\Models;


use App\Models\InterFaces\MediaAble;
use App\Models\InterFaces\Slugable;
use App\Models\Traits\MediaAble as MediaTrait;
use App\Services\Notify;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;


/**
 * @property mixed event_name
 */
class Event extends BaseModel implements MediaAble,Slugable
{
    use MediaTrait;

    /**
     * @param $eventId
     * @return integer
     */
    public static function updateAvgRate($eventId)
    {
        $rate = ceil(EventRates::whereEventId($eventId)->avg("rate"));
        $event = self::find($eventId);
        $event->overall_rating = $rate;
        $event->save();
        return $rate;
    }

    public function getName()
    {
        return $this->event_name;
    }

    public function canBeDelete()
    {
        return !Booking::EventIsBook($this);
    }

    public function andIncreaseViews()
    {
        $this->views++;
        $this->save();
        return $this;
    }

    public function linkToCategories(array $list)
    {
        $data = [];
        $now = Carbon::now();
        CategoyEvent::deleteByEventId($this->id);
        foreach ($list as $item) {
            $data[] = [
                "category_id" => $item,
                "event_id" => $this->id,
                "created_at" => $now,
                "updated_at" => $now,
            ];
        }
        if (!empty($data))
            CategoyEvent::insert($data);
        return $this;
    }

    /**
     * @param Request $request
     * @return self
     */
    public static function createNew(Request $request)
    {

        $data = self::makeDataArray($request);
        $data["overall_rating"] = 0;
        $data["status_id"] = Status::getIdForStatus(Status::active);

        $row = parent::quickSave($data)->linkToCategories($request->category_id)->updateMedia($request);
        return $row;
    }

    public static function updateOne(Request $request)
    {
        $data = self::makeDataArray($request);
        $data["id"] = $request->id;
        $row = self::quickUpdate($data)->linkToCategories($request->category_id)->updateMedia($request);
        return $row;
    }

    private static function makeDataArray(Request $request)
    {

        $data = $request->only(
            "event_name",
            "start_date",
            "end_date",
            "country",
            "city",
            "tags",
            "address",
            "event_location_lat",
            "event_location_long",
            "event_coordinator",
            "event_serial",
            "proposal_end_date",
            "organized_by",
            "event_highlightes",
            "details",
            "event_website"
        );
        return $data;
    }

    public function getMediaFiles(): array
    {
        return ["profile_picture" => "multi"];
    }

    public function getDay()
    {
        return Carbon::parse($this->start_date)->day;
    }

    public function getMonth()
    {
        return Carbon::parse($this->start_date)->format("M");
    }

    public function getYear()
    {
        return Carbon::parse($this->start_date)->format("Y");
    }

    public static function filter(Request $request)
    {

        $self = (new static);
//            $self = $self->where("status_id", "!=", Status::getIdForStatus(Status::deactivate));


        if (isset($request->i))
            $self = $self->where("id",$request->i);


        if ($request->q) {
            $keyword = $request->q;
            $self = $self->where(function ($q) use ($keyword) {
                return $q
                    ->where("event_name", "like", "%$keyword%")
                    ->orWhere("event_highlightes", "like", "%$keyword%")
                    ->orWhere("details", "like", "%$keyword%");
            });
        }

        if ($request->country)
            $self = $self->where("country", "like", "%$request->country%");

        if ($request->tags) {
            $tags = explode(",", $request->tags);
            for ($I = 0; $I < count($tags); $I++) {
                $tag = $tags[$I];
//                $self = $self->orWhere("tags", "like", "%$tag%");
                if ($I == 0)
                    $self = $self->where("tags", "like", "%$tag%");
                else
                    $self = $self->orWhere("tags", "like", "%$tag%");
            }
        }

        if ($request->date)
            $self = $self->orderBy("start_date", $request->date);

        if ($request->rate)
            $self = $self->orderBy("overall_rating", $request->rate);

        if ($request->views) {
            if ($request->views == "latest")
                $self = $self->orderBy("created_at", "desc");
            elseif ($request->views == "most")
                $self = $self->orderBy("views", "desc");
        }


        if ($request->categories) {
            $ids = Category::getIdsListByNames($request->categories)->toArray();
            $d = CategoyEvent::whereIn("category_id", $ids)->pluck("event_id");
            $self = $self->whereIn("id", $d);
        }
//        dd($self->toSql());
        return $self;
    }

    private $statusHolder;

    public function status()
    {
        if ($this->statusHolder) return $this->statusHolder;
        return $this->statusHolder = $this->belongsTo(Status::class);
    }

    public function startDate()
    {
        return Carbon::parse($this->start_date)->format("Y - m - d");
    }

    public function endDate()
    {
        return Carbon::parse($this->end_date)->format("Y - m - d");
    }

    public function proposalEndDate()
    {
        return Carbon::parse($this->proposal_end_date)->format("Y - m - d");
    }

    public function getStatusHtml()
    {
        return '<label class="label label - primary">' . $this->status->name . '</label>';
    }

    public function getRelatedStatus()
    {
        return Status::getById($this->status_id);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, (new CategoyEvent())->getTable());
    }

    public function withRelatedModels()
    {
        return $this->with("status")->with("categories")->with("media");
    }

    public function media()
    {
        return $this->hasMany(Media::class, "model_id")->whereModel($this->getTable());
    }

    public function withStatus()
    {
        return $this->with("status");
    }

    public function getTags()
    {
        return new Collection(($this->tags) ? explode(",", $this->tags) : []);
    }

    public function relatedCategoryAsIds()
    {
        return $this->categories->pluck("id");
    }

    public function getFirstImageName()
    {
        return (@$this->media->first()->file_name);
    }

    public function isBookedForUser(BaseModel $user = null)
    {
        if (!$user) return false;
        return Booking::isBooked($user->id, $this->id);
    }

    public function bookItForUser(BaseModel $user)
    {
        $row = Booking::Book($user->id, $this->id);
        Notify::notifyAdminAboutNewEventRegistration($row);
        return $row;
    }

    public function getSlug()
    {
       return $this->event_name;
    }
}