<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;

/**
 * Description of Role
 *
 * @author jooAziz
 */
class Role extends BaseModel
{
    private static $scope;

    public static function setScope($scope)
    {
        self::$scope = $scope;
    }

    public function getAll()
    {
        return self::where("scope", self::$scope)->get();
    }

    public function permissionsRoles()
    {
        return $this->hasMany(PermissionsRoles::class);
    }

    public function reformatPermissions()
    {
        $arr = [];
        foreach ($this->permissionsRoles as $permissionsRole) {
            $arr[$permissionsRole->permission->module][] = $permissionsRole->permission->action;
        }

        return new Collection($arr);
    }

    public function deleteWithRelated()
    {
        $id = $this->id;
//        dd($id);
        $rolesObj = PermissionsRoles::whereRoleId($id);
//        dd($this,$rolesObj->pluck("id")->toArray());
        $adminRols = AdminsRoles::whereIn("permission_id", $rolesObj->pluck("id")->toArray());
//        dd($rolesObj->lists("id"), $adminRols->lists("id"));
        $adminRols->delete();
        $rolesObj->delete();
        $this->delete();

    }

}
