<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/03/18
 * Time: 10:13 ص
 */

namespace App\Models;


use App\Models\InterFaces\Payable;
use App\Repo\Reservations\ReservationsStatus;
use Illuminate\Http\Request;


/**
 * @property mixed title
 * @property mixed id
 */
class Reservation extends BaseModel implements Payable
{
    protected $table = "reservation";

    public static function filter(Request $request)
    {
        $self = self::orderBy("created_at", "desc");


        if ($request->uid)
            $self = $self->whereUserId($request->uid);

        return $self;
    }

    public static function forPlane(Plan $plan, $user)
    {

        $data = [
            "type" => "plan",
            "user_id" => $user->id,
            "fees" => $plan->price,
            "status" => 0,
            "details" => json_encode(["details" => "reservation done automatically by system for pay a plane , please complete payment operation "]),
        ];
        return self::quickSave($data);
    }

    public function getType()
    {
        return $this->type;
    }

    public function shaneStatuses($newStatue)
    {
        $this->status = $newStatue;
        $this->save();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getStatusText()
    {
        return ReservationsStatus::getById($this->status);
    }

    public function getStatusLevel()
    {
        return ReservationsStatus::getLevelById($this->status);
    }

    public function getFees()
    {
        return $this->fees;
    }

    public function getDate()
    {
        return $this->created_at;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDetails()
    {
        return \GuzzleHttp\json_decode($this->details);
    }

    public function getModuleName(): string
    {
        return "Reservation";
    }

    public function getPrice(): string
    {
        return $this->getFees();
    }

    public function getModuleId(): int
    {
        return $this->id;
    }

    public function getModuleInformation(): array
    {
        return [
            "title" => $this->getTitle(),
            "type" => $this->getType(),
        ];
    }
}