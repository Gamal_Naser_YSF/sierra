<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of Permission
 *
 * @author jooAziz
 */
class Permission extends BaseModel
{
    public static function allGrouped($controler = null)
    {

        $self = new static;

        if (@$controler->scope)
            $self = $self->where("scope", $controler->scope);

        return $self->get()->groupBy("module");
    }
}
