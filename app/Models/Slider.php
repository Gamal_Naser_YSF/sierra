<?php

namespace App\Models;


use Hive\Models\MediaAble;
use Hive\Models\MediaInterface;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 12/12/17
 * Time: 11:17 م
 */
class Slider extends BaseModel
{
//    use MediaAble;

    public static function all($columns = ['*'])
    {
        return parent::orderBy("order")->get($columns);
    }

    public static function getTypes($i = null)
    {
        $ar = [0 => __("admin.please select type"), 1 => __("admin.internal"), 2 => __("admin.external")];

        if (is_null($i)) return $ar;

        return $ar[(int)$i];
    }

    public function getMediaFiles(): array
    {
        return ["image" => "single"];
    }

    public function getTypeName()
    {

        return self::getTypes($this->link_type);
    }


    public function linkTypeIs($linkType)
    {
        return (self::getTypes($this->link_type) == $linkType);
    }

    public function getImageAsLink()
    {
        return "/upload/".$this->image;
    }
}