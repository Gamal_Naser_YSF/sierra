<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use App\Http\Controllers\UserInfo;
use App\Models\InterFaces\Slugable;
use App\Models\InterFaces\tybeAble;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Description of BaseModel
 *
 * @author jooaziz
 */
class BaseModel extends Model implements tybeAble
{
    private static $enaabelDecrypt = true;
    protected $guarded = [];
    protected $myQuery;


    /**
     * @param $id
     * @return static
     */
    public static function findByDecryptId($id)
    {
        return self::findOrFail(self::decrypt($id));
    }

    public static function decrypt($id)
    {
        return (self::$enaabelDecrypt) ? _dec($id) : $id;
    }

    public static function encrypt($id, $endfix = "")
    {
        return (self::$enaabelDecrypt) ? _enc($id, $endfix) : $id;
    }


    public function encyrptId()
    {
        return self::encrypt($this->id, ($this instanceof Slugable) ? str_slug($this->getSlug()) : "");
    }

    /**
     * @param $data
     * @return static
     */
    public static function quickSave($data)
    {
        return (new static)->create($data);
    }

    public static function upsart(array $filds, array $data = [])
    {
        $row = static::firstOrNew($filds);

        foreach ($data as $k => $v) {
            $row->{$k} = $v;
        }


        $row->save();


        return $row;
    }

    /**3
     * @param $data
     * @return static
     */
    public static function quickUpdate($data)
    {
        $row = (new static)->findOrFail($data['id']);
        $row->update($data);
        return $row;
    }

    public static function quickDelete($id)
    {
        return (new static)->destroy($id);
    }

    public static function checkBeforDelete(BaseModel $model, $col, $id)
    {

        //  this check if model has col in other tabel 
        //  if it return number larger than  0 
        //  it abort mission 
        //  and return 0 as parent

        if ($model->where($col, $id)->count())
            return 0;
        return self::quickDelete($id);
    }

    public function getTableColumns($exeptions = [])
    {

        $arr = $this->getConnection()->getSchemaBuilder()->getColumnListing($this->getTable());

        foreach ($exeptions as $col) {
            if (($key = array_search($col, $arr)) !== false) {
                unset($arr[$key]);
            }
        }
        return $arr;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getQuery()
    {
        return (is_null($this->myQuery)) ? $this->query() : $this->myQuery;
    }

    public function deactive()
    {
        $this->active = 0;
        $this->save();
        return $this;
    }

    public function active()
    {
        $this->active = 1;
        $this->save();
        return $this;
    }

    public function commonFilter($self, UserInfo $userInfo = null)
    {

        if (is_null($userInfo))
            return $self;

        if ($this->branchCheck()) {
            if ($userInfo->getBranch() && $userInfo->getBranch()->id != 0)
                $self = $self->where("branch_id", $userInfo->getBranch()->id);
        }

        if ($userInfo->getSchoole())
            $self = $self->where("entity_id", $userInfo->getSchoole()->id);

        return $self;
    }

    public function branchCheck()
    {
        return true;
    }

    protected static function whereLike($builder, $filed, $val)
    {
        return $builder->where($filed, "like", "%$val%");
    }
//    public function newInstance($attributes = [])
//    {
//        return new Static($attributes);
//    }
    /**
     * @param string $key
     * @param \Closure|null $callback
     * @return Collection|mixed
     */
    public static function sCountBy($key = "status", \Closure $callback = null)
    {
        return (new static)->countBy($key, $callback);
    }

    /**
     * @param string $key
     * @param \Closure|null $callback
     * @return Collection|mixed
     */
    public function countBy($key = "status", \Closure $callback = null)
    {
        $data = self::groupBy($key)->select($key, \DB::raw("count(*) as count"))->get();
        $returner = new Collection();
        foreach ($data as $item) {
            $returner->put($item->{$key}, $item->count);
        }
        if ($callback)
            $returner = $callback($returner);

        return ($returner);
    }
}


