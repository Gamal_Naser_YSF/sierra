<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/12/17
 * Time: 09:45 م
 */

namespace App\Models;


use App\Models\InterFaces\Slugable;
use App\Models\Traits\MediaAble;
use App\Services\Notify;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Jlib\Upload\UploadFacad;

/**
 * @property mixed views
 * @property mixed name
 */
class TravelPackage extends BaseModel implements InterFaces\MediaAble, Slugable
{
    use MediaAble;

    protected $table = "travel_packages";

    public static function customeUpdate(Request $request)
    {
        $data = $request->only("id", "name", "destination", "duration_in_days", "description", "hotel", "airlines", "video", "tour_type");
//        if ($request->file("image"))
//            $data["image"] = UploadFacad::justUpload($request->file("image"))->first();

        return parent::quickUpdate($data)->updateMedia($request);
    }

    public static function customSave(Request $request)
    {
        $data = $request->only("name", "destination", "duration_in_days", "description", "hotel", "airlines", "video", "tour_type");
        $data["rate"] = 0;
        return parent::quickSave($data)->updateMedia($request);
    }

    public function getHotel()
    {
        return $this->belongsTo(Hotels::class, "hotel");
    }

    public function getAirlines()
    {
        return $this->belongsTo(Airlines::class, "airlines");
    }

    public function packageUser()
    {
        return $this->hasMany(TravelPackageUsers::class);

    }

    public function updateRate($rate)
    {
        $this->rate = $rate;
        $this->save();
    }

    public function getRateForUser(User $user = null)
    {
        return (int)(is_null($user)) ? 0 : @$this->packageUser()->whereUserId($user->id)->first()->rate;
    }


    public function getMediaFiles(): array
    {
        return ["image" => "multi"];
    }

    public function canBeDelete()
    {
        return true;
    }

    public function bookedRows()
    {
        return $this->hasMany(TravelPackagesBooking::class, "package_id");
    }

    public function bookedForUser(User $user = null)
    {
        if (is_null($user)) return false;
        return ($this->bookedRows()->whereUserId($user->id)->count() > 0) ? true : false;
    }

    public function bookItForUser(User $user)
    {
//        N
        $row = TravelPackagesBooking::bookForUser($user, $this);
        Notify::notifyAdminAboutNewTravelBookend($row);
        return $row;
    }

    public static function filter(Request $request)
    {
        $self = new static();


        if (isset($request->i))
            $self = $self->where("id", $request->i);

        if ($request->q)
            $self = $self->where("name", "like", "%{$request->q}%");

        if (($request->days) && is_numeric($request->days)) {
            $days = self::getDays("asNumbers", ["str" => $request->days]);
            $self = $self->whereBetween("duration_in_days", $days);
        }

        if ($request->location)
            $self = $self->where("destination", "like", "%{$request->location}%");

        $sort = $request->sort;
        if ($sort)
            if ($sort == "views")
                $self = $self->orderBy("views", "desc");
            elseif ($sort == "rate")
                $self = $self->orderBy("rate", "desc");

        if ($request->order)
            $self = $self->orderBy("name", $request->order);


        return $self;
    }

    public function media()
    {
        return $this->hasMany(Media::class, "model_id")->whereModel($this->getTable());
    }

    public function getFirstImageName()
    {
//        dump((@$this->media->first()->file_name));
        return (@$this->media->first()->file_name);
    }

    public function images()
    {
        return ($this->media->pluck("file_name"));
    }

    public static function getDays($str, $options = [])
    {
        $data = new Collection([
            "any" => [-1],
            "less than 3 days" => [0, 3],
            "3 ~ 7" => [3, 7],
            "7 ~ 14" => [7, 14],
            "14 ~ 30" => [14, 30],
            "more than 30 days" => [30, 300],
        ]);
        if ($str == "asArray")
            return $data->keys();

        if ($str == "asNumbers")
            return $data->values()->get($options["str"]);

    }

    public function increaseViews()
    {
        $this->views++;
        $this->save();
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSlug()
    {
        return $this->name;
    }
}