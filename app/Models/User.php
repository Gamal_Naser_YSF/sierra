<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 03:16 م
 */

namespace App\Models;


use App\Http\Controllers\Account\Auth\AuthLogic;
use App\Models\InterFaces\UserModel;
use App\Models\Traits\MediaAble;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jlib\Configs\Vars;
use Jlib\Utilitis\Encryption\UUID;

/**
 * @property mixed name
 * @property int is_complete
 */
class User extends BaseModel implements UserModel, \App\Models\InterFaces\MediaAble
{
    use MediaAble;

    public static function createNew(Request $request)
    {
        $data = $request->only("name", "email", "phone");
        $data["password"] = \Hash::make($request->password);
        $data["active"] = 1;
        return self::quickSave($data);

    }

    public function setInterest(array $data)
    {
        UsersCategoy::insertForUser($this->id, $data);
        return $this;
    }

    public function setProfileComplate()
    {
        $this->is_complete = 1;
        $this->save();
        return $this;
    }

    public function isProfileCompleate()
    {
        return $this->is_complete;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAvatar()
    {
        return ((filter_var($this->avatar, FILTER_VALIDATE_URL))) ? $this->avatar : Vars::getUploadPath() . "/" . $this->avatar;

    }

    public function getInterested()
    {
        $ids = $this->hasMany(UsersCategoy::class)->pluck("category_id");

        return Category::whereIn("id", $ids);
    }

    public function updateInterestes(array $interestIds = [])
    {

        $now = Carbon::now();
        $data = [];
        UsersCategoy::whereUserId($this->id)->delete();
        foreach ($interestIds as $interest) {
            $data[] = [
                "user_id" => $this->id,
                "category_id" => $interest,
                "created_at" => $now,
                "updated_at" => $now,
            ];
        }
        UsersCategoy::insert($data);
    }

    function getMyPermissions()
    {
        return [];
    }

    public function getMediaFiles(): array
    {
        return ["avatar" => "single"];
    }

    public function resetTokensFailds()
    {
        $this->token = "";
        $this->token_expire = "";
        $this->save();
    }

    public function updatePasswordAndWaitForNewOne()
    {
        $this->token = UUID::create();
        $this->token_expire = Carbon::now();
        $this->save();
    }

    public function setNewPassword($request)
    {
        if (!AuthLogic::isExpired($this->token_expire)) {

            $this->password = \Hash::make($request->password);
            $this->token = "";
            $this->token_expire = "";
            $this->save();
            return true;
        }

        return false;
    }


}
