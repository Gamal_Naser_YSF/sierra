<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 03/11/17
 * Time: 01:15 م
 */

namespace App\Models;


use Carbon\Carbon;
use \File;
use Jlib\Configs\Vars;

class Media extends BaseModel
{
    protected $table = "media";

    public static function insertNewMedia(BaseModel $model, array $ar)
    {
        $data = [];
        $table = $model->getTable();
        $modelId = $model->id;
        $now = Carbon::now();
        foreach ($ar as $file) {
            $data[] = [
                "file_name" => $file,
                "model" => $table,
                "model_id" => $modelId,
                "created_at" => $now,
                "updated_at" => $now,
            ];
        }
        self::insert($data);
    }

    public function delete()
    {
        if (File::exists($filePath = Vars::getUploadPath() . "/" . $this->file_name)) {
            File::delete($filePath);
        }
        return parent::delete(); // TODO: Change the autogenerated stub
    }
}