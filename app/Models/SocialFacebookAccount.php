<?php

namespace App\Models;


class SocialFacebookAccount extends BaseModel
{


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
