<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/01/18
 * Time: 01:47 ص
 */

namespace App\Models;


use Carbon\Carbon;


/**
 * @property mixed type
 * @property mixed status
 * @property Carbon created_at
 * @property mixed fees
 * @property mixed details
 * @property mixed user_id
 */
class Transaction extends BaseModel
{
    /**
     * @param array $data
     * @return self
     */
    public static function createRow(array $data)
    {
        if (@$data["total"])
            $data["total"] = json_encode($data["total"]);
        if (@$data["exchangeRates"])
            $data["exchangeRates"] = json_encode($data["exchangeRates"]);
        if (@$data["totalPurchases"])
            $data["totalPurchases"] = json_encode($data["totalPurchases"]);

        return self::quickSave($data);

    }

    public function doActions()
    {

    }
}


