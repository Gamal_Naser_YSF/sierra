<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

/**
 * Description of AdminsRoles
 *
 * @author jooAziz
 */
class AdminsRoles extends BaseModel
{
    protected $table = "admins_Permissions";
    public static function getAllForAdmin($id)
    {
        return self::whereAdminId($id)->where("scope", "admin")->get();
    }

    public static function getAllForUser($id)
    {
        return self::whereAdminId($id)->where("scope", "account")->get();
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class, "admin_id");
    }

    public function role()
    {
        return $this->belongsTo(PermissionsRoles::class, "role_id");
    }

    public function permission()
    {
        return $this->belongsTo(Permission::class, "permission_id");
    }

    public static function insertPermissionForUser($id, array $pers)
    {

    }
}
