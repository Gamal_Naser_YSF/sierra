<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 03/02/18
 * Time: 04:09 م
 */

namespace App\Models;


class EventRates extends BaseModel
{
    protected $table = "events_rates";

    /**
     * @param $eventId
     * @param $userId
     * @param $rate
     * @return int
     */
    public static function updateRateForUser($eventId, $userId, $rate)
    {
        self::upsart(["user_id" => $userId, "event_id" => $eventId], ["rate" => $rate]);
        return Event::updateAvgRate($eventId);
    }
}