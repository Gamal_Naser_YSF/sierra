<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 03/12/17
 * Time: 11:29 م
 */

namespace App\Models;


use Illuminate\Http\Request;

class Trip extends BaseModel
{
    public static function filter(Request $request)
    {
        $self = (new static)->orderBy("created_at","desc");
        return $self->with("user")->with("tripInfo");
    }

    public static function createForUser(User $user)
    {
        return self::quickSave(["created_by" => $user->id]);
    }

    public function tripInfo()
    {
        return $this->hasMany(UsersTripInfo::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, "created_by");
    }

    public function MakeItDone()
    {
        $this->is_done = 1;
        $this->save();
    }
}