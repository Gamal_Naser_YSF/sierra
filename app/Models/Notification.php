<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 20/03/18
 * Time: 08:40 م
 */

namespace App\Models;


use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * @property mixed message
 * @property Carbon created_at
 * @property mixed title
 * @property mixed status
 * @property mixed type
 */
class Notification extends BaseModel
{
    const seen = 1;
    const notSeen = 0;
    const admin = "admin";
    const user = "user";

    const event = "event";
    const payment = "payment";
    const trip = "trip";
    const travel = "travel";
    const reservation = "reservation";
    const error = "error";

    public function getType()
    {
        return ($this->type);
    }

    public static function filter(Request $request)
    {
        $self = self::whereScope(self::admin);

        if (isset($request->status))
            $self = $self->whereStatus($request->status);

        if (isset($request->type))
            $self = $self->whereType($request->type);

        return $self->orderBy("created_at", "desc");
    }

    public function isSeen()
    {
        return ($this->status == self::seen);
    }

    public function setSeen()
    {
        $this->status = self::seen;
        $this->save();
    }

    public static function getLatestForAdmin()
    {
        $nt = self::whereScope(self::admin)->whereStatus(self::notSeen)->orderBy("created_at", "desc");
        return (object)[
            "count" => $nt->count(),
            "notifications" => $nt->take(10)->get(),

        ];


    }

    public static function getLatestForUser($userId, $count = 3)
    {
        $nt = self::whereScope(self::user)->whereUserId($userId)->whereStatus(self::notSeen)->orderBy("created_at", "desc");

        return (object)[
            "count" => $nt->count(),
            "notifications" => $nt->take($count)->get(),
        ];


    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getStatusText()
    {
        return ($this->status == self::seen) ? "seen" : "not seen";
    }

    public function getDate()
    {
        return $this->created_at->diffForHumans();
    }

    public static function createNewNotification($to, $title, $message, $type, $userId = 0)
    {
        return self::quickSave(["scope" => $to, "title" => $title, "message" => $message, "user_id" => $userId, "type" => $type]);
    }

    public static function createNewAdminNotification($title, $message, $type)
    {
        return self::createNewNotification(self::admin, $title, $message, $type);
    }

    public static function createNewUserNotification($title, $message, $type, User $user)
    {
        $row = self::createNewNotification(self::user, $title, $message, $type, $user->id);
        return $row;
    }

}