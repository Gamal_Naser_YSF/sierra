<?php

namespace App\Models;

use App\Models\InterFaces\Payable;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 12:17 ص
 * @property mixed price
 * @property mixed name
 * @property mixed id
 */
class Plan extends BaseModel implements Payable
{
    public function getPrice(): string
    {
        return $this->price;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getModuleName(): string
    {
        return "Plane";
    }

    public function getModuleId(): int
    {
        return $this->id;
    }

    public function getModuleInformation(): array
    {
        return [
            "name" => $this->getName(),
        ];
    }
}