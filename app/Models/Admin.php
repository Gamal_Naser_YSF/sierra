<?php

namespace App\Models;

use App\Http\Controllers\UserInfo;
use App\Models\InterFaces\HasActiveAttr;
use App\Models\InterFaces\HasFillter;
use App\Models\InterFaces\UserModel;
use Applexicon\Utilitis\Encryption\UUID;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class Admin extends BaseModel implements HasActiveAttr, HasFillter, UserModel
{
    private $adminRolesClass;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->adminRolesClass = new AdminsRoles();
    }

    private $scope;

    public function adminRoles()
    {
        return $this->hasMany(AdminsRoles::class)->whereScope("admin");
    }

    public function roles()
    {
        return $this->hasMany(AdminsRoles::class)->whereScope("admin");
    }


    public function resetTokensFailds()
    {
        $this->token = "";
        $this->token_expire = "";
        $this->save();
    }

    public function setNewPassword($request)
    {
        $this->password = \Hash::make($request->password);
        $this->save();
        return $this;
    }

    /**
     *
     * @return Collection
     */
    public function getMyPermissions()
    {
        $permissions = [];
        foreach ($this->adminRoles as $adminRoles)
            if ($adminRoles->permission)
                $permissions[] = $adminRoles->permission;

        return new Collection($permissions);
    }

    public function fillter(Request $request, UserInfo $userInfo = null)
    {
        $self = $this;
        if (!is_null($request->f) && $request->f != "")
            $self = $self->whereActive($request->f);

        if ($request->phone)
            $self = $self->where("phone", "like", "%$request->phone%");

        if ($request->email)
            $self = $self->where("email", "like", "%$request->email%");


        if ($request->from)
            $self = $self->whereDate("created_at", "<=", $request->from);
        if ($request->to)
            $self = $self->whereDate("created_at", ">=", $request->to);

        return $self;
    }

    public function updatePasswordAndWaitForNewOne()
    {
        $this->password = UUID::create();
        $this->token = UUID::create();
        $this->token_expire = Carbon::now();
        $this->save();
    }

    public function deleteOldPermissions()
    {
        $this->adminRolesClass
            ->newInstance()
            ->whereAdminId($this->id)
            ->whereScope($this->scope)
            ->delete();
        return $this;
    }

    public function setScope($scope)
    {
        $this->scope = $scope;
        return $this;
    }

    public function insertNewPermissions(array $perIds)
    {
        $data = [];
        foreach ($perIds as $id)
            $data[] = ["admin_id" => $this->id, "permission_id" => $id, "scope" => $this->scope];

        $this->adminRolesClass
            ->newInstance()->insert($data);
        return $this;
    }

    public static function getAsList()
    {
        return self::pluck("username", "id");
    }

    public static function allExeptMe($id)
    {
        return self::where("id", "!=", $id)->pluck("username", "id");
    }


}
