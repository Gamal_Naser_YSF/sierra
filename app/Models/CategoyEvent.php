<?php

namespace App\Models;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 02/11/17
 * Time: 06:41 م
 */

class CategoyEvent extends BaseModel
{
    protected $table = "categories_events";

    public static function deleteByEventId($id)
    {
        self::whereEventId($id)->delete();
    }
}