<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/11/17
 * Time: 01:09 م
 */

namespace App\Models;


use Illuminate\Http\Request;

class Booking extends BaseModel
{
    protected $table = "booking";
    const statuses = ["new", "pending", "reserved", "closed"];

    public function changeStatus($status)
    {
        $this->seen = $status;
        $this->save();
    }

    public static function getStatuses()
    {
        return self::statuses;
    }

    public static function getStatusesByKey($k)
    {
        return @(self::statuses[$k]);
    }

    public static function EventIsBook(Event $event)
    {

        return (self::whereEventId($event->id)->count() > 0) ? true : false;
    }

    public function setSeen()
    {
        $this->seen = 1;
        $this->save();
        return $this;
    }

    public static function unBook($userId, $eventId)
    {
        return self::whereUserId($userId)->whereEventId($eventId)->delete();
    }

    public static function Book($userId, $eventId)
    {
        return self::upsart(["user_id" => $userId, "event_id" => $eventId]);
    }

    public static function getEventsBookedIds($userId)
    {
        return self::whereUserId($userId)->pluck("event_id")->toArray();
    }

    public static function isBooked($userId, $eventId)
    {
        $status = self::whereUserId($userId)->whereEventId($eventId)->first();
        return ($status) ? true : false;
    }

    public static function filter(Request $request)
    {
        $self = (new static);

        if (isset($request->s))
            $self = $self->whereSeen($request->s);

        if (isset($request->i))
            $self = $self->where("id",$request->i);

        return $self->with("user")->with("event");
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function event()
    {
        return $this->belongsTo(Event::class);
    }

    public function isSeen()
    {
        return ($this->seen == 1) ? true : false;
    }
}