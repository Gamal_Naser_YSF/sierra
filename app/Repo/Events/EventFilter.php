<?php

namespace App\Repo\Events;

use App\Models\Booking;
use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 25/11/17
 * Time: 02:23 م
 */
class EventFilter
{
    private $events;

    public function __construct($type, $userId)
    {
        $this->events = new Event();
        $myEventIds = Booking::getEventsBookedIds($userId);
        switch ($type) {
            case "myEvent":
                $this->events = $this->events->whereIn("id", $myEventIds);
                break;
            case "activeEvent":
                $this->events = $this->events
                    ->whereNotIn("id", $myEventIds)
                    ->whereDate("start_date", "<", Carbon::now())
                    ->whereDate("end_date", ">", Carbon::now());
                break;
            case "upcomingEvent":
                $this->events = $this->events
                    ->whereNotIn("id", $myEventIds)
                    ->whereDate("start_date", ">", Carbon::now())
                    ->whereDate("end_date", ">", Carbon::now());
                break;
        }
    }

    /**
     * @return Builder
     */
    public function getAsCollection()
    {
        return $this->events;
    }
}