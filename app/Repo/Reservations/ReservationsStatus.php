<?php

namespace App\Repo\Reservations;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 22/03/18
 * Time: 06:04 م
 */

class ReservationsStatus
{
    private static $statuses = [
        "0" => ["name" => "pending", "level" => "warning"],
        "1" => ["name" => "paid", "level" => "success"],
        "2" => ["name" => "failed", "level" => "danger"],
        "3" => ["name" => "ignored", "level" => "danger"],

    ];
    private static $arrayStatuses;
    private static $arrayLevel;

    public static function getById($id)
    {
        return self::getAsArray()[$id];
    }

    public static function getLevelById($id)
    {
        return self::getLevelAsArray()[$id];
    }

    public static function getAsArray()
    {
        if (!self::$arrayStatuses)
            foreach (self::$statuses as $k => $status)
                self::$arrayStatuses[$k] = $status["name"];

        return self::$arrayStatuses;

    }

    public static function getLevelAsArray()
    {
        if (!self::$arrayLevel)
            foreach (self::$statuses as $k => $status)
                self::$arrayLevel[$k] = $status["level"];

        return self::$arrayLevel;

    }
}