<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Jlib\Validation\CustomeValidation;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CustomeValidation::init();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
