<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 21/10/17
 * Time: 02:11 ص
 */

namespace App\Http\Middleware;


use Closure;
use Jlib\Auth\Auther;

class UserMustBeGuest
{
    public function handle($request, Closure $next, $key,$route)
    {
        Auther::setKey($key);
        if (Auther::isLogin())
            return redirect()->route($route);

        return $next($request);
    }
}