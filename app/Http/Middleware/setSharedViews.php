<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 28/03/18
 * Time: 10:38 م
 */

namespace App\Http\Middleware;


use App\Models\Notification;
use Closure;
use Illuminate\Http\Request;
use Jlib\Auth\Auther;

class setSharedViews
{
    public function handle(Request $request, Closure $next)
    {
        if (Auther::isLogin())
            view()->share("userNotification", Notification::getLatestForUser(Auther::getUser()->id));

        return $next($request);
    }
}
