<?php

namespace App\Http\Middleware;

use Closure;
use Jlib\Auth\Auther;

class MustLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $key,$route)
    {
        Auther::setKey($key);
        if (Auther::isLogin())
            return $next($request);

        return redirect()->route($route);
    }
}
