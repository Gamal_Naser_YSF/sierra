<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ShardAccountAndFront;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/11/17
 * Time: 10:07 ص
 */
class Base extends Controller
{
    use ShardAccountAndFront;

    public function __construct()
    {
        self::setNotificationShared();
        parent::__construct();
    }
}