<?php

namespace App\Http\Controllers\Account\Dashboard;

use App\Http\Controllers\Account\Base;
use App\Models\Event;
use App\Services\EventFormater;
use Jlib\Auth\Auther;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/11/17
 * Time: 10:08 ص
 */
class Dashboard extends Base
{
    public function getIndex()
    {

        if(!Auther::getUser()->isProfileCompleate())
        return redirect()->route("pages.interesting");
        return parent::view();
    }


}