<?php

namespace App\Http\Controllers\Account\Events;

use App\Http\Controllers\Account\Base;
use App\Models\Booking;
use App\Models\Event;
use App\Repo\Events\EventFilter;
use App\Services\EventFormater;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Jlib\Auth\Auther;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/11/17
 * Time: 06:19 م
 */
class Events extends Base
{
    public function getMyEvent(Request $request)
    {
        $events = (new EventFilter($request->type, Auther::getUser()->id))->getAsCollection()->get();
        $formattedEvents = (new EventFormater($events))->format($request->ft);
        return response()->json(["events" => $formattedEvents]);
    }

    public function unBook(Request $request)
    {
        Booking::unBook(Auther::getUser()->id, _dec($request->id));
        return response()->json(["status" => true]);
    }

    public function unSubscribe(Request $request)
    {
        $id = Event::decrypt($request->id);
        $userId = Auther::getUser()->id;
        Booking::unBook($userId, $id);
        return response()->json(["status" => true]);
    }
}