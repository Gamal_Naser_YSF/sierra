<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 15/12/17
 * Time: 09:11 ص
 */

namespace App\Http\Controllers\Account\Auth;


use App\Http\Controllers\Account\Auth\Service\{
    Providers, Fb as fab
};
use App\Http\Controllers\Account\Base;
use Illuminate\Http\Request;
use Jlib\Auth\Auther;

//use App\Services\SocialFacebookAccountService;
class FB extends Base
{
    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirect($provider)
    {
        return Providers::genral($provider)->redirect();
    }

    /**
     * @param fab $service
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback($provider, fab $service)
    {
        try {
            $user = $service->createOrGetUser($provider, Providers::genral($provider)->user());
            Auther::setUser($user);
        } catch (\Exception $e) {
            flash()->error("sorry some thing error please try again later ");
//            echo $e->getMessage()->message;
        }
        return "<script>window.close()</script>";
//        return redirect()->to('/');
    }
}