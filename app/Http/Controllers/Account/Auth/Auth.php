<?php

namespace App\Http\Controllers\Account\Auth;

//use \App\Http\Controllers\Admin\Base as BaseController;
use App\Http\Controllers\Account\Base;
use App\Models\User as Model;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Account\Auth\Validator as Valid;
use Jlib\Auth\Auther;

class Auth extends Base
{

    public function __construct(Model $model)
    {
        parent::__construct();
    }

    public function getIndex()
    {
        return redirect()->to($this->scope . '/' . $this->module . '/login');
    }

    public function getLogin()
    {
        return redirect()->to('/');
    }

    public function postLogin(Request $request)
    {

        $auth = AuthLogic::instance(new Model, $request, 'email')->login();
        if ($auth->canLogin())
            Auther::setUser($auth->getRow());

        if ($request->ajax()) {
            return response()->json(["level" => $auth->getLevel(), "status" => $auth->getStatus(), "message" => $auth->getMessage()]);
        } else {
            flash()->{$auth->getLevel()}($auth->getMessage());

            if ($auth->canLogin())
                return redirect()->to('account');

            return back();
        }


    }


    public function getRegister()
    {
        return redirect('admin/auth/login');
    }

    public function postRegister(Request $request)
    {
        $this->validate($request, [
            "name" => "required",
            "email" => "required|email|unique:users,email",
            "phone" => "required|numeric|unique:users,phone",
            "accept" => "required",
            "password" => "required|min:8",
            "password_confirmation" => "required|same:password",
        ]);

        $row = User::createNew($request);
        \Mail::send("emails.register", ["user" => $row], function ($mail) use ($row) {
            $mail->from(env('MAIL_FROM_EMAIL'), env('MAIL_FROM_NAME'));
            $mail->to($row->email, @$row->name)->subject(__("admin.Your new password at") . ' ' . env('SITE_NAME'));
        });
        return response()->json(["status" => true]);
    }

    public function getForgotPassword()
    {
        return parent::view();
    }

    public function postForgotPassword(Request $request)
    {

        if (AuthLogic::instance(new Model, $request)->forgotPassword()) {
            flash()->success("please check your email");
            return redirect()->route("adminLogin");
        }
        flash()->error("invalid or un registered email");
        return back();
    }

    public function getLogout()
    {
        Auther::logout();
        return back();
    }

    public function getResetPassword($token)
    {
        if ($token) {
            /** @var Model $row */
            if ($row = Model::whereToken($token)->first()) {
                if (!AuthLogic::isExpired($row->token_expire)) {
                    flash()->success("plese insert your new password");
                    return parent::view(compact('row'));
                }
            }
        }
        flash()->error("invalid or old token");
        return redirect()->to('account/auth/forget-password');
    }

    public function postResetPassword(Request $request, $token)
    {

        $this->validate($request, [
            "password" => "required|min:8",
            "password_confirmation" => "required|min:8|same:password",
        ]);
        /** @var Model $row */
        $reselt = Model::whereToken($token)->first()->setNewPassword($request);
        if ($reselt) {
            flash()->success("new password set ,please type it to login");
            return redirect()->to("account/auth/login");
        } else {
            flash()->error("token expired please enter your email to send a new token");
            return redirect()->to("account/auth/forget-password");
        }
    }
}
