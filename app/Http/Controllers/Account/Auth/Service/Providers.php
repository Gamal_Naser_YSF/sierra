<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 15/12/17
 * Time: 11:20 ص
 */

namespace App\Http\Controllers\Account\Auth\Service;


class Providers
{
    /**
     * @return \Laravel\Socialite\Contracts\Provider
     */
    public static function facebook()
    {
        return \Socialite::driver('facebook');
    }

    /**
     * @param $provider
     * @return \Laravel\Socialite\Contracts\Provider
     */
    public static function genral($provider)
    {
        return \Socialite::driver($provider);
    }

}