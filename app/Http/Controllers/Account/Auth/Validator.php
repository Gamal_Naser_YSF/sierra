<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Account\Auth;

/**
 * Description of Validator
 *
 * @author jooAziz
 */
use Jlib\Validation\BaseValidation;

class Validator extends BaseValidation
{
    protected $myUrl = "account/auth/";

    public function rules()
    {
        if ($this->is($this->myUrl . "reset-password"))
            return $this->resetPassword();
        return parent::rules();
    }

    private function resetPassword()
    {
        return [
            "password" => "required|min:8"
        ];

    }

    protected function validaCreate()
    {

    }

    protected function validaEdit()
    {

    }


    protected function validatCreate()
    {
        // TODO: Implement validatCreate() method.
    }

    protected function currentRoute()
    {
        // TODO: Implement currentRoute() method.
    }

    protected function validatEdit()
    {
        // TODO: Implement validatEdit() method.
    }
}
