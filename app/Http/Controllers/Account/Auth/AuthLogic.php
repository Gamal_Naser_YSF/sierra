<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Account\Auth;

/**
 * Description of AuthLogic
 *
 * @author jooaziz
 */
use Illuminate\Http\Request;
use \App\Models\BaseModel;
use Carbon\Carbon;

class AuthLogic
{

    private $row;
    private $status;
    private $message;
    private $level;
    private $searchabelField = 'email';
    private $searchabelFieldPassword = 'password';
    private $request;
    private $validationFalid = false;

    /**
     *
     * @param BaseModel $model
     * @param Request $request
     * @param type $searchabelField
     * @return AuthLogic
     */
    public static function instance(BaseModel $model, Request $request, $searchabelField = 'email')
    {
        return new static($model, $request, $searchabelField);
    }

    private function __construct(BaseModel $model, Request $request, $searchabelField)
    {
        self::validatInputs($request, [$searchabelField => 'required', 'password' => 'required']);
        $this->searchabelField = $searchabelField;
        $this->request = $request->all();
        $this->row = $model;
    }

    private function validatInputs($request, array $rules)
    {
        $validation = \Validator::make($request->all(), $rules);
        if ($validation->fails()) {
            $this->message = "Please check all field are entered";
            $this->level = "error";
            $this->validationFalid = TRUE;
        }
    }

    /**
     *
     * @return \App\Models\BaseModel
     */
    public function getRow()
    {
        return $this->row;
    }

    public function canLogin()
    {
        return $this->status;
    }

    public function login()
    {
        if (!$this->validationFalid) {
            $this->row = $this->row->where($this->searchabelField, $this->request[$this->searchabelField])->first();
            $this->isValid();
        }
        return $this;
    }

    private function isValid()
    {
        $this->status = false;

        if (!$this->row) {
            $this->message = _t('admin.' . $this->searchabelField ).' ' . _t('admin.doesn’t exist');
            $this->level = "error";
        } elseif (!$this->row->active) {
            $this->message = _t('admin.Account is not active Please contact your manager to activate your account');
            $this->message = _t('admin.Account is not active Please contact your manager to activate your account');
            $this->level = "error";
        } else if (!\Hash::check($this->request[$this->searchabelFieldPassword], $this->row->password)) {
            $this->message = _t('admin.wrong password');
            $this->level = "error";
        } else {
            $this->status = TRUE;
            $this->message = _t('admin.welcome');
            $this->level = "success";
        }
        return $this->status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function forgotPassword()
    {
        $row = $this->row->whereEmail($this->request['email'])->first();
        if ($row) {
            $row->updatePasswordAndWaitForNewOne();

            $data = [
                'row' => $row,
                "resetUrl" => url( "account/auth/reset-password/$row->token"),
                "name" => $row->name
            ];
            \Mail::send('emails.forgetPassword', $data, function ($mail) use ($row) {
                $mail->from(env('MAIL_FROM_EMAIL'), env('MAIL_FROM_NAME'));
                $mail->to($row->email, @$row->name)->subject(_t("admin.Your new password at") . ' ' . env('SITE_NAME'));
            });
            return TRUE;
        }
        return FALSE;
    }

    public static function isExpired($data)
    {
        return !(Carbon::parse($data)->addHours(24)->gte(Carbon::now()));
    }

}
