<?php

namespace App\Http\Controllers\Account\Reservations;

use App\Http\Controllers\Account\Base;
use App\Models\Reservation;
use App\Repo\Reservations\ReservationsStatus;
use App\Services\MapPaginate;
use App\Services\Transactions\TransactionsURLGenerator;
use Illuminate\Http\Request;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Jlib\Auth\Auther;

class Reservations extends Base
{
    public function myReservation(Request $request)
    {
        $request->request->add(["uid" => Auther::getUser()->id]);
        $itemsPaginated = Reservation::filter($request)->paginate(3);

        return MapPaginate::get($itemsPaginated, function ($item) {
            $item->status_type = ReservationsStatus::getById($item->status);
            $item->payment_url = TransactionsURLGenerator::forModel($item)->url();
            return $item;
        });

    }
}