<?php

namespace App\Http\Controllers\Account\Users;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 26/11/17
 * Time: 09:47 م
 */


use App\Http\Controllers\Account\Base;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Jlib\Auth\Auther;
use Jlib\Configs\Vars;
use Jlib\Upload\UploadFacad;

class Users extends Base
{
    public function update(Request $request)
    {
        $id = Auther::getUser()->id;
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $id,
            'phone' => 'required|numeric|unique:users,phone,' . $id,
            'address' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(["status" => false, "errors" => $validator->errors()]);
        }

        $data = $request->only("name", "email", "phone", "address");
        $data["id"] = Auther::getUser()->id;
        Auther::setUser(User::quickUpdate($data));
        return response()->json(["status" => true]);
    }

    public function getInfo()
    {
        $userInfo = Auther::getUser()->only("address", "avatar", "phone", "name", "email");
        $userInfo["avatar"] = Auther::getUser()->getAvatar();
        return response()->json(["userInfo" => $userInfo]);
    }

    public function updateImage(Request $request)
    {
        $user = User::find(Auther::getUser()->id);
        $user->avatar = UploadFacad::justUpload($request->avatar)->first();
        $user->save();
        Auther::setUser($user);
        $userInfo = $user->only("name", "email", "phone", "address", "avatar");
        $userInfo["avatar"] = Auther::getUser()->getAvatar();

        return response()->json(["userInfo" => $userInfo]);
    }

    private static function makeAvatarUrl($avatar)
    {
        return ((filter_var($avatar, FILTER_VALIDATE_URL))) ? $avatar : Vars::getUploadPath() . "/" . $avatar;
    }

}