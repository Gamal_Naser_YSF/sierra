<?php

namespace App\Http\Controllers\Account\Notifications;


use Jlib\Auth\Auther;
use App\Models\Notification as Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Account\Base;
use App\Repo\Reservations\ReservationsStatus;
use Illuminate\Pagination\LengthAwarePaginator;

class Notifications extends Base
{
    public function myNotifications(Request $request)
    {
        $request->request->add(["uid" => Auther::getUser()->id]);
        $rows = Model::whereUserId(Auther::getUser()->id);
        /** @var LengthAwarePaginator $data */
        $data = $rows->orderBy("created_at", "desc")->paginate(3);
        Model::whereIn("id",$data->map(function ($i){return$i->id;})->toArray())->update(["status" => 1]);

        return $data;


    }
}