<?php

namespace App\Http\Controllers\Account\Transactions;

use App\Http\Controllers\Account\Base;
use App\Models\Transaction as Model;
use App\Services\MapPaginate;
use App\Services\Transactions\TransactionTransformer;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Jlib\Auth\Auther;

class Transactions extends Base
{
    public function myTransactions(Request $request)
    {
        $transactions = Model::orderBy("created_at", "desc")->whereUserId(Auther::getUser()->id)->paginate(3);
        return MapPaginate::get($transactions, function ($t) {
            return new TransactionTransformer($t);
        });


    }
}