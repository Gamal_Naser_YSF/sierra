<?php

namespace App\Http\Controllers\Front;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 08/10/17
 * Time: 09:34 م
 */


use App\Models\Airlines;
use App\Models\Category;
use App\Models\CategoryTypes;
use App\Models\CustomPackage;
use App\Models\Event;
use App\Models\EventRates;
use App\Models\Hotels;
use App\Models\Partner;
use App\Models\Plan;
use App\Models\Reservation;
use App\Models\Slider;
use App\Models\Transaction;
use App\Models\TravelPackage;
use App\Models\TravelPackagesBooking;
use App\Models\TravelPackageUsers;
use App\Models\UsersTrip;
use App\Services\Notify;
use App\Services\Transactions\TransactionsURLGenerator;
use App\Services\Trips\TripDetailsFromRequest;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Jlib\Auth\Auther;
use Plugin\Payment\PaymentInit;

class Home extends FrontBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function isLogin()
    {
        return response()->json(["status" => Auther::isLogin()]);
    }

    public function plans()
    {
        return parent::view(["rows" => Plan::all()]);
    }

    public function payPlan($id)
    {
        if (!Auther::isLogin()) {
            flash()->warning("please login first to complete payment");
            return back();
        }

        return redirect()->to(
            TransactionsURLGenerator::forModel(Plan::findByDecryptId($id))->url()
        );


    }

    public function saveInterest(Request $request)
    {
        $this->validate($request, [
            "data" => "required|array",
        ]);

        Auther::getUser()->setInterest(array_values($request->data))->setProfileComplate();
        return response()->json(["status" => true]);
    }

    public function interesting(Request $request)
    {

        if (!Auther::isLogin())
            return redirect("/account/auth/login");


        if (Auther::getUser()->isProfileCompleate())
            return redirect("/account");
        return parent::view(["categories" => Category::with("typeModel")->get()]);
    }

    public function generalPay(Request $request)
    {
        $this->validate($request, [
            "price" => "required|numeric",
            "message" => "required",
        ]);

        return PaymentServiceLayer::generalPay($request->price, Auther::getUser()->email, $request->message);
    }

    public function payment($status, Request $request)
    {


        $res = PaymentServiceLayer::getDetailsByToken($request->token)->toArray();
        $res["user_id"] = Auther::getUser()->id;
        Transaction::createRow((array)$res)->doActions();

        if ($status == "canceled") {
            Notify::notifyAdminAboutFaildPayment($request->token);
            flash()->warning("sorry you cancel payment process ");

        } else {
            Notify::notifyAdminAboutSuccessdPayment($request->token, $res);
            flash()->success("thank you you pay successfully ");
        }
        return redirect()->to("/account");

    }

    public
    function about()
    {

        return parent::view();
    }

    public
    function getIndex()
    {
        $data = [];
        $data['types'] = CategoryTypes::all();
        $data["travelPackages"] = TravelPackage::limit(12)->get();
        $data["plans"] = Plan::all();
        $data["partners"] = Partner::all();
        $data["sliders"] = Slider::orderBy("order", "asc")->get();
//        dd($data);
        $data['categoriesTypes'] = Category::getShowabelCategores()->map(function (Collection $item) {
            return $item->take(10);
        });
        return Parent::view($data);
    }

    public
    function search(Request $request)
    {
        if ($request->ajax()) {
            return response()->json([
                "eventsList" => Event::filter($request)->paginate()
            ]);
        } else {
            return parent::view([
                "countries" => config("countries"),
                "categoriesList" => Category::getAsList(true, "name"),
                "eventsList" => Event::filter($request)->paginate()->appends($request->all())
            ]);
        }
    }

    public
    function eventDetails($id)
    {
        return parent::view(["row" => Event::findByDecryptId($id)->andIncreaseViews()]);
    }

    public
    function bookEvent(Request $request)
    {
        if (!Auther::isLogin())
            return response()->json(["status" => false, "message" => "please login to book it"]);

        Event::findByDecryptId($request->event_id)->bookItForUser(Auther::getUser());

        return response()->json(["status" => true, "message" => "done successfully"]);
    }

    public
    function onePackage($id)
    {
        $row = TravelPackage::findByDecryptId($id);
        $row->increaseViews();
        $airlines = Airlines::asList();
        $hotels = Hotels::asList();
        return parent::view(compact("row", "airlines", "hotels"));
    }

    public
    function createCustomPackage(Request $request)
    {
//        dd($request->all());
        if (!Auther::isLogin()) {
            flash()->error("please login first");
            return back();
        }
        $this->validate($request, [
            "departure_date" => "required",
            "hotel_id" => "required",
            "airlines" => "required",
            "adults" => "required|min:1",
            "Children" => "required",
            "Infant" => "required",
        ]);
        CustomPackage::createNewOne($request, Auther::getUser());

        flash()->success("done successfully");
        return back();
    }

    public
    function createTrip()
    {
        return parent::view();
    }

    public
    function saveCreateTrip(Request $request)
    {

        if (!Auther::isLogin()) {
            flash()->warning("please login first");
            return back();
        }

        $rows = $request->rows;

        if (!$rows || empty($rows)) {
            flash()->warning("please insert at least one stop point");
            return back();
        }

        foreach ($rows as $row) {
            $checkStatus = Carbon::parse($row["check_out"])->lt(Carbon::parse($row["check_in"])); // must false
            $arrivalStatus = Carbon::parse($row["departure"])->gt(Carbon::parse($row["arrival"])); // must false
            if (!$checkStatus) {
                flash()->warning("please make sure that all check in dates is before check out dates");
                return back();
            }

            if (!$arrivalStatus) {
                flash()->warning("please make sure that all arrival dates is before departure dates");
                return back();
            }
        }


        flash()->success("thank you, Booking created");
        (new TripDetailsFromRequest($rows, Auther::getUser()))->insertRows();

        return back();

    }

    public
    function ratePackage(Request $request)
    {
        TravelPackageUsers::rate(Auther::getUser()->id, _dec($request->package_id), $request->rate);
        return response()->json(["result" => true]);
    }

    public
    function bookPackage(Request $request)
    {
        TravelPackage::findByDecryptId($request->id)->bookItForUser(Auther::getUser());
        return response()->json(["status" => true]);
    }

    public
    function travelPackage(Request $request)
    {
//        dd(is_numeric($request->limit),($request->limit));
        return parent::view(
            [
                "packages" => TravelPackage::filter($request)
                    ->paginate((is_numeric($request->limit) ? $request->limit : 15))
                    ->appends($request->all())
            ]
        );
    }

    public
    function rateEvent(Request $request)
    {
        if (!Auther::isLogin())
            return response()->json(["message" => "user not login", "status" => false]);

        $this->validate($request, ["rate" => "required", "event_id" => "required"]);

        $eventId = Event::decrypt($request->event_id);

        $newRate = EventRates::updateRateForUser($eventId, Auther::getUser()->id, $request->rate);


        return response()->json(["message" => "done, thank you", "status" => true, "newRate" => $newRate]);
    }

    public
    function getCountries()
    {
        return response()->json(["countries" => config("countries")]);
    }


}