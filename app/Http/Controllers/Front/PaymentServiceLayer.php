<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/01/18
 * Time: 12:28 ص
 */

namespace App\Http\Controllers\Front;

use App\Models\Plan;
use Jlib\Auth\Auther;
use Plugin\Payment\ParchesDetails;
use Plugin\Payment\PaymentDetails;
use Plugin\Payment\PaymentInit;
use Plugin\Payment\Response;

class PaymentServiceLayer
{
    public static function payPlan(Plan $plan)
    {
        $data = [
            "amount" => $plan->price,
            "userEmail" => Auther::getUser()->email,
            "paymentDescription" => "user id[" . Auther::getUser()->id . "] want to pay plane [{$plan->id}]"
        ];

        (new PaymentInit(new PaymentDetails($data)))->init()->redirect();
    }

    public static function generalPay($price, $email, $description)
    {
        $data = ["amount" => $price, "userEmail" => $email, "paymentDescription" => $description];
        (new PaymentInit(new PaymentDetails($data)))->init()->redirect();
    }

    /**
     * @param $token
     * @return Response
     */
    public static function getDetailsByToken($token)
    {
        return (new ParchesDetails($token))->getDetailsForToken()->getResponse();
    }
}