<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Controllers\ShardAccountAndFront;
use phpDocumentor\Reflection\Types\Parent_;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 08/10/17
 * Time: 09:19 م
 */
class FrontBase extends Controller
{
    use ShardAccountAndFront;

    public function __construct()
    {
        self::setNotificationShared();
        parent::__construct();
    }
}