<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Jlib\UI\ControllerHelper\ControllerHelper;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ControllerHelper;

    public $scope;
    public $module;

    public function __construct()
    {
        if (!\App::runningInConsole()) {
            $this->scope = self::getScope();
            $this->module = self::getModule();
        }
    }
}
