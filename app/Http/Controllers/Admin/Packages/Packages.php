<?php

namespace App\Http\Controllers\Admin\Packages;

use App\Http\Controllers\Admin\Base;
use App\Models\Airlines;
use App\Models\CustomPackage;
use App\Models\Hotels;
use App\Models\InterFaces\MediaAble;
use App\Models\TravelPackage;
use App\Models\TravelPackage as Model;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 12:20 ص
 */
class Packages extends Base
{


    public function getIndex()
    {
        return parent::view(["rows" => Model::filter(request())->paginate()]);
    }

    public function create()
    {
        $airlines = Airlines::asList();
        $hotels = Hotels::asList();
        $row = new Model;

        return parent::view(compact("row", "hotels", "airlines"));
    }

    public function edit($id)
    {
        $airlines = Airlines::asList();
        $hotels = Hotels::asList();
        $row = Model::findByDecryptId($id);
        return parent::view(compact("row", "hotels", "airlines"));
    }

    public function save(Request $request)
    {


        $rules = [
            "name" => "required|max:200",
//            "destination" => "required",
            "duration_in_days" => "max:3",
            "description" => "required",
//            "hotel" => "required",
//            "airlines" => "required",
//            "video" => "url",
            "tour_type" => "max:100",
            "image" => "required|allIsImages",
        ];

        if ($request->video)
            $rules["video"] = "url";

        $this->validate($request, $rules);


        TravelPackage::customSave($request);
        flash()->success("Travel package created successfully");
        return redirect()->route("packagesIndex");

    }

    public function update(Request $request)
    {

        $rules = [
            "name" => "required|max:200",
            "destination" => "required",
            "duration_in_days" => "required|max:3",
            "description" => "required",
            "hotel" => "required",
//            "airlines" => "required",
//            "video" => "required|url",
            "tour_type" => "required|max:100",
            "image" => "required|allIsImages",
        ];
        if ($request->video)
            $rules["video"] = "url";

        $this->validate($request, $rules);

        TravelPackage::customeUpdate($request);
        flash()->success("Travel package updated successfully");
        return redirect()->route("packagesIndex");
    }

    public function delete($id)
    {
        $row = Model::findByDecryptId($id);
        if ($row->canBeDelete()) {
            $row->deleteImageFile()->delete();
            flash()->success("delete successfully");
        } else {
            flash()->error("you can not delete package, its booked for some users");
        }
        return back();
    }


    public function customPackages(Request $request)
    {
        return parent::view(["rows" => CustomPackage::Filter($request)->paginate()]);
    }

    public function makeCustomPackageDone($status, $id)
    {

        CustomPackage::findByDecryptId($id)->changeStatus(($status == "done") ? 1 : 0);
        flash()->success("custom package done");
        return back();
    }
}