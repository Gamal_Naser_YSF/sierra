<?php

namespace App\Http\Controllers\Admin\Reservations;

use App\Http\Controllers\Admin\Base;

use App\Repo\Reservations\ReservationsStatus;
use App\Repo\Reservations\ReservationsTypes;
use App\Services\Notify;
use Illuminate\Http\Request;
use App\Models\Reservation as Model;
use Jlib\Upload\UploadFacad;

class Reservations extends Base
{
    public function index(Request $request)
    {

        $data["rows"] = Model::filter($request)->paginate();
        $data["statuses"] = ReservationsStatus::getAsArray();

        return parent::view($data);
    }

    public function show($id)
    {
        $data["row"] = Model::find($id);
        $data["statuses"] = ReservationsStatus::getAsArray();
        return parent::view($data);
    }

    public function store(Request $request)
    {

//        return response()->json(["message" => "sdf", "errors" => ["sdf"=>["sdfsd","dsfsdf"],"ddddd"=>["sdfsd","dsfsdf"]]])->setStatusCode(422);
        $this->validate($request,
            [
                "uid" => "required|numeric",
                "image" => "required|image",
                "title" => "required|max:191",
                "fees" => "required|numeric",
                "type" => "required",
                "details" => "required|array",
            ]
        );


        $data = $request->only("type", "fees","title");

        if ($request->image)
            $data["image"] = UploadFacad::justUpload($request->image)->first();


        $data["user_id"] = $request->uid;
        $data["status"] = 0;
        $details = [];
        foreach ($request->details as $detail)
            if ($detail["key"])
                $details[$detail["key"]] = $detail["val"];

        $data["details"] = json_encode((object)$details);

        $reservation = Model::quickSave($data);
        Notify::notifyUserAboutNewReservation($reservation);
        flash()->success("transaction add successfully");
        return response()->json(["status" => true, "redirect_url" => route("reservations.index")]);
    }

    public function changeStatus($id, Request $request)
    {
        Model::find($id)->shaneStatuses($request->new_status);
        flash()->success("statuses changed successfully");
        return back();
    }

    public function create(Request $request)
    {

        $data["types"] = ReservationsTypes::getAsArray();

        return parent::view($data);
    }
}