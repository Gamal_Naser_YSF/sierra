<?php

namespace App\Http\Controllers\Admin\Events;

use App\Http\Controllers\Admin\Base;
use App\Models\Category;
use App\Models\Event as Model;
use App\Models\Status;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:55 م
 */
class Events extends Base
{

    public function getIndex(Request $request)
    {
//        dd(Model::filter(request())->withRelatedModels()->first()->toArray());
        return parent::view(["rows" => Model::filter($request)->paginate()->appends($request->all()), "statuses" => Status::asList()]);
    }

    public function create()
    {
        return parent::view([
            "row" => new Model,
            "categoriesList" => Category::getAsList(false),
        ]);
    }

    public function store(Validation $request)
    {
        Model::createNew($request);
        flash()->success(_t("admin.Event created successfully"));
        return redirect()->route("eventsIndex");
    }

    public function edit($id)
    {
        return parent::view(["row" => Model::findByDecryptId($id), "categoriesList" => Category::getAsList(false)]);
    }

    public function update(Validation $request)
    {
        Model::updateOne($request);
        flash()->success("Event updated successfully");
        return redirect()->route("eventsIndex");
    }

    public function delete($id)
    {
        $row = Model::findByDecryptId($id);
        if ($row->canBeDelete()) {
            $row->deleteImageFile()->delete();
            flash()->success("delete successfully");
        } else {
            flash()->error("you can not delete event, its booked for some users");
        }
        return back();
    }

    public function media()
    {

        return response()->json(["data" => Model::findByDecryptId(request()->id)->media->map(function ($item) {
            return ["file_name" => $item->file_name, "id" => Model::encrypt($item->id)];
        })]);
    }
}