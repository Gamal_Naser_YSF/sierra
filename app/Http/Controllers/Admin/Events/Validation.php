<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:20 م
 */

namespace App\Http\Controllers\Admin\Events;


use Illuminate\Http\UploadedFile;
use Illuminate\Validation\Validator;
use Jlib\Validation\BaseValidation;

class Validation extends BaseValidation
{


    protected function validatCreate()
    {
        $rules = [
            "event_name" => "required|max:245",
            "start_date" => "required|date|after:tomorrow",
            "end_date" => "required|date|after:start_date",
            "country" => "required|max:20",
            "city" => "required|max:20",
            "address" => "required|max:200",
            "tags" => "required|max:200",
            "details" => "required",
            "event_coordinator" => "required|max:202",
            "event_serial" => "required|max:200",
            "proposal_end_date" => "required",
            "organized_by" => "required|max:100",
            "event_location_lat" => "required",
            "event_location_long" => "required",
            "event_highlightes" => "required",
            "profile_picture" => "required|array|allIsImages",
        ];

        if ($this->event_website)
            $rules["event_website"] = "url|max:240";

        return $rules;
    }

    protected function currentRoute()
    {
        return "admin/events";
    }

    protected function validatEdit()
    {
        $rules = [
            "event_name" => "required|max:245",
            "start_date" => "required",
            "end_date" => "required",
            "country" => "required|max:20",
            "city" => "required|max:20",
            "address" => "required|max:200",
            "tags" => "required|max:200",
            "details" => "required",
            "event_coordinator" => "required|max:202",
            "event_serial" => "required|max:200",
            "proposal_end_date" => "required",
            "organized_by" => "required|max:100",
            "event_location_lat" => "required",
            "event_location_long" => "required",
            "event_highlightes" => "required",
        ];

        if ($this->event_website)
            $rules["event_website"] = "url|max:240";

        return $rules;
    }
}