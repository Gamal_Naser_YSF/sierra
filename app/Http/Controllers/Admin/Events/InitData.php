<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 06:50 م
 */

namespace App\Http\Controllers\Admin\Events;


use App\Models\Status;
use Illuminate\Http\Request;

class InitData
{
    public static function create(Request $request)
    {
        $data = $request->only("event_name", "start_date", "end_date", "country", "city", "address", "event_location_lat", "event_location_long", "event_coordinator", "event_serial", "proposal_end_date", "organized_by", "event_highlightes", "status_id", "category_id", "details", "event_website");
        $data["status_id"] = Status::getIdForStatus(Status::active);
        dd($data);
        return $data;
    }

    public static function edit(Request $request)
    {
        return $request->only("id", "event_name", "start_date", "end_date", "country", "city", "address", "event_location_lat", "event_location_long", "event_coordinator", "event_serial", "proposal_end_date", "organized_by", "event_highlightes", "status_id", "category_id", "details", "event_website");
    }
}