<?php
/**
 * Created by PhpStorm.
 * User: server
 * Date: 06/07/17
 * Time: 09:38 م
 */

namespace App\Http\Controllers\Admin\Roles;


use Carbon\Carbon;
use Illuminate\Http\Request;

class InitData

{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var null
     */
    private $scope;

    public function __construct(Request $request, $scope = null)
    {
        $this->request = $request;
        $this->scope = $scope;
    }

    /**
     * @return array
     */
    public function create()
    {

        return ["name" => $this->request->name, "scope" => $this->scope];
    }

    public function edit()
    {
        return ["name" => $this->request->name, "id" => $this->request->id];
    }

    /**
     * @param $row
     * @return array
     */
    public function createPermissionArray($row)
    {
        $data = [];
        $now = Carbon::now();
        foreach ($this->request->per as $listOfPermission) {
            foreach ($listOfPermission as $id) {

                $data[] = [
                    "role_id" => $row->id,
                    "permission_id" => $id,
                    "created_at" => $now,
                    "updated_at" => $now,
                ];
            }
        }


        return $data;
    }
}