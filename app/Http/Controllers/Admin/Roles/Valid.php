<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Admin\Roles;

use Jlib\Validation\BaseValidation;

/**
 * Description of Valid
 *
 * @author jooAziz
 */
class Valid extends BaseValidation
{
    protected function currentRoute()
    {
        return "admin/roles";
    }


    protected function validatCreate()
    {
        return [
            "name" => "required|unique:roles,name",
            "per" => "required|array|min:1",
        ];
    }

    protected function validatEdit()
    {
        return [
            "name" => "required|unique:roles,name," . $this->id,
            "per" => "required|array|min:1",
        ];
    }
}
