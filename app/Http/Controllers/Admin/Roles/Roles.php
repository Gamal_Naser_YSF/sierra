<?php

namespace App\Http\Controllers\Admin\Roles;

use App\Http\Controllers\Admin\Base;
use App\Models\Permission;
use App\Models\PermissionsRoles;
use App\Models\Role as Model;
use Applexicon\Utilitis\Str\Str;
use Illuminate\Http\Request;
use PhpParser\Node\Expr\AssignOp\Mod;

/**
 * Created by PhpStorm.
 * User: server
 * Date: 06/07/17
 * Time: 08:47 م
 */
class Roles extends Base
{
    public function __construct()
    {
        parent::__construct();
//        dd($this->scope);
        Model::setScope($this->scope);
    }

    public function getIndex()
    {
        $rows = (new Model())->getAll();
        return parent::view(compact("rows"));
    }

    public function create()
    {
        $row = new Model;
        $permissions = Permission::allGrouped($this);
        return parent::view(compact("row", "permissions"));
    }

    public function store(Valid $request)
    {
        $data = new InitData($request, $this->scope);
        $row = Model::quickSave($data->create());
        PermissionsRoles::insert($data->createPermissionArray($row));
        flash()->success(_t('admin.created successfully'));
        return redirect()->route("groupsIndex");
    }

    public function edit($id)
    {
        $row = Model::findByDecryptId($id);
        $oldPermissions = $row->reformatPermissions()->toJson();
//        dd($oldPermissions);
        $permissions = Permission::allGrouped($this);
//        dd(compact("row", "permissions", "oldPermissions"));
        return parent::view(compact("row", "permissions", "oldPermissions"));
    }

    public function update(Valid $request)
    {

        $data = new InitData($request);
        $row = Model::quickUpdate($data->edit());
        (new  PermissionsRoles)->updatePermissionsForRole($row->id, $data->createPermissionArray($row));
        flash()->success(_t('admin.updatd successfully'));
        return redirect()->route("groupsIndex");
    }


    public function getDelete($id)
    {
        Model::findByDecryptId($id)->deleteWithRelated();
        flash()->success(_t('admin.deleted successfully'));
        return redirect()->route("groupsIndex");

    }
}
