<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Notification;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 16/10/17
 * Time: 08:55 م
 */
class Base extends Controller
{
    public function __construct()
    {
        view()->share("adminNotifications", Notification::getLatestForAdmin());
        parent::__construct();
    }
}