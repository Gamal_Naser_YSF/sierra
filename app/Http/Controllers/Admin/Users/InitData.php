<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:23 م
 */

namespace App\Http\Controllers\Admin\Users;


use Illuminate\Http\Request;


class InitData
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function create()
    {
        $data = $this->request->only("name", "email", "phone");
        $data["password"] = \Hash::make($this->request->password);
        return $data;
    }

    public function edit()
    {
        $data = $this->request->only("name", "description", "category_id", "id");

//        if ($this->request->file("image"))
//            $data["image"] = UploadFacad::justUpload($this->request->file("image"))->first();

        return $data;
    }
}