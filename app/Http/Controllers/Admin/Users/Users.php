<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\Base;
use App\Models\Category;
use App\Models\User as Model;
use Jlib\Utilitis\Str\Str;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 01:01 ص
 */
class Users extends Base
{
    public function getIndex()
    {
        return parent::view(["rows" => Model::orderBy("created_at", "desc")->paginate()]);
    }

    public function getInterest($id)
    {
        $row = Model::findByDecryptId($id);
        $interested = $row->getInterested()->pluck("id")->toArray();
        $categories = Category::pluck("name", "id");
        return parent::view(compact("interested", "row", "categories"));
    }

    public function postInterest($id)
    {
        $interested = (request()->interested) ? request()->interested : [];
        Model::findByDecryptId($id)->updateInterestes($interested);
        flash()->success("admin.update interested successfully");
        return redirect()->route("usersIndex");
    }


    public function create()
    {
        return parent::view([
            "row" => new Model()
        ]);
    }

//
    public function store(Validation $validation)
    {
        $data = new InitData($validation);
        Model::quickSave($data->create())->updateMedia($validation);
        flash()->success(_t("admin." . Str::singular($this->module) . " created successfully"));
        return redirect()->to("$this->scope/$this->module");
    }
//
//    public function delete($id)
//    {
//        $res = Model::deleteIfNotHaveOtherCategories($id);
//        flash()->{$res->getStatus()}($res->getMessage());
//        return back();
//    }
//
    public function edit($id)
    {
        return parent::view([
            "row" => Model::findByDecryptId($id),
        ]);
    }

    public function fetch($id)
    {
        $row = Model::find($id);;
        return response()->json(($row) ? ["status" => true, "data" => $row->only("name", "avatar", "address", "email", "phone")] : ["status" => false]);


    }
//
//    public function update(Validation $request)
//    {
//        $data = new InitData($request);
//        Model::quickUpdate($data->edit())->updateMedia($request);
//        flash()->success(_t("admin." . Str::singular($this->module) . " updated successfully"));
//        return redirect()->route("categoriesIndex");
//    }
//

}