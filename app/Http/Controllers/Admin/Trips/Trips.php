<?php

namespace App\Http\Controllers\Admin\Trips;

use App\Http\Controllers\Admin\Base;
use App\Models\Trip;
use Illuminate\Http\Request;
use App\Models\Trip as Model;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 02:32 م
 */
class Trips extends Base
{
    public function customTrips(Request $request)
    {
        return parent::view(["rows" => Model::filter($request)->paginate()]);
    }

    public function tripDetails($id)
    {
        $row = Model::findByDecryptId($id);
        $data["trip"] = $row;
        $data["tripInfo"] = $row->tripInfo;
//        dd($row, @$data);
        return parent::view($data);
    }

    public function makeTripDone($id)
    {
        Trip::findByDecryptId($id)->MakeItDone();
        flash()->success("trip done successfully");
        return redirect()->route("UsersTripsIndex");
    }
}