<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Admin\Auth;

/**
 * Description of Validator
 *
 * @author jooAziz
 */
use App\Http\Validations\BaseValidation;

class Validator extends BaseValidation
{
    protected $myUrl = "admin/auth/";

    public function rules()
    {
        if ($this->is($this->myUrl . "reset-password"))
            return $this->resetPassword();
        return parent::rules();
    }

    private function resetPassword()
    {
        return [
            "password" => "required|min:8"
        ];

    }

    protected function validaCreate()
    {

    }

    protected function validaEdit()
    {

    }


}
