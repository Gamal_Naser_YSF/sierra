<?php

namespace App\Http\Controllers\Admin\Auth;

//use \App\Http\Controllers\Admin\Base as BaseController;
use App\Http\Controllers\Admin\Base;
use App\Models\Admin as Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Authanticator;
use App\Http\Controllers\Admin\Auth\Validator as Valid;
use Jlib\Auth\Auther;

class Auth extends Base
{

    public function __construct(Model $model)
    {
        parent::__construct();
    }

    public function getIndex()
    {
        return redirect()->to($this->scope . '/' . $this->module . '/login');
    }

    public function getLogin()
    {
        return parent::view();
    }

    public function postLogin(Request $request)
    {
        $auth = AuthLogic::instance(new Model, $request, 'email')->login();
        flash()->{$auth->getLevel()}($auth->getMessage());

        if ($auth->canLogin()) {
//            $this->getAuth()->update_user($auth->getRow());
            Auther::setUser($auth->getRow());
            return redirect()->to('admin');
        }
        return back();
    }

    public function getRegister()
    {
        return redirect('admin/auth/login');
    }

    public function postRegister(Request $request)
    {
        return redirect()->route("adminLogin");
    }

    public function getForgotPassword()
    {
        return parent::view();
    }

    public function postForgotPassword(Request $request)
    {
        if (AuthLogic::instance(new Model, $request)->forgotPassword()) {
            flash()->success("please check your email");
            return redirect()->route("adminLogin");
        }
        flash()->error("invalid or un registered email");
        return back();
    }

    public function getLogout()
    {
        Auther::logout();
        return redirect()->route("adminLogin");
    }

    public function getResetPassword(Request $request)
    {
        if ($request->_t) {
            if ($row = Model::whereToken($request->_t)->first()) {
                $row->resetTokensFailds();
                if (!AuthLogic::isExpired($row->token_expire)) {
                    flash()->success("plese insert your new password");
                    return parent::view(compact('row'));
                }
            }
        }
        flash()->error("invalid or old token");
        return redirect()->to('admin/auth/forgot-password');
    }

    public function postResetPassword(Valid $request)
    {
        $row = Model::findOrFail($request->id)->setNewPassword($request);
        flash()->success("new password set ,please type it to login");
        return redirect()->route("adminLogin");
    }

}
