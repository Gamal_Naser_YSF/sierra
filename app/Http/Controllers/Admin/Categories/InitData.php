<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:23 م
 */

namespace App\Http\Controllers\Admin\Categories;


use Illuminate\Http\Request;
use Jlib\Upload\UploadFacad;

class InitData
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function create()
    {
        $data = $this->request->only("name", "description", "type", "category_id");
        $data["show_in_home_page"] = ($this->request->has("show_in_home_page")) ? 1 : 0;
        return $data;
    }

    public function edit()
    {
        $data = $this->request->only("name", "description", "type", "category_id", "id");
        $data["show_in_home_page"] = ($this->request->has("show_in_home_page")) ? 1 : 0;

        return $data;
    }
}