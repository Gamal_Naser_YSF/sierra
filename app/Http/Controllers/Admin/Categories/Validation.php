<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:20 م
 */

namespace App\Http\Controllers\Admin\Categories;


use Jlib\Validation\BaseValidation;

class Validation extends BaseValidation
{

    protected function validatCreate()
    {
        return [
            "name" => "required|max:200|unique:categories,name",
            "description" => "required"
        ];
    }

    protected function currentRoute()
    {
        return "admin/categories";
    }

    protected function validatEdit()
    {
        return [
            "name" => "required|max:200|unique:categories,name," . $this->id,
            "description" => "required"
        ];
    }
}