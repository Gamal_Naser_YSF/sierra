<?php

namespace App\Http\Controllers\Admin\Categories;

use App\Http\Controllers\Admin\Base;
use App\Models\Category as Model;
use App\Models\CategoryTypes;
use Jlib\Utilitis\Str\Str;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 01:01 ص
 */
class Categories extends Base
{
    public function getIndex()
    {
        return parent::view(["rows" => Model::getIndexList(request())]);
    }

    public function create()
    {
        return parent::view([
            "row" => new Model(),
            "categoriesType" => CategoryTypes::pluck("name", "id"),
            "categories" => Model::getAsList()
        ]);
    }

    public function store(Validation $validation)
    {
        $data = new InitData($validation);
        Model::quickSave($data->create())->updateMedia($validation);
        flash()->success(_t("admin." . Str::singular($this->module) . " created successfully"));
        return redirect()->route("categoriesIndex");
    }

    public function delete($id)
    {
        $res = Model::deleteIfNotHaveOtherCategories($id);
        flash()->{$res->getStatus()}($res->getMessage());
        return back();
    }

    public function edit($id)
    {
        return parent::view([
            "row" => Model::findByDecryptId($id),
            "categoriesType" => CategoryTypes::pluck("name", "id"),
            "categories" => Model::getAsList()
        ]);
    }

    public function update(Validation $request)
    {
        $data = new InitData($request);
        Model::quickUpdate($data->edit())->updateMedia($request);
        flash()->success(_t("admin." . Str::singular($this->module) . " updated successfully"));
        return redirect()->route("categoriesIndex");
    }

    public function relatedUsers($id)
    {
        $rows = Model::findByDecryptId($id)->users()->paginate();
        return parent::view(compact("rows"));
    }

    public function changeStatus()
    {
        Model::find(request()->id)->changeShowStatus();
        return response()->json(["statsu" => true]);
    }
}