<?php

namespace App\Http\Controllers\Admin\Dashboard;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dashboard
 *
 * @author jooAziz
 */
use App\Http\Controllers\Admin\Base as BaseController;
use App\Http\Controllers\Lang;
use App\Models\Admin;
use App\Models\Course;
use App\Models\Entity;
use App\Models\Gaurdian;
use App\Models\Level;
use App\Models\PromoCode;
use App\Models\Requests;
use App\Models\Session;
use App\Models\Student;
use App\Models\Teacher;

class Dashboard extends BaseController
{

    public function getIndex()
    {
        return parent::view();
    }

    public function getLang($l)
    {
        Lang::getAuth($this->getAuth())->changTo($l);
        return back();
    }

}
