<?php
/**
 * Created by PhpStorm.
 * User: server
 * Date: 27/08/17
 * Time: 04:14 م
 */

namespace App\Http\Controllers\Admin\Dashboard;


use Illuminate\Database\Eloquent\Collection;

class StatisticService
{
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function splitKysVals()
    {
        $values = $labels = [];
        foreach ($this->collection as $v) {
            $labels[] = $v['name'];
            $values[] = $v['count'];
        }
      return new Collection(["labels" => $labels, "values" => $values]);
    }
}