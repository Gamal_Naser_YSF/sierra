<?php

namespace App\Http\Controllers\Admin\Notifications;

use App\Http\Controllers\Admin\Base;
use App\Models\Notification as Model;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 01:01 ص
 */
class Notifications extends Base
{
    public function getIndex()
    {
        return parent::view(["rows" => Model::filter(request())->paginate()->appends(request()->all())]);
    }

    public function seen($id)
    {
        Model::findByDecryptId($id)->setSeen();
        return back();
    }


}