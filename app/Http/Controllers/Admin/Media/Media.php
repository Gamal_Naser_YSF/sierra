<?php

namespace App\Http\Controllers\Admin\Media;

use App\Http\Controllers\Admin\Base;
use App\Models\Media as Model;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/11/17
 * Time: 12:59 م
 */
class Media extends Base
{
    public function delete($id)
    {
        Model::findByDecryptId($id)->delete();
        return response()->json(["status" => true]);
    }
}