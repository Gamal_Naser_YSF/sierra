<?php

namespace App\Http\Controllers\Admin\Plans;

use App\Http\Controllers\Admin\Base;
use App\Models\Plan;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 12:11 ص
 */
class Plans extends Base
{
    public function index()
    {
        return parent::view(["rows" => Plan::paginate()]);
    }

    public function create()
    {
        return parent::view(["row" => new Plan]);
    }

    public function edit($id)
    {
        return parent::view(["row" => Plan::findByDecryptId($id)]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            "name" => "required|max:190",
            "description" => "required|max:190",
            "price" => "required|max:190",
            "discount" => "required|numeric|max:100",
            "package_type" => "required|max:190",
            "start_date" => "required|max:190",
            "end_date" => "required|max:190",
        ]);

        $data = $request->only("name", "description", "price", "discount", "package_type", "start_date", "end_date");

        Plan::quickSave($data);
        return redirect()->to("/admin/plans");
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            "name" => "required|max:190",
            "description" => "required|max:190",
            "price" => "required|max:190",
            "discount" => "required|numeric|max:100",
            "package_type" => "required|max:190",
            "start_date" => "required|max:190",
            "end_date" => "required|max:190",
        ]);

        $data = $request->only("id", "name", "description", "price", "discount", "package_type", "start_date", "end_date");

        Plan::quickUpdate($data);
        return redirect()->to("/admin/plans");
    }

    public function delete($id)
    {
//        dd($id);
        Plan::findByDecryptId($id)->delete();
        flash()->success("plan delete successfully");
        return back();
    }
}