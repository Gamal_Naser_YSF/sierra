<?php

namespace App\Http\Controllers\Admin\Emails;

use App\Http\Controllers\Admin\Base;
use App\Models\EmailLog as Model;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 12:11 ص
 */
class Emails extends Base
{
    public function index()
    {
        return parent::view(["rows" => Model::filter(request())->paginate()]);
    }

    public function resend(Request $request)
    {
       Model::findOrFail($request->email)->reSend();
       return back();
    }

}