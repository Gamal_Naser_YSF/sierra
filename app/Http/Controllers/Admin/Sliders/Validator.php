<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 13/12/17
 * Time: 12:36 ص
 */

namespace App\Http\Controllers\Admin\Sliders;


use Jlib\Validation\BaseValidation;

class Validator extends BaseValidation
{


    protected function validatCreate()
    {
        return [
            "image" => "required",
            "text" => "required|max:190",
            "link" => "required|max:190|url",
            "link_type" => "required",
        ];
    }

    protected function currentRoute()
    {
        return "admin/sliders";
    }

    protected function validatEdit()
    {
        return [
            "text" => "required|max:190",
            "link" => "required|max:190|url",
            "link_type" => "required",
        ];
    }
}