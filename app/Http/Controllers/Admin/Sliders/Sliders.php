<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 12/12/17
 * Time: 11:13 م
 */

namespace App\Http\Controllers\Admin\Sliders;


use Illuminate\Http\Request;

use App\Http\Controllers\Admin\Sliders\SlidersServiceLayer as ServiceLayer;
use App\Http\Controllers\Admin\Base;

class Sliders extends Base
{

    public function index(Request $request)
    {
        $data = ["rows" => ServiceLayer::all($request)];

        return parent::view($data);
    }

    public function create()
    {
        return parent::view(["row" => ServiceLayer::getModelInstance(), "types" => ServiceLayer::getTypes()]);
    }

    public function edit($id)
    {
        return parent::view(["row" => ServiceLayer::getModelInstance($id), "types" => ServiceLayer::getTypes()]);
    }

    public function store(Validator $validator)
    {
        ServiceLayer::createNew($validator);

        return redirect()->route("sliders.index");
    }

    public function update(Validator $validator)
    {

        ServiceLayer::update($validator);
        return redirect()->route("sliders.index");
    }

    public function delete($id)
    {
        ServiceLayer::deleteRecord($id);

        return redirect()->route("sliders.index");
    }

    public function reorder(Request $request)
    {
        foreach ($request->rows as $order => $id) {
            $model = ServiceLayer::getModelInstance($id);
            $model->order = $order;
            $model->save();
        }

    }

}