<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 12/12/17
 * Time: 11:19 م
 */

namespace App\Http\Controllers\Admin\Sliders;


use Illuminate\Http\Request;
use App\Models\Slider as Model;
use Jlib\Upload\UploadFacad;


class SlidersServiceLayer
{


    public static function all(Request $request = null)
    {
        return Model::all();
    }

    /**
     * @param null $id
     * @return Model
     */
    public static function getModelInstance($id = null)
    {
        return (is_null($id)) ? new Model : Model::find($id);
    }

    public static function getTypes($i = null)
    {
        return Model::getTypes($i);

    }


    public static function createNew(Request $request)
    {
        $data = $request->only("text", "link", "link_type");


        if ($request->file("image"))
            $data["image"] = UploadFacad::justUpload($request->file("image"))->first();

        flash()->success("slider created successfully");
        Model::quickSave($data);
    }

    public static function deleteRecord($id)
    {
        $model = Model::quickDelete($id);
    }

    public static function update(Request $request)
    {

        $data = $request->only("text", "link", "link_type", "id");


        if ($request->file("image"))
            $data["image"] = UploadFacad::justUpload($request->file("image"))->first();

        flash()->success("slider updated successfully");
        Model::quickUpdate($data);


    }

}