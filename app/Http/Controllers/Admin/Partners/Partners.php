<?php

namespace App\Http\Controllers\Admin\Partners;

use App\Http\Controllers\Admin\Base;
use App\Models\Partner as Model;
use App\Models\CategoryTypes;
use Jlib\Utilitis\Str\Str;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 01:01 ص
 */
class Partners extends Base
{
    public function index()
    {
        return parent::view(["rows" => Model::filter(request())->paginate()]);
    }

    public function create()
    {
        return parent::view([
            "row" => new Model()
        ]);
    }

    public function store(Validation $validation)
    {
        $data = new InitData($validation);
        Model::quickSave($data->create())->updateMedia($validation);
        flash()->success(_t("admin." . Str::singular($this->module) . " created successfully"));
        return redirect()->route("partners.index");
    }

    public function delete($id)
    {
        $res = Model::findByDecryptId($id);
        if ($res) {
            $res->delete();
            $res->deleteImageFile();
        }
        return back();
    }

    public function edit($id)
    {
        return parent::view([
            "row" => Model::findByDecryptId($id),
        ]);
    }

    public function update(Validation $request)
    {
        $data = new InitData($request);
        Model::quickUpdate($data->edit())->updateMedia($request);
        flash()->success(_t("admin." . Str::singular($this->module) . " updated successfully"));
        return redirect()->route("partners.index");
    }

    public function relatedUsers($id)
    {
        $rows = Model::findByDecryptId($id)->users()->paginate();
        return parent::view(compact("rows"));
    }

    public function changeStatus()
    {
        Model::find(request()->id)->changeShowStatus();
        return response()->json(["statsu" => true]);
    }
}