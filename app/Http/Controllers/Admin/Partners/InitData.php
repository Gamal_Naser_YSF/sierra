<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:23 م
 */

namespace App\Http\Controllers\Admin\Partners;


use Illuminate\Http\Request;
use Jlib\Upload\UploadFacad;

class InitData
{
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function create()
    {
        $data = $this->request->only("name");

        return $data;
    }

    public function edit()
    {
        $data = $this->request->only("name","id");
        return $data;
    }
}