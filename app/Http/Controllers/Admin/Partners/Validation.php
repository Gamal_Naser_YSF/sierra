<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 09:20 م
 */

namespace App\Http\Controllers\Admin\Partners;


use Jlib\Validation\BaseValidation;

class Validation extends BaseValidation
{

    protected function validatCreate()
    {
        return [
            "name" => "required",
            "logo" => "required"
        ];
    }

    protected function currentRoute()
    {
        return "admin/partners";
    }

    protected function validatEdit()
    {
        return [
            "name" => "required",
//            "logo" => "required"
        ];
    }
}