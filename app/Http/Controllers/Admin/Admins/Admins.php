<?php

namespace App\Http\Controllers\Admin\Admins;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Companies
 *
 * @author jooAziz
 */

use App\Http\Controllers\Admin\Base;
use App\Models\Admin as Model;
use App\Models\Admin;
use App\Models\AdminsRoles;
use App\Models\Permission;
use App\Models\PermissionsRoles;
use App\Models\Role;
use App\Repo\Roles\RoleReformation;
use App\Services\Roles;
use Applexicon\Utilitis\Encryption\UrlIdEncoder;
use Applexicon\Utilitis\Str\Str;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Http\Controllers\Admin\Admins\Validator as Valid;
use Jlib\Auth\Auther;

class Admins extends Base
{
    public function __construct()
    {
        parent::__construct();
        Role::setScope($this->scope);
    }

    public function getIndex(Request $request)
    {
        return parent::view(
            ['rows' => (new Model)->fillter($request)->whereNotIn("id", [Auther::getUser()->id])->paginate()]
        );
    }

    public function unactivated($id)
    {
        Model::findByDecryptId($id)->deactive();
        return back();
    }

    public function active($id)
    {
        Model::findByDecryptId($id)->active();
        return back();
    }

    public function create()
    {
        return parent::view(['row' => new Model]);
    }

    public function store(Valid $request)
    {
        Model::quickSave(InitData::create($request));
        flash()->success(_t('admin.created successfully'));
        return redirect()->route("adminsIndex");
    }

    public function edit($id)
    {
        return parent::view(['row' => Model::findByDecryptId($id)]);
    }

    public function update(Valid $request)
    {
        Model::quickUpdate(InitData::editAccount($request));
        flash()->success(_t('admin.Edited successfully'));
        return redirect()->route("adminsIndex");
    }


    public function getChangeUserPassword($id)
    {
        return parent::view(['row' => Model::findByDecryptId($id)]);
    }

    public function postChangeUserPassword(Valid $request, $id)
    {
        Model::findByDecryptId($id)->setNewPassword($request);
        flash()->success(_t("admin.update successfully"));
        return back();
    }
//
//    public function postChangePassword(Valid $request, $id = null)
//    {
//        dd($request->all());
//        $id = $this->getAuth()->user()->id;
//        $res = AdminModifires::validatChangPassword($request->get('old-password'), $request->get('new-password'), Model::find($id));
//        flash()->{$res->status}(_t($res->message));
//        return back();
//    }
//
//    public function getDelete($id)
//    {
//        (new Model)->find($id)->deactive();
//        flash()->success(_t('admin.deactived successfully'));
//        return redirect()->route("adminsIndex");
//    }
//
//    public function getActive($id)
//    {
//        (new Model)->find($id)->active();
//        flash()->success(_t('admin.actived successfully'));
//        return redirect()->route("adminsIndex");
//    }
//
//    public function getChangePassword($id)
//    {
//
//        return parent::view(['row' => Model::findByDecryptId($id)]);
//    }
//
    public function getRoles()
    {

        $rolesRepo = new RoleReformation((new Role())->getAll());
        $roles = $rolesRepo->lists();
        $permissionsData = $rolesRepo->getPermissions()->toJson();
        $permissions = Permission::allGrouped($this);
//        dd($permissions);
        $userId = _dec(request()->id);

        $adminPermissions = new Roles(AdminsRoles::getAllForAdmin($userId));
        $oldPermissions = $adminPermissions->formatToPermissionsArray()->toJson();

//        dd($userId,$adminPermissions,$oldPermissions,request()->all());

        return parent::view(compact("roles", "permissionsData", "permissions", "oldPermissions"));

    }

    public function postRoles(Request $request)
    {

        $ids = (new InitData($request))->getPermissonsIds();

        if (!empty($ids)) {

            Model::findByDecryptId($request->id)
                ->setScope($this->scope)
                ->deleteOldPermissions()
                ->insertNewPermissions($ids);
        }

        return redirect()->route("adminsIndex");
    }
//
//    public function status($id)
//    {
//        $row = Model::findOrFail($id);
//        $row->active = !$row->active;
//        $row->save();
//        return back();
//    }
//
//    public function getProfile()
//    {
////        $data['row']=
//        return parent::view(["row" => Admin::find($this->getAuth()->user()->id)]);
//    }
//
//    public function editadmin()
//    {
////        $data['row']=
//        return parent::view(["row" => Admin::find($this->getAuth()->user()->id)]);
//    }
//
//    public function editProfile(Valid $request)
//    {
//        Model::quickUpdate(InitData::editaccount($request));
//        flash()->success(_t('admin.Profile has Edited successfully'));
//        return redirect()->to('admin/' . $this->module . '/profile');
//    }

}
