<?php

namespace App\Http\Controllers\Admin\Admins;

use Jlib\Validation\BaseValidation;

class Validator extends BaseValidation
{
    protected function currentRoute()
    {
        return "admin/admins";
    }

    public function rules()
    {
        if ($this->is($this->currentRoute()))
            return $this->validaCreate();

        if ($this->is($this->currentRoute() . '/change-password/*'))
            return $this->changePassword();

        if ($this->is($this->currentRoute() . '/change-password'))
            return $this->changePassword();

        if ($this->is($this->currentRoute() . '/change-user-password/*'))
            return $this->changeUserPassword();

        if ($this->is($this->currentRoute() . '/editadmin'))
            return self::editAccount($this->id);


        if ($this->is($this->currentRoute() . '/' . $this->id))
            return self::editAccount($this->id);


        return parent::rules();
    }

    private function changeUserPassword()
    {
        return [
            "password" => "required|min:8"
        ];
    }

    protected function validaCreate()
    {
        return [
            "full_name" => "required",
            "email" => "required|email|unique:admins,email",
            "phone" => "required|isPhone|unique:admins,phone",
            "username" => "required|unique:admins,username",
            "password" => "required|min:8",
        ];
    }

    protected function validaEdit()
    {

        return self::editAccount($this->id);
    }

    /**
     * @int $id
     * @return array
     */
    private static function editAccount($id)
    {

        return [
            "full_name" => "required",
            "username" => "required|unique:admins,username," . $id,
            "email" => "required|email|unique:admins,email," . $id,
            "phone" => "required|isPhone|unique:admins,phone," . $id,
        ];
    }


    private function changePassword()
    {

        return [
            "old-password" => "required|min:8",
            "new-password" => "required|min:8",
            "confirm-new-password" => "required|min:8|same:new-password"
        ];
    }


    protected function validatCreate()
    {

        return [
            "full_name" => "required",
            "password" => "required|min:8",
            "email" => "required|email|unique:admins,email",
            "phone" => "required|isPhone|unique:admins,phone",
        ];
    }

    protected function validatEdit()
    {
        return [
            "full_name" => "required",
            "email" => "required|email|unique:admins,email," . $this->id,
            "phone" => "required|isPhone|unique:admins,phone," . $this->id,
        ];
    }
}
