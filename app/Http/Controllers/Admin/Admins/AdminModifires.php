<?php

namespace App\Http\Controllers\Admin\Admins;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


class AdminModifires {

    public static function validateUpdateAdmin($controller, $row) {
        if ($row) {
            $controller->getAuth()->update_user($row);
            $rt['status'] = 'success';
            $rt['message'] = __('admin.Admin has been edited successfully');
        } else {
            $rt['status'] = 'error';
            $rt['message'] = __('admin.Failed to save');
        }

        return (object) $rt;
    }

    public static function validatChangPassword($oldPass, $newPass, $row) {
//        dd($oldPass, $newPass, $row);
        $rt = [];
        if (!\Hash::check($oldPass, @$row->password)) {
            $rt['status'] = "error";
            $rt['message'] = __('admin.Password entered not matched with Current Password');
        } else {
            $row->password = \Hash::make($newPass);
            if ($row->save()) {
                $rt['status'] = "success";
                $rt['message'] = __('admin.Password Changed successfully');
            } else {
                $rt['status'] = "error";
                $rt['message'] = __('admin.can`t change password');
            }
        }
        return (object) $rt;
    }

}
