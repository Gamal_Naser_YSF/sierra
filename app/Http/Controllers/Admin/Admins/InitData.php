<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Admin\Admins;

use Illuminate\Http\Request;

/**
 * Description of initData
 *
 * @author jooaziz
 */
class InitData
{
    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {

        $this->request = $request;
    }

    public function getPermissonsIds()
    {
        if (!$this->request->per) return [];

        $ids = [];
        foreach ($this->request->per as $per)
            foreach ($per as $id)
                $ids[] = $id;
        return $ids;
    }

    public static function create(Request $request)
    {
        $data = $request->only("email", "phone","full_name");
        $data['password'] = \Hash::make($request->password);
        $data['is_super_admin'] = ($request->is_super_admin) ? 1 : 0;
        $data['avatar'] = "";
        return $data;
    }

    public static function editAccount(Request $request, $id = null)
    {

        $data = $request->only(['phone', 'username', 'email', "id", 'full_name']);

        $data['is_super_admin'] = ($request->is_super_admin) ? 1 : 0;
        if ($request->file("avatar"))
            $data["image"] = UploadFacad::justUpload($request->file("image"))->first();

        if ($id)
            $data['id'] = $id;


        return $data;
    }


}
