<?php

namespace App\Http\Controllers\Admin\Bookings;

use App\Http\Controllers\Admin\Base;
use App\Models\Booking;
use App\Models\Booking as Model;
use App\Models\TravelPackagesBooking;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/11/17
 * Time: 02:05 م
 */
class Bookings extends Base
{
    public function eventBookings()
    {
        return parent::view(["statuses" => Booking::getStatuses(), "rows" => Booking::filter(request())->paginate()]);
    }

    public function changeStatus($id, Request $request)
    {
        Booking::findOrFail($id)->changeStatus($request->status);
        return back();
    }

    public function packagesBookings()
    {
        return parent::view(["rows" => TravelPackagesBooking::filter(request())->paginate()]);
    }

    public function seen($id)
    {
        Model::findByDecryptId($id)->setSeen();
        flash()->success("Booking set to seen");
        return back();
    }

    public function packageSeen($id)
    {
        TravelPackagesBooking::findByDecryptId($id)->setSeen();
        flash()->success("Booking set to seen");
        return back();
    }
}