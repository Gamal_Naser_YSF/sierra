<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18/12/17
 * Time: 07:04 Ù…
 */

namespace App\Http\Controllers\Admin\Configs;


use Illuminate\Http\Request;
use Jlib\Upload\UploadFacad;
use App\Models\Config as Model;

class ConfigsServiceLayer
{


    public static function all(Request $request = null)
    {
        return Model::filter($request)->all();
    }

    public static function updateForm(Request $request)
    {
        foreach (self::getFields() as $field) {
            if ($field->type == "file") {
                if ($request->{$field->key})
                    $field->value = UploadFacad::justUpload($request->{$field->key})->first();
            } else {
                $field->value = $request->{($field->key)};
//dd($field,$field->type, $request->{$field->key},$field->key,$request->all());
            }
            $field->save();
        }

    }

    private static function getFields()
    {
        return Model::all();
    }


}