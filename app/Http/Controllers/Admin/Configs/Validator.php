<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 13/12/17
 * Time: 12:36 Øµ
 */

namespace Modules\Admin\Configs;


use Hive\Controllers\Validate\FormValidator;

class Validator extends FormValidator
{
    public function getModule()
    {
        return "admin/configs";
    }


    public function store(): array
    {
        return [
            "key" => "required|unique:configs|no_spaces",

        ];
    }

    public function update(): array
    {
        return [
            "configs" => "required|no_spaces|unique:configs,key," . $this->id,
        ];
    }

}