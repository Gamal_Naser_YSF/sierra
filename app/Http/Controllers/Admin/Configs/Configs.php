<?php

namespace App\Http\Controllers\Admin\Configs;

use App\Http\Controllers\Admin\Base;
use Illuminate\Http\Request;
use App\Http\Controllers\Admin\Configs\ConfigsServiceLayer as ServiceLayer;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 18/12/17
 * Time: 06:55 Ù…
 */
class Configs extends Base
{
    private static $_data;

    public function index(Request $request)
    {
        return parent::view(["rows" => ServiceLayer::all($request)]);
    }



    public function store(Request $validator)
    {

        $res = ServiceLayer::updateForm($validator);
        flash()->success(__("admin.update susccessfully"));
        return redirect()->route("configs.index");
    }

}