<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/11/17
 * Time: 05:21 م
 */

namespace App\Services;


use App\Models\Event;
use Illuminate\Database\Eloquent\Collection;

class EventFormater
{
    /**
     * @var Collection
     */
    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function format($formatType = null)
    {
        switch ($formatType) {
            case "list":
                return $this->formatAsList();
            case "calender":
                return $this->formatAsCalender();
            default:
                return $this->nonFormated();
        }
    }

    private function formatAsCalender()
    {
        $newCollection = new Collection();
        $this->collection->each(function (Event $item) use (&$newCollection) {
            $encKey = $item->encyrptId();
            $newCollection->push([
                "id" => $encKey,
                "name" => $item->event_name,
                "startdate" => $item->start_date,
                "enddate" => $item->end_date,
                "starttime" => "00:00",
                "endtime" => "23:59",
                "color" => "#3f9c1a",
                "url" => "/event-details/$encKey"
            ]);
        });
        return $newCollection;
    }

    private function formatAsList()
    {
        $newCollection = new Collection();
        $this->collection->each(function (Event $item) use (&$newCollection) {
            $encKey = $item->encyrptId();
            $newCollection->put($encKey, [
                "id" => $encKey,
                "title" => $item->event_name,
                "desc" => $item->details,
                "day" => $item->getDay(),
                "mon" => $item->getMonth(),
                "image" => "/upload/" . $item->getFirstImageName()]);
        });
        return $newCollection;
    }

    private function nonFormated()
    {
        return $this->collection;
    }
}