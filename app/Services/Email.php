<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/03/18
 * Time: 08:46 م
 */

namespace App\Services;


use App\Models\EmailLog;


class Email
{
    public static function sendWithLog($template, $subject, $data, $userEmail, $userName = "Guest")
    {
        return (new static($template, $subject, $data, $userEmail, $userName))->send()->log();
    }

    private $template;
    private $subject;
    /**
     * @var array|object
     */
    private $data;
    private $userEmail;

    /**
     * @var string
     */
    private $userName;
    /**
     * @var bool
     */
    private $status;

    public function __construct($template, $subject, $data, $userEmail, $userName = "Guest")
    {
        $this->template = $template;
        $this->subject = $subject;
        $this->data = $data;
        $this->userEmail = $userEmail;
        $this->userName = $userName;
    }


    public function log()
    {
        EmailLog::createLog($this->getStatus(), $this->getTemplate(), $this->getSubject(), json_encode($this->getData()), $this->getUserEmail(), $this->getUserName());
        if (!$this->getStatus())
            Notify::notifiyErrorToAdmin("Sending Email Error", "sending Email to user [" . $this->getUserName() . "] with subject [" . $this->getSubject() . "]  was not send because an error you can send it again from emails section");

    }

    public function send()
    {
        try {
            @\Mail::send($this->getTemplate(), ["data" => (array)$this->getData()], function ($mail) {
                $mail->from(env('MAIL_FROM_EMAIL'), env('MAIL_FROM_NAME'));
                $mail->to($this->getUserEmail(), $this->getUserName())->subject($this->getSubject());
            });
            $this->setStatus(EmailLog::sent);
        } catch (\Exception $exception) {
            $this->setStatus(EmailLog::fails);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param mixed $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getUserEmail()
    {
        return $this->userEmail;
    }

    /**
     * @param mixed $userEmail
     */
    public function setUserEmail($userEmail)
    {
        $this->userEmail = $userEmail;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     */
    public function setUserName(string $userName)
    {
        $this->userName = $userName;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

}