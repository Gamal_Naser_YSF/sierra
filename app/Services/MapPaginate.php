<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 31/03/18
 * Time: 03:15 م
 */

namespace App\Services;


use Illuminate\Pagination\LengthAwarePaginator;

class MapPaginate
{
    public static function get($panginat, $map)
    {

        $panginatTransformed = $panginat->getCollection()->map(function ($t) use ($map) {
            return $map($t);
        });

        return new LengthAwarePaginator(
            $panginatTransformed->toArray(),
            $panginat->total(),
            $panginat->perPage(),
            $panginat->currentPage(), ['path' => request()->url(), 'query' => ['page' => $panginat->currentPage()]]
        );
    }
}