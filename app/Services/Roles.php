<?php
namespace App\Services;

use Illuminate\Database\Eloquent\Collection;

/**
 * Created by PhpStorm.
 * User: server
 * Date: 09/07/17
 * Time: 01:29 م
 */
class Roles
{
    /**
     * @var Collection
     */
    private $roles;

    public function __construct(Collection $roles)
    {
        $this->roles = $roles;

    }

    public function formatToPermissionsArray()
    {
        $list=[];
        foreach ($this->roles as $adminRole) {
//            dd($adminRole->permission);
            $list[$adminRole->permission->module][] = $adminRole->permission->action;
        }

      return new Collection($list);
    }
}