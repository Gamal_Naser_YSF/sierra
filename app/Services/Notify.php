<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 22/03/18
 * Time: 08:17 ص
 */

namespace App\Services;

use App\Models\Booking;
use App\Models\CustomPackage;
use App\Models\Event;
use App\Models\Notification;
use App\Models\Reservation;
use App\Models\TravelPackage;
use App\Models\TravelPackagesBooking;
use App\Models\Trip;
use App\Models\User;

class Notify
{
    public static function notifiyErrorToAdmin($title, $text)
    {
        Notification::createNewAdminNotification($title, $text, Notification::error);
    }

    public static function notifyAdminAboutNewEventRegistration(Booking $bookingModel)
    {

        /** @var Event $event */
        $event = $bookingModel->event;
        $eventName = "<a href='" . route("eventsIndex", ["i" => $event->id]) . "'>{$event->getName()}</a>";
//        ------------------------------

        /** @var User $user */
        $user = $bookingModel->user;
        $userName = "<a href='" . route("usersIndex", ["i" => $user->id]) . "'>{$user->getName()}</a>";

        $detailsLink = "<a href='" . route("bookingsIndex", ["i" => $bookingModel->id]) . "'>here</a>";

        $text = "<p>event [$eventName] had booked for user [$userName], you can see full details $detailsLink </p>";

        Notification::createNewAdminNotification("New Event had Booked", $text, Notification::event);
    }

    public static function notifyAdminAboutNewCuastomPackageCreated(CustomPackage $row)
    {
        /** @var User $user */
        $user = $row->user;
        $userName = "<a href='" . route("usersIndex", ["i" => $user->id]) . "'>{$user->getName()}</a>";

        $here = "<a href='" . route("customPackages", ["i" => $row->id]) . "'>here</a>";
        $text = "<p>New custom package required for user [$userName], you can see mor details $here </p>";
        Notification::createNewAdminNotification("New custom package required", $text, Notification::travel);
    }

    public static function notifyAdminAboutNewTravelBookend(TravelPackagesBooking $row)
    {
        /** @var User $user */
        $user = $row->user;
        $userName = "<a href='" . route("usersIndex", ["i" => $user->id]) . "'>{$user->getName()}</a>";

        /** @var TravelPackage $package */
        $package = $row->package;
        $packageNaame = "<a href='" . route("packagesIndex", ["i" => $package->id]) . "'>{$package->getName()}</a>";

        $detailsLink = "<a href='" . route("packagesBookings", ["i" => $row->id]) . "'>here</a>";

        $text = "<p>Travel package [$packageNaame] had booked for user [$userName], you can see full details $detailsLink </p>";
        Notification::createNewAdminNotification("New travel package booked", $text, Notification::travel);
    }

    public static function notifyAdminAboutSuccessdPayment($token, $res)
    {
        Notification::createNewAdminNotification("New payment done", "token $token , data : " . json_encode($res), Notification::payment);
    }

    public static function notifyAdminAboutFaildPayment($token)
    {
//        $text = "";
//        Notification::createNewAdminNotification("Failed Payment operation", $text, Notification::payment);
//        dd($token,$res);
    }

    public static function notifiAdminAboutNewTransaction($row)
    {
    }

    public static function notifayAdminAboutCreateTrip(Trip $getTrip)
    {
        /** @var User $user */
        $user = $getTrip->user;
        $userName = "<a href='" . route("usersIndex", ["i" => $user->id]) . "'>{$user->getName()}</a>";

        $here = "<a href='" . route("tripDetails", ["id" => $getTrip->encyrptId()]) . "'>here</a>";
        $text = "<p>User [$userName] asked for new trip, you can see mor details $here </p>";
        Notification::createNewAdminNotification("User Request for new trip", $text, Notification::trip);
    }

    public static function notifyUserAboutNewReservation(Reservation $reservation)
    {
        /** @var User $user */
        $user = $reservation->user;
        $text = "<p>Hi [" . $user->getName() . "] there is new reservation required please go to reservations section for more information</p>";

        self::sendToUser($user, "New Reservation Required", $text, Notification::reservation);
    }

    private static function sendToUser(User $user, $title, $text, $type)
    {
        Notification::createNewUserNotification($title, $text, $type, $user);
        Email::sendWithLog("emails.genral", $title, ["text" => $text], $user->email, $user->getName());
    }
}