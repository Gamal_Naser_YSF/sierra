<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/12/17
 * Time: 07:13 م
 */

namespace App\Services\Trips;


interface TripDetails
{
    public function getTripId();

    public function getTrip();

    /**
     * @return RowInfoIterface[]
     */
    public function getRows();
}