<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/12/17
 * Time: 07:51 م
 */

namespace App\Services\Trips;


class TripRow implements RowInfoIterface
{
    private $row;

    public function __construct($row)
    {
        $this->row = $row;
    }

    public function getLocationFrom()
    {
        return $this->row["from"];
    }

    public function getLocationTo()
    {
        return $this->row["to"];
    }

    public function getCheckIn()
    {
        return $this->row["check_in"];
    }

    public function getCheckOut()
    {
        return $this->row["check_out"];
    }

    public function getGuestNumber()
    {
        return $this->row["guest_number"];
    }

    public function getHotelRate()
    {
        return $this->row["hotel_rate"];
    }

    public function getDepartureDate()
    {
        return $this->row["departure"];
    }

    public function getArrivalDate()
    {
        return $this->row["arrival"];
    }

    public function getAirLines()
    {
        return $this->row["airlines"];
    }

    public function __toString()
    {
        return implode(",", $this->row);
    }
}