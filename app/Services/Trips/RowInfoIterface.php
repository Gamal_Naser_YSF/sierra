<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/12/17
 * Time: 07:15 م
 */

namespace App\Services\Trips;


interface RowInfoIterface
{
    public function getLocationFrom();

    public function getLocationTo();

    public function getCheckIn();

    public function getCheckOut();

    public function getGuestNumber();

    public function getHotelRate();

    public function getDepartureDate();

    public function getArrivalDate();

    public function getAirLines();
}