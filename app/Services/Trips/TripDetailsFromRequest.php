<?php

namespace App\Services\Trips;

use App\Models\Trip;
use App\Models\User;
use App\Models\UsersTripInfo;
use App\Services\Notify;
use Carbon\Carbon;
use Illuminate\Http\Request;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 04/12/17
 * Time: 07:12 م
 */
class TripDetailsFromRequest implements TripDetails
{

    private $rows;
    private $trip;
    /**
     * @var User
     */
    private $user;

    /**
     * @param $rows
     * @return RowInfoIterface[]
     */
    private function makeRows($rows)
    {
        $b = [];
        foreach ($rows as $row) {
            $b[] = new TripRow($row);
        }
        return $b;
    }

    public function __construct($rows, User $user)
    {
        $this->rows = $rows;
        $this->user = $user;
    }

    public function getTripId()
    {
        return $this->getTrip()->id;
    }

    public function getTrip()
    {
        if ($this->trip) return $this->trip;

        return $this->trip = Trip::createForUser($this->user);

    }

    /**
     * @return RowInfoIterface[]
     */
    public function getRows()
    {
        return $this->rows;
    }

    public function insertRows()
    {

        $arr = [];
        $now = Carbon::now();
        foreach ($this->rows as $row) {
            $arr[] = [
                "location_from" => $row["from"],
                "location_to" => $row["to"],
                "check_in" => $row["check_in"],
                "check_out" => $row["check_out"],
                "guest_number" => $row["guest_number"],
                "hotel_rate" => $row["hotel_rate"],
                "departure" => $row["departure"],
                "arrival" => $row["arrival"],
                "airlines" => $row["airlines"],
                "created_at" => $now,
                "updated_at" => $now,
                "trip_id" => $this->getTripId()
            ];
        }
        UsersTripInfo::insert($arr);
        Notify::notifayAdminAboutCreateTrip($this->getTrip());
    }
}