<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 31/03/18
 * Time: 03:04 م
 */

namespace App\Services\Transactions;


class CalculateFees
{
    public static function calculate($subTotal, $discount = 0, $VAT = 0)
    {

        $total = ($subTotal - ($subTotal * $discount / 100)) + (($subTotal * $VAT / 100));
        return (object)compact("VAT", "subTotal", "discount", "total");
    }
}