<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 31/03/18
 * Time: 02:16 م
 */

namespace App\Services\Transactions;


use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;

class TransactionTransformer
{


    private $transaction;

    private static function makeDetails($referenceCode)
    {
        $data = [];
        $items = explode("|", $referenceCode);
        foreach ($items as $item) {
            list($k, $v) = explode(":", trim($item));
            $data[$k] = $v;
        }
        return (object)$data;
    }

    private static function makeFees($total)
    {
        return CalculateFees::calculate(json_decode($total)->USD,12.5);

    }

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return ($this->total);
    }

    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
        $this->fees = self::makeFees($transaction->total);
        $this->paymentMethod = $transaction->paymentMethod;
        $this->time = Carbon::parse($transaction->timestamp);
//        $this->errorCode = $transaction->errorCode;
        $this->status = ($transaction->isSuccessful === "true") ? true : false;
        $this->requestId = $transaction->requestId;
        $this->referenceCode = ($transaction->referenceCode);
        $this->details = self::makeDetails($transaction->referenceCode);

        $this->contact = (object)[
            "firstName" => $transaction->billingFirstName,
            "lastName" => $transaction->billingLastName,
            "email" => $transaction->contactEmail,
            "phone" => $transaction->contactPhone,
            "address" => (object)[
                "address" => $transaction->billingAddress,
                "city" => $transaction->billingCity,
                "countryCode" => $transaction->billingCountryCode,
                "state" => $transaction->billingState,
                "zip" => $transaction->billingZip,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        if (!$this->user)
            $this->user = User::find($this->transaction->user_id);
        return $this->user;
    }
}