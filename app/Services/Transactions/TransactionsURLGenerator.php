<?php

namespace App\Services\Transactions;

use App\Models\InterFaces\Payable;
use App\Models\Plan;
use App\Models\Reservation;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 31/03/18
 * Time: 10:39 ص
 */
class TransactionsURLGenerator
{
    private $price;
    private $message;


    public static function forModel(Payable $model)
    {
        return new  static($model->getPrice(), self::makeReservationMessage($model->getModuleName(), $model->getModuleId(), $model->getModuleInformation()));
    }

    private static function makeReservationMessage($type, $id, array $items)
    {
        $msg = "module:$type | id:$id";

        foreach ($items as $k => $item)
            $msg .= " | " . "$k:" . $item;

        return $msg;
    }


//    -----------------------------------------------

    private function __construct($price, $message)
    {
        $this->price = $price;
        $this->message = $message;
    }


    public function url()
    {
        return route("pages.payment", ["price" => $this->price, "message" => $this->message]);
    }
}