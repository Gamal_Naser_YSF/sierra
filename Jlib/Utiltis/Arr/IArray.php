<?php

namespace Jlib\Utilitis\Arr;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of IArray
 *
 * @author jooAziz
 */
interface IArray {

    public function all();

    public function first();

    public function push($item);

    public function add($key, $val);

    public function remove($key);
}
