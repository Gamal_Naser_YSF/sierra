<?php

namespace Jlib\Utilitis\Arr;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Arr
 *
 * @author jooAziz
 */
class Arr implements IArray {

    private $arr = [];

    /**
     * 
     * @param array $givenArr
     */
    public function __construct(array $givenArr = []) {
        $this->arr = $givenArr;
    }

    /**
     * 
     * @param type $key
     * @param type $val
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public function add($key, $val) {
        $this->arr[$key] = $val;
        return $this;
    }

    public function remove($key) {
        unset($this->arr[$key]);
        return $this;
    }

    /**
     * 
     * @return array
     */
    public function all() {

        return $this->arr;
    }

    public function first() {
        return (is_array($this->arr) && (count($this->arr) >= 1)) ? $this->arr[0] : null;
    }

    public function push($item) {
        $this->arr[] = $item;
    }
    public function get($name){
        return @$this->arr[$name];
    }
//put your code here
}
