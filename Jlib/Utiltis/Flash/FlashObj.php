<?php

namespace Jlib\Utilitis\Flash;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 24/10/17
 * Time: 10:05 م
 */

interface FlashObj
{
    public function getMessage();

    public function getStatus();

    public function getRow();
}