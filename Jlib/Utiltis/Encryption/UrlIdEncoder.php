<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Utilitis\Encryption;

/**
 * Description of UrlIdEncoder
 *
 * @author server
 */
class UrlIdEncoder
{

    private $id;
    private $UrlId;

    public function __construct($id = null)
    {

//        if (!$data)
//            abort(404, "Missing data in URLIdEncoder");
        $this->id = $id;
        $this->UrlId = request()->segment(4);
    }

    public function __toString()
    {
        return $this->encode();
    }

    public function encode()
    {
        return base64_encode(json_encode(["id" => $this->id]));
    }

    public function decode()
    {
        return json_decode(base64_decode($this->UrlId));
    }

    public function id()
    {
        return $this->decode()->id;
    }

    /**
     * @param $id
     * @return static
     */
    public static function sDecode($id)
    {
        $self = new static;
        $self->UrlId = $id;
        return $self;
    }
}
