<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Utilitis\Encryption;

/**
 * Description of JwtEncription
 *
 * @author server
 */
use Illuminate\Database\Eloquent\Model;

class JwtEncription {

    public static function fromStr($string) {
        return self::encriptor($string);
    }

    public static function fromElquentObj(Model $elquentObj) {
        return self::encriptor($elquentObj->toJson());
    }

    public static function fromArray(array $array) {
        return self::encriptor(json_encode($array));
    }

    private static function encriptor($string) {
        return base64_encode($string);
    }

}
