<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Utilitis\ImageResizer;

/**
 * Description of SizeOptions
 *
 * @author jooAziz
 */
class SizeOptions {

    private static $typeCrop = 'crop';
    private static $typeResize = 'resize';
    private $type;
    private $quality;
    private $width;
    private $height;

    /**
     * 
     * @param type $width
     * @param type $height
     * @param type $quality
     */
    public function __construct($width, $height = null, $quality = 90) {
        $this->quality = $quality;
        $this->type = self::$typeResize;
        $this->width = $width;
        $this->height = (is_null($height)) ? $width : $height;
    }

    /**
     * 
     * @return \Jlib\Utilitis\ImageResizer\SizeOptions
     */
    public function setTypeToCrop() {
        $this->type = self::$typeCrop;
        return $this;
    }

    /**
     * 
     * @return \Jlib\Utilitis\ImageResizer\SizeOptions
     */
    public function setTypeToResize() {
        $this->type = self::$typeResize;
        return $this;
    }

    /**
     * 
     * @param int $quality
     * @return \Jlib\Utilitis\ImageResizer\SizeOptions
     */
    public function setQuality($quality) {
        $this->quality = $quality;
        return $this;
    }

    /**
     * 
     * @param int $width
     * @return \Jlib\Utilitis\ImageResizer\SizeOptions
     */
    public function setWidth($width) {
        $this->width = $width;
        return $this;
    }

    /**
     * 
     * @param int $height
     * @return \Jlib\Utilitis\ImageResizer\SizeOptions
     */
    public function setHeight($height) {
        $this->height = $height;
        return $this;
    }

    /**
     * 
     * @return int
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * 
     * @return int
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * 
     * @return int
     */
    public function getQulity() {
        return $this->quality;
    }

    /**
     * 
     * @return string
     */
    public function getType() {
        return $this->type;
    }

}
