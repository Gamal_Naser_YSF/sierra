<?php

namespace Jlib\Utilitis\ImageResizer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Resize
 *
 * @author jooAziz
 */
class ResizeBulider {

    private $sizesList;

    /**
     * 
     * @param \Jlib\Utilitis\ImageResizer\SizeOptions $sizeClass
     * @return \Jlib\Utilitis\ImageResizer\ResizeBulider
     */
    public function addSize(SizeOptions $sizeClass) {
        $this->sizesList[] = $sizeClass;
        return $this;
    }

    /**
     * 
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public function bilud() {
        return new \Jlib\Utilitis\Arr\Arr($this->sizesList);
    }

}
