<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Utilitis\ImageResizer;

/**
 * Description of Resizer
 *
 * @author jooAziz
 */
class Resizer {

    private static $uploadPath = 'uploads/';

    private function __construct() {
        
    }

    public static function fromResizeBulider(ResizeBulider $bulider, \Jlib\Utilitis\Arr\Arr$filesName) {
        $v = $bulider->bilud();
        foreach ($bulider->bilud()->all() as $option) {
            self::resizeOneByOne($option, $filesName->all());
        }
    }

    private static function resizeOneByOne(SizeOptions $option, $filesName) {
        $fullDirName = self::$uploadPath . $option->getWidth() . 'x' . $option->getHeight().'/';
        (!\File::exists($fullDirName)) ? mkdir($fullDirName) : null;
        foreach ($filesName as $file) {
            $oldImageFullPath = self::$uploadPath . $file;
            $image = new \Eventviva\ImageResize($oldImageFullPath);
            $image->quality_jpg = $option->getQulity();
            $image->{$option->getType()}($option->getWidth(), $option->getHeight());
//            dd($fullDirName);
            $image->save($fullDirName . $file);
        }
    }

}
