<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Utilitis\ImageResizer;

/**
 * Description of ResizeFacad
 *
 * @author jooAziz
 */
use Jlib\Utilitis\ImageResizer\ResizeBulider;
use Jlib\Utilitis\ImageResizer\SizeOptions;

class ResizeBuldirFacad {

    /**
     * 
     * @param type $size
     * @return ResizeBulider
     */
    public static function fromArray($size) {
        $bulider = new ResizeBulider();

        if (is_array($size) && !empty($size)) {
            foreach ($size as $oneSize) {
                $sizeOprionsAttr = explode(',', $oneSize);
                $sizeOptionClass = new SizeOptions($sizeOprionsAttr[1]); //width
                $sizeOptionClass->setHeight($sizeOprionsAttr[2]); //height
                ($sizeOprionsAttr[0] == 'crop') ? $sizeOptionClass->setTypeToCrop() : $sizeOptionClass->setTypeToResize(); //type
                $bulider->addSize($sizeOptionClass);
            }
        }
        return $bulider;
    }

}
