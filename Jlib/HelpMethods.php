<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 23/10/17
 * Time: 10:01 م
 */


if (!function_exists("_t")) {
    function _t($key, $replace = [], $locale = null)
    {
        return @explode(".", $key)[1];
        return app('translator')->getFromJson($key, $replace, $locale);
    }
}

if (!function_exists("_con")) {

    function _con($k)
    {
        return \App\Models\Config::get($k);
    }

}
if (!function_exists("_enc")) {

    function _enc($val, $endfix = "")
    {
        return ($val * (19876)) . (($endfix) ? "-$endfix" : "");
        return \Crypt::encrypt($val);
    }

}
if (!function_exists("_dec")) {

    function _dec($val)
    {

        list($id) = explode("-",$val);

        return $id / (19876);
        return \Crypt::decrypt($val);
    }
}
if (!function_exists("_ren")) {

    function _ren($name, $path = 'upload/', $options = ['class' => 'cropped_preview img-responsive img-rounded'])
    {
        return \Jlib\Render\Render::image($name, $path, $options);
    }
}
if (!function_exists("_str")) {

    function _strToWords($val, $count = 100)
    {
        return \Illuminate\Support\Str::words($val, $count);
    }
}

if (!function_exists("auther")) {
    function auther()
    {
        return \Jlib\Auth\Auther::class;
    }
}

if (!function_exists("getDays")) {
    function getDays($str)
    {
        return \App\Models\TravelPackage::getDays($str);
    }
}