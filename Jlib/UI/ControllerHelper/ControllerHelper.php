<?php

namespace Jlib\UI\ControllerHelper;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 08/10/17
 * Time: 09:26 م
 */
use Illuminate\Routing\Route;

trait ControllerHelper
{
    public function view(array $data = [])
    {
        $vars = self::getCommanVars();

        $data["scope"] = $vars->scope;
        $data["module"] = $vars->module;

        return view($vars->view, $data);
    }

    private static $route;

    private static function getRoute()
    {
        if (self::$route)
            return self::$route;

        return self::$route = request()->route();
    }

    private static function getCommanVars()
    {
        return (object)[
            "scope" => self::getScope(),
            "module" => self::getModule(),
            "view" => strtolower(self::getScope() . "." . self::getModule() . "." . self::getRoute()->getActionMethod())
        ];
    }

    private static $_module;

    public static function getModule()
    {
        if (!self::$_module) {

            $uriParts = explode("@", self::getRoute()->getAction()["controller"]);
            $ctrParts = explode("\\", $uriParts[0]);
            self::$_module = strtolower(end($ctrParts));
        }
        return self::$_module;
    }

    private static $_scope;

    public static function getScope(Route $prefix = null)
    {
        if (!self::$_scope) {
            $prefix = self::getRoute()->getPrefix();
            self::$_scope = ($prefix == "/") ? "front" : (function () use ($prefix) {
                $prts = explode("/", $prefix);
                return ($prts[0]) ? $prts[0] : $prts[1];
            })();

        }
        return self::$_scope;
    }
}