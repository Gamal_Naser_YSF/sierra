<?php

namespace Jlib\Auth;

use App\Models\InterFaces\UserModel;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 21/10/17
 * Time: 01:48 ص
 */
class Auther
{
    private $userKey;
    private static $instance;


    public static function setUser(UserModel $userModel)
    {
        return self::getInstance()->_setUser($userModel);
    }

    public static function isLogin()
    {
        return self::getInstance()->_isLogin();
    }

    public static function logout()
    {
        return self::getInstance()->_logout();
    }

    public static function getUser()
    {
        return self::getInstance()->_getUser();
    }

    private static $endFix;

    public static function setKey($key)
    {
        self::$endFix = $key;
    }

    /**
     * @return static
     */
    public static function getInstance()
    {
        if (self::$instance)
            return self::$instance;

        return self::$instance = new static;
    }

    private function __construct()
    {
        $this->userKey = env("APP_KEY") . self::$endFix;
    }

    private function _setUser(UserModel $userModel)
    {
        \Session::put($this->userKey, $userModel);
    }


    private function _isLogin()
    {
        return (\Session::has($this->userKey) && \Session::get($this->userKey)) ? true : false;
    }

    private function _logout()
    {
        \Session::forget($this->userKey);
    }

    public function _getUser()
    {

        return \Session::get($this->userKey);
    }
}
