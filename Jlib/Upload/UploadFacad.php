<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Upload;

/**
 * Description of UploadFacad
 *
 * @author jooAziz
 */
use Jlib\Utilitis\ImageResizer\ResizeBuldirFacad;

class UploadFacad {

    /**
     *
     * @param type $files
     * @param type $size
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public static function uploadWithResize($files, $size = []) {

        $uploadHandler = UploadAndResize::uploadRequestFiled($files);

        if (is_array($size) && !empty($size)) {
            $uploadHandler->resize(ResizeBuldirFacad::fromArray($size));
        }
        return $uploadHandler->getNames();
    }

    /**
     *
     * @param type $files
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public static function justUpload($files) {
        $uploadHandler = UploadAndResize::uploadRequestFiled($files);
        return $uploadHandler->getNames();
    }

}
