<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Upload;

/**
 * Description of UploadFacad
 *
 * @author jooAziz
 */
use Jlib\Upload\UploadedFilesColllection as UFC;
use Jlib\Utilitis\Arr\Arr;
use Jlib\Utilitis\ImageResizer\Resizer;
use Jlib\Utilitis\ImageResizer\ResizeBulider;

class UploadAndResize
{

    private $names;

    private function __construct($names)
    {
        $this->names = $names;
    }

    /**
     *
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public function getNames()
    {
        return $this->names;
    }

    public function resize(ResizeBulider $bulider)
    {
        Resizer::fromResizeBulider($bulider, $this->names);
        return $this;
    }

    /**
     *
     * @param type $files
     * @return \Jlib\Upload\UploadFacad
     */
    public static function uploadRequestFiled($files)
    {
        return new static((new Uploader(new UFC($files)))->get());
    }

    /**
     *
     * @param type $fileds
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public static function uploadMultiRequestFields($fileds)
    {
        $res = new Arr();
        foreach ($fileds as $fieldName => $field) {
            $res->add($fieldName, self::uploadRequestFiled($field));
        }

        return $res;
    }

}
