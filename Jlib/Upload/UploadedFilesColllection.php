<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Jlib\Upload;

/**
 * Description of UplodedFilesColllection
 *
 * @author jooAziz
 */
class UploadedFilesColllection {

    private $files;

    public function __construct($files) {

        $filesArray = [];

        if ($files instanceof \Illuminate\Http\UploadedFile) {
            $filesArray = [$files];
        } elseif (is_array($files) && isset($files[0]) && ($files[0] instanceof \Illuminate\Http\UploadedFile )) {
            $filesArray = $files;
        }

        $this->files = new \Jlib\Utilitis\Arr\Arr($filesArray);
    }

    /**
     * 
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public function get() {
        return $this->files;
    }

}
