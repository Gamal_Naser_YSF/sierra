<?php

namespace Jlib\Upload;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Jlib\Configs\Vars;
use Jlib\Utilitis\Encryption\UUID;

/**
 * Description of Uploader
 *
 * @author jooAziz
 */
class Uploader {

    private $uploadesFilesList = [];
    private $uploadList;

    public function __construct(UploadedFilesColllection $files) {
        $this->uploadesFilesList = new \Jlib\Utilitis\Arr\Arr();
        $this->uploadList = $files->get();
    }

    /**
     * 
     * @return \Jlib\Utilitis\Arr\Arr
     */
    public function get() {
        foreach ($this->uploadList->all() as $oneFile) {
            $this->uploadesFilesList->push(self::runUploda($oneFile));
        }
        return $this->uploadesFilesList;
    }

    /**
     * 
     * @param \Jlib\Utilitis\Arr\Arr $files
     * @return \Jlib\Utilitis\Arr\Arr
     */
    private static function runUploda(\Illuminate\Http\UploadedFile $file) {
        $filename = UUID::uniqeFileName() . '.' . strtolower($file->getClientOriginalExtension());
        $file->move(Vars::getUploadPath(), $filename);
//        dd($filename);
        return $filename;
    }

}
