<?php
/**
 * Created by PhpStorm.
 * User: Mahmoud Ali
 * Date: 3/28/2017
 * Time: 4:09 PM
 */

namespace Jlib\Configs;


class Vars
{
    public static function getUploadPath()
    {
        return env('UPLOAD_BASE', 'upload');
    }
}