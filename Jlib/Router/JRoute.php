<?php

namespace Jlib\Router;


/**
 * Created by PhpStorm.
 * User: joe
 * Date: 08/10/17
 * Time: 09:32 م
 */

use ReflectionClass;
use ReflectionMethod;
use Illuminate\Support\Str;
use Route;

class JRoute
{
    public static function controller($prefix, $namespace, $controller)
    {
        $fullNameSpace = $namespace . "\\" . $controller;

        $class = new ReflectionClass($namespace . "\\" . $controller);
        $AllMethods = $class->getMethods(ReflectionMethod::IS_PUBLIC);

        foreach ($AllMethods as $method) {
            if ($method->class == $fullNameSpace) {
                $methodObj = self::analysesName($method->name);
                if ($methodObj["name"] == "index" || $methodObj["name"] == "getIndex")
                    Route::{$methodObj["action"]}($prefix, $controller . "@" . $methodObj["name"]);

                Route::{$methodObj["action"]}($prefix . "\\" . $methodObj["slug"], $controller . "@" . $methodObj["name"]);
            }
        }

    }


    private static function analysesName($methodName)
    {
        $actions = ["get", "post", "put", "delete"];
        foreach ($actions as $k) {
            if (Str::startsWith($methodName, $k)) {
                $pureMethodName = Str::replaceFirst($k, "", $methodName);
                return [
                    "action" => $k,
                    "slug" => Str::snake($pureMethodName, "-"),
                    "name" => $methodName
                ];
            }
        }


    }
}