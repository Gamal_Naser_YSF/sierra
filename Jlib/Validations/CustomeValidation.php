<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 23/10/17
 * Time: 10:49 م
 */

namespace Jlib\Validation;

use Illuminate\Support\Facades\Validator;

//use Illuminate\Validation\Validator;


class CustomeValidation
{
    public static function init()
    {
        self::isPhone();
    }

    private static function isPhone()
    {

        Validator::extend('isPhone', function ($attribute, $value, $parameters, $validator) {
            return is_numeric($value);
        });

        Validator::extend('allIsImages', function ($attribute, $value, $parameters, $validation) {

            if (!is_array($value)) return false;
            foreach ($value as $img)
                if (!$validation->validateImage($attribute, $img)) return false;
            return true;
        });
    }
}