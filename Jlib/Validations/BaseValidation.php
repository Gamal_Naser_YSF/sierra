<?php

namespace Jlib\Validation;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Illuminate\Foundation\Http\FormRequest;

/**
 * Description of BaseValidation
 *
 * @author jooaziz
 */
Abstract class BaseValidation extends FormRequest
{
    public function __construct(array $query = array(), array $request = array(), array $attributes = array(), array $cookies = array(), array $files = array(), array $server = array(), $content = null)
    {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
    }

    protected $myUrl;

    public function authorize()
    {
        return true;
    }

    abstract protected function validatCreate();

    abstract protected function currentRoute();

    abstract protected function validatEdit();

    public function rules()
    {

        if ($this->is($this->currentRoute() . '/create')) {
            return $this->validatCreate();

        } else if ($this->is($this->currentRoute() . '/edit')) {
            return $this->validatEdit();
        }
        return [];
    }

}
