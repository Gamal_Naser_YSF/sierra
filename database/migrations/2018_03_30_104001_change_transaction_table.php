<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('transactions');
        Schema::create("transactions", function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer("user_id")->index();

            $table->string("errorCode")->nullable();
            $table->string("message")->nullable();
            $table->string("errorField")->nullable();
            $table->string("token")->nullable();
            $table->string("isSuccessful")->nullable();
            $table->string("timestamp")->nullable();
            $table->string("requestId")->nullable();
            $table->string("accountSuffix")->nullable();
            $table->string("billingAddress")->nullable();
            $table->string("billingCity")->nullable();
            $table->string("billingCountryCode")->nullable();
            $table->string("billingFirstName")->nullable();
            $table->string("billingLastName")->nullable();

            $table->string("billingState")->nullable();
            $table->string("billingZip")->nullable();
            $table->string("bincode")->nullable();

            $table->string("contactEmail")->nullable();
            $table->string("contactPhone")->nullable();
            $table->string("isTheCardEnrolled")->nullable();
            $table->string("paymentMethod")->nullable();
            $table->string("referenceCode")->nullable();
            $table->string("transactionId")->nullable();
            $table->text("total")->nullable();// objec
            $table->text("exchangeRates")->nullable(); // objec
            $table->text("totalPurchases")->nullable();// objec
            $table->text("errorMessage")->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
