<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ConfigsSiteInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ["name" => "main_phone", "type" => "text"],
            ["name" => "phones", "type" => "textarea"],
            ["name" => "addresses", "type" => "textarea"],
            ["name" => "emails", "type" => "textarea"],
            ["name" => "vision", "type" => "textarea"],
            ["name" => "mission", "type" => "textarea"],
            ["name" => "profile", "type" => "textarea"],
        ];


        \App\Models\Config::setNewRecord($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
