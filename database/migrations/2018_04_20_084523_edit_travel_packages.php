<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTravelPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('travel_packages', function (Blueprint $table) {
            $table->string('destination')->nullable()->change();
            $table->integer('rate')->default(0)->change();
            $table->integer('duration_in_days')->nullable()->change();
            $table->string('hotel')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->string('tour_type')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}