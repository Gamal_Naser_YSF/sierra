<?php

// create_social_facebook_accounts.php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string("accountSuffix");
            $table->string("billingAddress");
            $table->string("billingCity");
            $table->string("billingCountryCode");
            $table->string("billingFirstName");
            $table->string("billingLastName");
            $table->string("billingState");
            $table->string("billingZip");
            $table->string("bincode");
            $table->string("contactEmail");
            $table->string("contactPhone");
            $table->string("isTheCardEnrolled");
            $table->string("paymentMethod");
            $table->string("referenceCode");
            $table->string("requestId");
            $table->string("timestamp");
            $table->string("token");
            $table->string("total");
            $table->string("transactionId");
            $table->string("user_id");


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}