<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TravelPackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_packages', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('views')->default(0);
            $table->string('name');
            $table->string('destination');
            $table->integer('rate');
            $table->integer('duration_in_days');
            $table->string('hotel');
            $table->string('airlines')->nullable();
            $table->string('description');
            $table->string('image')->default("");
            $table->string('video')->nullable();
            $table->string('tour_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
