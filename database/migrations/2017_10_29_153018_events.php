<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Events extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('event_name');
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('country');
            $table->string('city');
            $table->string('address');
            $table->string('tags');
            $table->longText('details');

//            $table->integer('category_id')->unsigned();
//            $table->string('profile_picture');
            $table->integer('overall_rating');
            $table->integer('status_id')->unsigned();
            $table->string('event_coordinator');
            $table->string('event_serial');
            $table->string('proposal_end_date')->nullable();
            $table->string('organized_by');
            $table->string('event_website');
            $table->string('event_location_lat');
            $table->string('event_location_long');
            $table->text('event_highlightes')->nullable();
            $table->integer('views')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('events');
    }
}