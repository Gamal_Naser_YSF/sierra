<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdminsPermissions extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        \Schema::create('admins_Permissions', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('admin_id');
            $table->integer('permission_id');
            $table->string('scope');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        \Schema::drop('admins_Permissions');
    }

}
