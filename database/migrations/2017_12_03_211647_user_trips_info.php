<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserTripsInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_trips_info', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('trip_id')->index();
            $table->string('location_from');
            $table->string('location_to');
            $table->string('check_in');
            $table->string('check_out');
            $table->integer('guest_number');
            $table->integer('hotel_rate');
            $table->string('departure');
            $table->string('arrival');
            $table->string('airlines');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
