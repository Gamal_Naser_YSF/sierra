<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:32 م
 */


use Illuminate\Database\Seeder;

class StatusSeeeder extends Seeder
{
    public function run()
    {
        $st = ["active", "deactivate", "canceled", "approved", "pending"];
        foreach ($st as $s) {
            $data[] = ["name" => $s];
        }
        \App\Models\Status::truncate();
        \App\Models\Status::insert($data);
    }
}