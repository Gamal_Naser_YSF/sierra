<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/12/17
 * Time: 09:56 م
 */
class UsersTripInfoSeeders extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 50;
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'trip_id' => mt_rand(1, 30),
                'location_from' => $faker->country,
                'location_to' => $faker->country,
                'check_out' => $faker->date("Y-m-d"),
                'check_in' => $faker->date("Y-m-d"),
                'guest_number' => mt_rand(1, 20),
                'hotel_rate' => mt_rand(1, 5),
                'departure' => $faker->date("Y-m-d"),
                'arrival' => $faker->date("Y-m-d"),
                'airlines' => $faker->name,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Models\UsersTripInfo::truncate();
        \App\Models\UsersTripInfo::insert($data);
    }
}
