<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();
        $now = Carbon::now();
        Admin::create(
            [
                "full_name" => "super admin",
                "email" => "admin@admin.admin",
                "password" => \Hash::make("123456789"),
                "phone" => "012012",
                "token" => "",
                "token_expire" => "",
                "is_super_admin" => 1,
                "active" => 1,
                "avatar" => "",
                "created_at" => $now,
                "updated_at" => $now,
            ]
        );
        $faker = \Faker\Factory::create();
        $data = [];
        $count = 3;
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                "full_name" => $faker->userName,
                "email" => $faker->email,
                "password" => \Hash::make($faker->password()),
                "phone" => $faker->phoneNumber,
                "token" => $faker->sha256,
                "token_expire" => $faker->date(),
                "is_super_admin" => $faker->randomElement([0, 1]),
                "active" => $faker->randomElement([0, 1]),
                "avatar" => "",
                "created_at" => $faker->date("Y-m-d H:i:s"),
                "updated_at" => $faker->date("Y-m-d H:i:s"),
            ];
        }

        Admin::insert($data);
    }

}

