<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:32 م
 */


use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 300;

        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'file_name' => mt_rand(1, 31) . ".jpg",
                'model' => "events",
                'model_id' => mt_rand(1, 150),
            ];
        }
        \App\Models\Media::truncate();
        \App\Models\Media::insert($data);
    }
}