<?php

use Illuminate\Database\Seeder;

class CategoryTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $faker = \Faker\Factory::create();

        $data = [[
            "name" => "Bussiness",
            "created_at" => $faker->date("Y-m-d H:i:s"),
            "updated_at" => $faker->date("Y-m-d H:i:s"),
        ], [
            "name" => "Traveller",
            "created_at" => $faker->date("Y-m-d H:i:s"),
            "updated_at" => $faker->date("Y-m-d H:i:s"),
        ]];


        \App\Models\CategoryTypes::truncate();
        \App\Models\CategoryTypes::insert($data);
    }

}

