<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/12/17
 * Time: 09:56 م
 */
class UsersTripsSeeders extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 50;
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'created_by' => mt_rand(1, 30),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Models\Trip::truncate();
        \App\Models\Trip::insert($data);
    }
}