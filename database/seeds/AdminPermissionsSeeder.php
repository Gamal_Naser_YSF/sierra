<?php

use App\Models\AdminsRoles;
use Illuminate\Database\Seeder;

class AdminPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AdminsRoles::truncate();
        $faker = \Faker\Factory::create();
        $adminIds = \App\Models\Admin::pluck("id")->toArray();
        $count = 50;
        $data = [];
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                "admin_id" => $faker->randomElement($adminIds),
                "scope" => $faker->randomElement(["admin", "account"]),
                "permission_id" => $faker->randomElement([1, 2]),
                "created_at" => $now,
                "updated_at" => $now,
            ];
        }

        AdminsRoles::insert($data);
    }
}
