<?php

use Illuminate\Database\Seeder;

class DevSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()

    {
        $this->call(AdminSeeder::class);
        $this->call(StatusSeeeder::class);
        $this->call(CategoryTypesSeeder::class);
        $this->call(HotelsSeeders::class);
        $this->call(AirLinesSeeder::class);
        $this->call(CustomePackageSeeder::class);
        $this->call(CategorySeeder::class);
        $this->call(EventsSeeder::class);
        $this->call(EventCategorySeeder::class);
        $this->call(MediaSeeder::class);
        $this->call(travelPakcagesSeeder::class);
        $this->call(UsersTripsSeeders::class);
        $this->call(UsersTripInfoSeeders::class);
        $this->call(travelPakcagesSeeder::class);

    }
}
