<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 05/12/17
 * Time: 10:57 م
 */

class CustomePackageSeeder extends Seeder
{

    public function run()
    {
        $data = [];
        $count = 50;
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                "departure_date" => "1/2/2014",
                "user_id" => mt_rand(1, 30),
                "hotel_id" => mt_rand(1, 30),
                "airlines" => mt_rand(1, 30),
                "adults" => mt_rand(1, 10),
                "Children" => mt_rand(1, 3),
                "Infant" => mt_rand(1, 5),
                "created_at" => $now,
                "updated_at" => $now
            ];
        }
        \App\Models\CustomPackage::truncate();
        \App\Models\CustomPackage::insert($data);

    }
}