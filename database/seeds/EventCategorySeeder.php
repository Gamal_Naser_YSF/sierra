<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:32 م
 */


use Illuminate\Database\Seeder;

class EventCategorySeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 50;

        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'event_id' => mt_rand(1, 150),
                'category_id' => mt_rand(1, 30),
            ];
        }
        \App\Models\CategoyEvent::truncate();
        \App\Models\CategoyEvent::insert($data);
    }
}