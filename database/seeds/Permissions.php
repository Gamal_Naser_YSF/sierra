<?php
use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Permission;

class Permissions extends Seeder
{

    private static function getPermissions()
    {
        $scops = [
            "admin" => [
                "dashboard" => [
                    ["label" => "view dashboard", "action" => "index"],
                ],
                "admin" => [
                    ["label" => "list admins", "action" => "index"],
                    ["label" => "edit admins", "action" => "edit"],
                    ["label" => "delete admins", "action" => "delete"],
                    ["label" => "view admins", "action" => "view"],
                    ["label" => "create admins", "action" => "create"],
                ],
                "request" => [
                    ["label" => "list request", "action" => "index"],
                    ["label" => "edit request", "action" => "edit"],
                    ["label" => "delete request", "action" => "delete"],
                    ["label" => "view request", "action" => "view"],
                    ["label" => "create request", "action" => "create"],
                ],
            ],
            "account" => [
                "dashboard" => [
                    ["label" => "view dashboard", "action" => "index"],
                ],
                "admins" => [
                    ["label" => "list admins", "action" => "index"],
                    ["label" => "edit admins", "action" => "edit"],
                    ["label" => "delete admins", "action" => "delete"],
                    ["label" => "view admins", "action" => "view"],
                    ["label" => "create admins", "action" => "create"],
                ],
                "requests" => [
                    ["label" => "list request", "action" => "index"],
                    ["label" => "edit request", "action" => "edit"],
                    ["label" => "delete request", "action" => "delete"],
                    ["label" => "view request", "action" => "view"],
                    ["label" => "create request", "action" => "create"],
                ],
            ],
        ];

        $list = [];

        foreach ($scops as $scope => $modules)
            foreach ($modules as $module => $permissions)
                foreach ($permissions as $permission)
                    $list[] = ["scope" => $scope, "module" => $module, "label" => $permission["label"], "action" => $permission["action"]];
        return $list;
    }

    public function run()
    {
        Permission::truncate();
        Permission::insert(self::getPermissions());
    }
}

