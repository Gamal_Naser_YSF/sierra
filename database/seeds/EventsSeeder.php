<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 29/10/17
 * Time: 05:32 م
 */

use App\Models\Event;
use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 150;

        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'event_name' => $faker->name,
                'start_date' => $faker->date("Y-m-d"),
                'end_date' => $faker->date("Y-m-d"),
                'country' => $faker->country,
                'city' => $faker->city,
                'views' => $faker->randomNumber(),
                'address' => $faker->address,
                'details' => $faker->paragraph,
//                'profile_picture' => mt_rand(1, 31) . ".jpg",
                'tags' => "yya,sdf,sdfdsf,tads,fdfe,sdfdv,utyij,fder,dfg",
                'overall_rating' => mt_rand(1, 5),
                'status_id' => mt_rand(1, 5),
                'event_coordinator' => $faker->text,
                'event_serial' => $faker->text,
                'proposal_end_date' => $faker->date("Y-m-d"),
                'organized_by' => $faker->text,
                'event_website' => $faker->url,
                'event_location_lat' => $faker->latitude,
                'event_location_long' => $faker->longitude,
                'event_highlightes' => $faker->text(),
                'created_at' => $faker->date("Y-m-d H:i:s"),
                'event_highlightes' => $faker->date("Y-m-d H:i:s"),
            ];
        }
        Event::truncate();
        Event::insert($data);
    }
}