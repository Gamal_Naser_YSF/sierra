<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
\App\Models\Category::truncate();

        $faker = \Faker\Factory::create();
        $data = [];
        $count = 30;
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                "name" => $faker->userName,
                "image" => mt_rand(1, 31) . ".jpg",
                "type" => mt_rand(1, 2),
                "show_in_home_page" => mt_rand(0, 1),
                "category_id" => mt_rand(1, $count),
                "description" => $faker->text,
                "created_at" => $faker->date("Y-m-d H:i:s"),
                "updated_at" => $faker->date("Y-m-d H:i:s"),
            ];
        }

        \App\Models\Category::insert($data);
    }

}

