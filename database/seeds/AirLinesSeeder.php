<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/12/17
 * Time: 09:56 م
 */
class AirLinesSeeder extends Seeder
{

    public function run()
    {
        \App\Models\Airlines::truncate();
        $faker = Faker\Factory::create();
        $data = [];
        $count = 4;
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'name' => $faker->name,
                'image' => mt_rand(1, 30) . ".jpg",
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Models\Airlines::insert($data);
    }
}