<?php

use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 01/12/17
 * Time: 09:56 م
 */
class travelPakcagesSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker\Factory::create();
        $data = [];
        $count = 50;
        $now = \Carbon\Carbon::now();
        for ($I = 0; $I < $count; $I++) {
            $data[] = [
                'name' => $faker->name,
                'destination' => $faker->country,
                'rate' => mt_rand(1, 5),
                'duration_in_days' => mt_rand(1, 20),
                'hotel' => $faker->name,
                'airlines' => $faker->name,
                'description' => $faker->name,
                'image' => mt_rand(1, 20) . ".jpg",
                'video' => $faker->url,
                'tour_type' => mt_rand(1, 5),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }
        \App\Models\TravelPackage::truncate();
        \App\Models\TravelPackage::insert($data);
    }
}