<?php

namespace Hive\Auth\Social\Services;
use Laravel\Socialite\Facades\Socialite;

/**
 * Created by PhpStorm.
 * User: joe
 * Date: 15/12/17
 * Time: 11:32 ص
 */

class SocilaProviders
{
    /**
     * @return \Laravel\Socialite\Contracts\Provider
     */
    public static function facebook()
    {
        return Socialite::driver('facebook');
    }
}