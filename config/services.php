<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [
        'client_id' => env("SOCIAL_FACEBOOK_APP_ID"),
        'client_secret' => env("SOCIAL_FACEBOOK_APP_SECRET"),
        'redirect' => env("SOCIAL_FACEBOOK_APP_REDIRECT"),
    ]
    ,
    'google' => [
        'client_id' => env("SOCIAL_GOOGLE_APP_ID"),
        'client_secret' => env("SOCIAL_GOOGLE_APP_SECRET"),
        'redirect' => env("SOCIAL_GOOGLE_APP_REDIRECT"),
    ],
    'twitter' => [
        'client_id' => env("SOCIAL_TWITTER_APP_ID"),
        'client_secret' => env("SOCIAL_TWITTER_APP_SECRET"),
        'redirect' => env("SOCIAL_TWITTER_APP_REDIRECT"),
    ],
];
