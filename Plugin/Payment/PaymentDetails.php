<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 09:01 م
 */

namespace Plugin\Payment;


use Plugin\Payment\Contracts\PaymentDetails as PaymentDetailsInterFace;

class PaymentDetails implements PaymentDetailsInterFace
{


    private $amount = 0;
    private $userEmail = "no_mail@mail.com";
    private $paymentDescription = "no description";
    private $currencyCode = "USD";

    /**
     * PaymentDetails constructor.
     * @param array $options
     */
    public function __construct($options = ["userEmail" => "no_mail@mail.com", "paymentDescription" => "no description", "currencyCode" => "USD"])
    {
        foreach ($options as $name => $option) {
            $this->{$name} = $option;
        }

    }

    public function amount()
    {
        return $this->amount;
    }

    public function userEmail()
    {
        return $this->userEmail;
    }

    public function paymentDescription()
    {
        return $this->paymentDescription;
    }

    public function currencyCode()
    {
        return $this->currencyCode;
    }


}