<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 10:49 م
 */

namespace Plugin\Payment;


class ParchesDetails
{
    private $wpcEndpoint;
    private $purchaseDetailsEndpoint;
    private $response;

    public function __construct($token)
    {
        $config = PaymentConfig::instance();
        $this->wpcEndpoint = $config->getPathUrl();

        $this->purchaseDetailsEndpoint = $this->wpcEndpoint . '/purchase-details';
        $this->parms = array(
            "token" => $token,
            'merchantName' => $config->getWpcMerchantUserName(),
            'merchantSecretKey' => $config->getWpcMerchantSecretKey(),
        );

        $this->parms['sig'] = PaymentHelper::generateSignatureWpc($this->purchaseDetailsEndpoint, $this->parms, $config->getWpcMerchantSecretKey());
//        dd($this->parms);
    }

    public function getDetailsForToken()
    {
        $this->response = new Response(PaymentHelper::callWpc($this->purchaseDetailsEndpoint, $this->parms));
        return $this;
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }
}


/*



if ($reply != null && $reply->isSuccessful == 'true') {
    //Charged successfully!

    echo '<p>Successfully charged!</p>';
    echo '<p>Timestamp: '.$reply->timestamp.'</p>';
    echo '<p>Transaction ID: '.$reply->transactionId.'</p>';
    echo '<p>Token: '.$reply->token.'</p>';
    echo '<p>Request ID: '.$reply->requestId.'</p>';
}
else {
    //Error handling
    if ($reply) {
        echo '<p>We\'ve got some errors retrieving from WPC service.</p>';
        echo '<p>- Error Code: ' . $reply->errorCode . '</p>';
        echo '<p>- Error Message: ' . $reply->message . '</p>';
        if (isset($reply->errorField)) {
            echo '<p>- Error Field: ' . $reply->errorField . '</p>';
        }
    }
    else if (!empty($errorMessage)) {
        echo '<p>We\'ve got some errors when connecting to Secure WPC service.</p>';
        echo '<p>- Error Code: ' . $errorCode . '</p>';
        echo '<p>- Error Message: ' . $errorMessage . '</p>';
    }
}
 */