<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 08:49 م
 */

namespace Plugin\Payment\Contracts;


interface Response
{

    public function getToken();

    public function getErrorCode();

    public function getMessage();

    public function getErrorField();

}