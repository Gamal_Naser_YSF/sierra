<?php

namespace Plugin\Payment\Contracts;
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 08:19 م
 */

interface PaymentDetails
{
    public function amount();

    public function userEmail();

    public function paymentDescription();

    public function currencyCode();



}