<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 08:31 م
 */

namespace Plugin\Payment\Contracts;


interface ItemDetails
{
    public function name();

    public function quantity();

    public function amount();
}