<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 08:51 م
 */

namespace Plugin\Payment;


use ArrayAccess;
use Illuminate\Contracts\Support\Arrayable;
use Plugin\Payment\Contracts\Response as ResponseInterface;

class Response implements ResponseInterface, ArrayAccess, Arrayable
{
    public $errorCode;
    public $message;
    public $errorField;
    public $token;
    public $isSuccessful;
    private $data;


    public function __toString()
    {

        return \json_encode($this->data);

    }

    public function __construct($options)
    {
        $this->data = $options;
        foreach ($options as $name => $option) {
            $this->{$name} = $option;
        }

    }

    /**
     * @return mixed
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getErrorField()
    {
        return $this->errorField;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @return mixed
     */
    public function getisSuccessful()
    {
        return ($this->isSuccessful == "true");
    }


    /**
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     * @since 5.0.0
     */
    public function offsetExists($offset)
    {
        return isset($this->data[$offset]);
    }

    /**
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     * @since 5.0.0
     */
    public function offsetGet($offset)
    {
        return ($this->data[$offset]);
    }

    /**
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetSet($offset, $value)
    {
        return $this->data[$offset] = $value;
    }

    /**
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     * @since 5.0.0
     */
    public function offsetUnset($offset)
    {
        unset($this->data[$offset]);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        $data = (array)$this->data;
//        unset($data["exchangeRates"]);
//        unset($data["totalPurchases"]);
//        unset($data["isSuccessful"]);
//        $data["total"] = $data["total"]->USD;
        return $data;
    }
}