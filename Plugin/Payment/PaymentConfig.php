<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 08:05 م
 */

namespace Plugin\Payment;


class PaymentConfig
{

    private static $_instance;
    private $pathUrl;

    public static function instance()
    {
        if (!self::$_instance)
            self::$_instance = new static();


        return self::$_instance;

    }

    //World Payments Corp Config
    private $wpcTestMode = true;
    //Replace it by your vendor secret key
    private $wpcMerchantSecretKey = 'tumjMAXbrj9sV2W12kdLNvNAM|48naFQE_6ny|yZiHoaJhbLCotlUcWCb-Ky7mlMks5TrEOKh9V_q2J|r8khlcUx9B2gXn^eP43dIRY-_PYoSEnfNIx4UoB0ZDhT_g3N';
    //Replace it by your vendor user name
    private $wpcMerchantUserName = 'wvd_sierrab3yzwvxvnu8hglp79894lu';


    private function __construct()
    {

        $this->pathUrl = ($this->getWpcTestMode()) ? 'https://securetest.worldpaymentscorp.com/redirect-service' : 'https://secure.worldpaymentscorp.com/redirect-service';


    }

    /**
     * @return mixed
     */
    public function getWpcMerchantSecretKey()
    {
        return $this->wpcMerchantSecretKey;
    }

    /**
     * @return mixed
     */
    public function getWpcMerchantUserName()
    {
        return $this->wpcMerchantUserName;
    }

    /**
     * @return mixed
     */
    public function getWpcTestMode()
    {
        return $this->wpcTestMode;
    }

    /**
     * @return string
     */
    public function getPathUrl()
    {
        return $this->pathUrl;
    }


}