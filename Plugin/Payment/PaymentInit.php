<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/01/18
 * Time: 10:09 م
 */

namespace Plugin\Payment;


class PaymentInit
{

    private $wpcEndpoint;
    private $params;
    private $urlRequestToken;
    private $response;
    private $cancelUrl;

    private $returnUrl;

    public function __construct(PaymentDetails $PaymentDetails, $items = [], $merchToken = null)
    {
        $this->cancelUrl = url("/payment/canceled");
        $this->returnUrl = url("/payment/success");

        $config = PaymentConfig::instance();

        $this->wpcEndpoint = $config->getPathUrl();
        $this->urlRequestToken = $this->wpcEndpoint . '/request-token';
        $this->urlWebScreen = $this->wpcEndpoint . '/webscreen';

        $this->params = $this->createDataArray($config, $PaymentDetails, $items, $merchToken);
    }

    /**
     * @return $this
     */
    public function init()
    {

        $this->response = new Response(PaymentHelper::callWpc($this->urlRequestToken, $this->params));
        return $this;
    }

    /**
     * @return string
     */
    public function getWpcEndpoint()
    {
        return $this->wpcEndpoint;
    }

    /**
     * @return string
     */
    public function getUrlRequestToken()
    {
        return $this->urlRequestToken;
    }

    public function redirect()
    {

        if ($this->getResponse()->getisSuccessful()) {
            header('Location:' . $this->urlWebScreen . '?token=' . $this->getResponse()->getToken());
            die();
        }

        throw new \Exception("error in init payment request with error [ " . $this->getResponse() . " ]");
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    public function createDataArray(PaymentConfig $config, PaymentDetails $paymentDetails, $items = [], $merchantToken = null)
    {

        $merchantToken = (is_null($merchantToken)) ? time() : $merchantToken;

//Build request parameters
        $params = array(
            'amount' => $paymentDetails->amount(),                                           //Replace it by your total amount
            'cancelUrl' => $this->cancelUrl,   //Replace it by your cancel call back url
            'currencyCode' => $paymentDetails->currencyCode(),                                    //Replace it by your currency code
            'merchantName' => $config->getWpcMerchantUserName(),
            'merchantSecretKey' => $config->getWpcMerchantSecretKey(),
            'merchantToken' => $merchantToken,                                  //You can change it by your unique token builder
            'merchantReferenceCode' => $paymentDetails->paymentDescription(),  //Replace it by your description
//            'userEmail' => $paymentDetails->userEmail(),                          //Replace it by your buyer email
            'returnUrl' => $this->returnUrl,   //Replace it by your return call back url
            //You can specify up to 10 items, where n is a digit between 0 and 9, inclusive; except for digital goods, which supports only single payments (set n to 0).

        );

//        self::itemArrayT($items)[0]->quantity()

        foreach (self::itemArrayT($items) as $key => $item) {
            $params['item_' . $key . '_amt'] = $items->amount();
            $params['item_' . $key . '_name'] = $items->name();
            $params['item_' . $key . '_qty'] = $items->quantity();
        }

        $params['sig'] = PaymentHelper::generateSignatureWpc($this->urlRequestToken, $params, $config->getWpcMerchantSecretKey());
//        ksort($params);
//        dd(($params));
        return $params;
    }

    /**
     * @param array $items
     * @return ItemDetails[]
     */
    private static function itemArrayT(array $items)
    {
        return $items;
    }
}