LaravelErrors = function (errors) {

    var err = JSON.parse(errors).errors;

    $(".olderrorP").remove();
    return {

        showHints: function () {

            for (var i in err) {

                var input = $("[name=" + i + "]").first()

                for (var ind in err[i]) {
                    var ms = "validation error on: " + err[i][ind]

                    //try {
                    //    $(document).ready(function () {
                    //        Applexicon.notify.show(ms, "danger");
                    //    });
                    //} catch (e) {
                    //    alert(ms);
                    //}


                    input.closest("div").append(" <span style='display: block' class='olderrorP text-danger'>" + err[i][ind] + "</span>")
                }

            }
        }
    }
}