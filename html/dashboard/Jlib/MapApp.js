$.fn.createMap = (options) => {
    let defautOpt = {
        zoomLevel: 8,
        position: {lat: 0, lng: 2},
    };

    let opts = $.extend(defautOpt, options);

    // new google.maps.Map(this, {zoom: opts.zoomLevel, center: opts.position});
    return this;
}


// var map = function (target, options) {
//     var self = this;
//
//     // set default seting
//     // *******************
//     var settings = $.extend({
//
//         target: {
//             width: "100%",
//             height: "350px",
//         },
//     }, options);
//
//     var creatHtml = function (target, setting) {
//
//         return {
//             mapContener: $("<div class='map_contener col-lg-12'></div>"),
//             targetContener: $("<div id='" + target + "_contener'></div>"),
//             targetDom: $("#" + target),
//             init: function () {
//                 this.targetContener.append(this.mapContener);
//
//                 this.targetDom.parent().prepend(this.targetContener);
//
//                 this.targetDom.css({height: setting.target.height}).addClass("col-sm-12");
//
//                 this.targetDom.appendTo(this.mapContener);
//             },
//
//         }
//     }
//
//     self.html = creatHtml(target, settings)
//
//     /**
//      * create dom object from given id
//      * set some css for it
//      *
//      * @param target
//      * @param setting
//      * @returns {Element}
//      */
//     var createTarget = function (target) {
//         self.html.init();
//         return document.getElementById(target);
//     }
//
//
//     return {
//         markers: {},
//
//         /**
//          *
//          * @param uluru
//          * @param zoomLevel
//          * @returns {MapApp.map}
//          */
//         create: function (position, zoomLevel) {
//             this.map = new google.maps.Map(createTarget(target), {
//                 zoom: (zoomLevel) ? zoomLevel : 8,
//                 center: position
//             });
//             return this;
//         },
//         onClick: function (calBack) {
//             var self = this;
//             this.map.addListener('click', function (e) {
//                 calBack(e, self);
//             });
//             return this;
//         },
//         addMarkerOrUpdatePosition: function (id, position, callBack) {
//             var name = "mark" + id;
//
//             if (this.markers[name]) {
//                 new MoveSmothly(this.markers[name]).moveTo(position);
//                 if (callBack)
//                     callBack(this.markers[name]);
//             }
//             else {
//                 this.addMarker(id, position, callBack);
//             }
//
//
//             return this;
//         },
//         /**
//          *
//          * @param id
//          * @param position
//          * @returns {MapApp.map}
//          */
//         addMarker: function (id, position, callBack) {
//
//             var name = "mark" + id;
//
//             this.markers[name] = new google.maps.Marker({
//                 position: position,
//                 map: this.map
//             });
//
//
//             //this.markers[name].addListener('click', function (e) {
//             //    if (callBack)
//             //        callBack.onClick(e);
//             //});
//
//             if (callBack)
//                 callBack(this.markers[name]);
//
//             return this;
//         },
//         addLatLngInputs: function (callback) {
//             var form;
//             var inputs = {
//                 addressInput: $('<input class="form-control" name="address" placeholder="enter address"  />'),
//                 latInput: $('<input class="form-control" name="lat" placeholder="enter lat" readonly />'),
//                 lngInput: $('<input class="form-control" name="lng" placeholder="enter lng" readonly />')
//             }
//             //
//             form = $("<div class='col-sm-4'></div>")
//             //
//             form
//                 .prepend(inputs.latInput)
//                 .prepend(inputs.lngInput)
//                 .prepend(inputs.addressInput)
//
//             self.html.targetContener.prepend(form);
//             self.html.mapContener.addClass("col-sm-8").removeClass(("col-lg-12"));
//             if (callback)
//                 callback(inputs, this);
//             return this;
//         }
//     }
// }
//
// var MoveSmothly = function (marker) {
//     var self = this;
//     self.numDeltas = 100;
//     self.delay = 5; //milliseconds
//     self.marker = marker;
//
//     function moveMarker(marker, oldPosition, deltaLat, deltaLng, i) {
//         oldPosition.lat += deltaLat;
//         oldPosition.lng += deltaLng;
//
//         marker.setPosition(new google.maps.LatLng(oldPosition.lat, oldPosition.lng));
//
//         if (i != self.numDeltas) {
//             i++;
//             setTimeout(function () {
//                 moveMarker(marker, oldPosition, deltaLat, deltaLng, i);
//             }, self.delay);
//         }
//     }
//
//     return {
//         moveTo: function (newPosition) {
//
//             var oldPosition = self.marker.getPosition().toJSON();
//             var CastNewPosition = {
//                 lat: (isNaN(newPosition.lat)) ? newPosition.lat() : newPosition.lat,
//                 lng: (isNaN(newPosition.lng)) ? newPosition.lng() : newPosition.lng,
//             }
//
//
//             var i = 0;
//             var deltaLat = (CastNewPosition.lat - oldPosition.lat) / self.numDeltas;
//             var deltaLng = (CastNewPosition.lng - oldPosition.lng) / self.numDeltas;
//             moveMarker(self.marker, oldPosition, deltaLat, deltaLng, i);
//         }
//     }
// }

