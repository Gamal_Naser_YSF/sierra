function _load_overlay() {
    let div = $(`<div></div>`);

    div.css({
        background: "rgba(255, 255, 255, 0.90)",
        opacity: 1,
        position: "fixed",
        top: 0,
        left: 0,
        width: "100%",
        height: "100%",
        "z-index": 100,
        display: "none"
    });
    $("body").append(div)
    return div;
}

let _di = _load_overlay();
let Loader = {

    overlay: $(".fff-overlay"),
    show: () => {
        _di.fadeIn("100");
    },
    hide: () => {
        _di.fadeOut("100");
    },
}