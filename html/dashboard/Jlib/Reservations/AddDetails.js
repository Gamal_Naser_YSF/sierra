class AddMoreDetails {

    constructor() {
        this.count = 0;
    }


    _getHtml($name, $val) {
        let html = `           
            <div class="col-lg-4">
                <label>Key</label>
                <input name="details[${this.count}][key]" class="form-control" value="${$name || ""}">
            </div>
            <div class="col-lg-8">
                <label>Value</label>
                <textarea rows="10" name="details[${this.count}][val]" class="form-control">${$val || ""}</textarea>
            </div>
            <hr/>
            `;
        this.count++;

        return html;
    }

}