(function () {
    let
        fechDataBTN = $("#_fetchUserData"),
        UIDHolder = $("#uidHolder"),
        changeUID = $("#_changeUID"),
        moreDetails = $(".moreDetails"),
        detailsControl = new AddMoreDetails(),
        saveBTN = $(".sbForm"),
        TForm = $("#transForm");


    let setUserData = function (user) {
        $("#_email").text(user.email);
        $("#_phone").text(user.phone);
        $("#_username").text(user.name);
        $("#_address").text(user.address);
        $("#infoSec").show("slow")
        moreDetails.show("slow")
    }
    let resetsetUserData = function () {
        $("#_email").text("");
        $("#_phone").text("");
        $("#_username").text("");
        $("#_address").text("");
        $("#infoSec").hide("slow")
        moreDetails.hide("slow")
    }

    function insertDetailsSection(name, val) {
        $("#newDetails").append(detailsControl._getHtml(name, val));
    }

    function fecheUser() {
        Loader.show();
        fechDataBTN.prop("disabled", true);
        UIDHolder.prop("readonly", true);
        let val = UIDHolder.val();

        if (val.length > 0) {

            $.post(url + "/admin/users/fetch/" + val, {_token: csrf_token}, function (res) {
                if (res.status) {
                    setUserData(res.data);
                } else {
                    UIDHolder.prop("readonly", false);
                    swal("Oops", "wrong or invalid ID", "error")
                    fechDataBTN.prop("disabled", false);
                    resetsetUserData()
                }
                Loader.hide();
            })
        } else {
            resetsetUserData()
            swal("Oops", "sorry please insert data", "error")
            Loader.hide();
            UIDHolder.prop("readonly", false);
            fechDataBTN.prop("disabled", false);
        }


    }

    fechDataBTN.click(() => fecheUser());
    UIDHolder.keyup((e) => {
        e.preventDefault();
        if (e.keyCode === 13) {
            fecheUser();
            return false;
        }
    });
    changeUID.click(() => {
        fechDataBTN.prop("disabled", false);
        UIDHolder.prop("readonly", false).val("").focus();
    })


    TForm.keyup(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }

    })

    saveBTN.click(function () {
        Loader.show();
        saveBTN.prop("disabled", true);

        let formData = new FormData(TForm[0]);
        $.ajax({
            url: TForm.attr("action"),
            type: TForm.attr("method"),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.status)
                    window.location.href = data.redirect_url;
            },
            error: function (data) {
                let res = JSON.parse(data.responseText);
                console.log(res, data);
                let msg = "";
                for (let err in res.errors) {
                    res.errors[err].forEach(function (e) {
                        msg += e + "\n";
                    })
                }

                swal(res.message, msg, "error");
                Loader.hide();
                saveBTN.prop("disabled", false);
            }
        });

        // $("#transForm").submit();
    })

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

    if (UIDHolder.val() != "" && UIDHolder.val().length > 0)
        fecheUser()


    insertDetailsSection("details", getParameterByName("text"));

    $("#insertNewSection").click(() => insertDetailsSection())
})()