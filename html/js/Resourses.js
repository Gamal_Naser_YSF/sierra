let R = {
    events: {
        setNewRate: function (eventId, Rate, cb) {

            $.ajax({
                url: baseUrl() + "/rate-event",
                method: "POST",
                data: {_token: _token(), rate: Rate, event_id: eventId},
                success: function (res) {
                    cb(res)
                },
                error: function (err) {
                    let res = err.responseJSON;
                    res.status = false;
                    cb(res);

                }
            });

        }
    }
}


