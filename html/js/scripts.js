//hide loading
$(window).on("load", function () {
    var myNav = $('#scrollnav');
    $('.fixed-loading').fadeOut(1000).remove();
    $('body').css('padding-top', myNav.outerHeight() + 'px');
});
//end loading

$(document).ready(function () {
    //variables
    var body = $('body'),
        myNav = $('#scrollnav'),
        slider_img = $('.hero-slider .slide-img');

//reg popup
    $(".reg-popup").niceScroll({
        cursorwidth: '0',
        cursorborder: '0',
        scrollspeed: 120,
        horizrailenabled: false,
        mousescrollstep: 40
    });
    $('.btnReg').click(function () {
        $(".reg-popup").css('overflow', 'visible').css('overflow', 'hidden');
    });

    var bodyOverlay = $('.body-overlay');
    $('.reg_show').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('active').siblings().removeClass('active');
        $('.reg-popup').slideDown();
        $($(this).data('form')).slideDown().siblings().hide();
        bodyOverlay.fadeIn();
    });
    $('.loginReg_show').on('click', function (e) {
        e.preventDefault();
        $('.reg-btn').addClass('active').siblings().removeClass('active');
        $($(this).data('form')).slideDown().siblings().hide();
    });
    $('.close-popup').on('click', function () {
        $('.reg-popup').slideUp();
        $('.reg_show').removeClass('active');
        bodyOverlay.fadeOut();
    });
    bodyOverlay.on('click', function () {
        if ($(this).hasClass('dim')) {
            $('#login_message').fadeIn();
        } else {
            $(this).fadeOut();
            $('.reg-popup').slideUp();
            $('.reg_show').removeClass('active');
        }

    });
    $('.body-overlay-dim').on('click', function () {
        $(this).fadeIn();
        $('.reg-popup').slideDown();
    });
    $('.prevdef').on('click', function (e) {
        e.preventDefault()
    });


    //rate
    $('.rate_generate').each(function () {
        var rateCount = $(this).data('rate') - 1,
            rates_items = $(this).find('.rate-item');
        for (var i = 0; i <= rateCount; i++) {
            $(rates_items[i]).attr('class', 'rate-item icon-star-full')
        }
    });

    generateRating();
    //hover rate action
    $('.user_rate .rate-item').hover(function () {
        var rateCount = $(this).index(),
            rates_items = $(this).parent().find('.rate-item');
        rates_items.attr('class', 'rate-item icon-star-empty');
        for (var i = 0; i <= rateCount; i++) {
            $(rates_items[i]).attr('class', 'rate-item icon-star-full')
        }
    }, function () {
        generateRating()
    });

    //Generate Rats
    function generateRating() {
        $('.user_rate').each(function () {
            var rateCount = $(this).data('rate-value') - 1,
                rates_items = $(this).parent().find('.rate-item');
            rates_items.attr('class', 'rate-item icon-star-empty');
            for (var i = 0; i <= rateCount; i++) {
                $(rates_items[i]).attr('class', 'rate-item icon-star-full')
            }
        })
    }


    // // Rate Click
    // $('.user_rate .rate-item').click(function () {
    //     Events.emit("updateRate", $(this))
    // });
    //
    // //msg alert
    // $('.trigger-msg').click(function (e) {
    //     e.preventDefault();
    //     Events.emit("bookTravelPackages", $(this))
    // });
    body.on('click', '.close-msg', function () {
        $(this).parents('.msg-wrapper').siblings('.trigger-msg').removeClass('has_msg');
    });

    //body badding


    //open and close left menu
    $('#menu_opener').on('click', function () {
        $('#menu_left').addClass('menu-show');
    });
    $('#menu_close').on('click', function () {
        $('#menu_left').removeClass('menu-show');
    });


    //hero full height
    slider_img.height($(window).height() - myNav.outerHeight());
    $(window).on('resize', function () {
        slider_img.height($(window).height() - myNav.outerHeight());
        body.css('padding-top', myNav.outerHeight() + 'px');
    });


// smoothScroll
    $('.smoothScroll').smoothScroll({
        speed: 600,
        offset: -100
    });


//lazy load
    // add class lazy and  data-original to the img
    $(function () {
        $(".lazy").lazyload({
            effect: "fadeIn"
        });
    });


//chosen select box customize
    $(".chosen-select").chosen({
        no_results_text: "Oops, nothing found!",
        width: "100%",
        placeholder_text_multiple: "Select Some Options",
        placeholder_text_single: "Select an Option"
    });
//end chosen select

//back to top btn click
    var bacToTop = $('#back-to-top');
    bacToTop.click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
//show and hide back to top button and add nav-bar fixed top

    if ($(window).scrollTop() > 250) {
        bacToTop.fadeIn();
    } else {
        bacToTop.fadeOut();
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() > 250) {
            bacToTop.fadeIn();
        } else {
            bacToTop.fadeOut();
        }
    });
//end back to top

    //add Scrolled action to the selected section
    $('.catch-scroll').each(function () {
        if ($(window).scrollTop() > $(this).offset().top - 200) {
            $(this).addClass('scrolled-action');
        } else {
            $(this).removeClass('scrolled-action');
        }
    });
    $(window).on('scroll', function () {
        $('.catch-scroll').each(function () {
            if ($(window).scrollTop() > $(this).offset().top - 200) {
                $(this).addClass('scrolled-action');
            } else {
                $(this).removeClass('scrolled-action');
            }
        });

    });

//sliders
    var video_hero = $('.video-slider');
    video_hero.owlCarousel({
        items: 1,
        loop: false,
        autoplay: false,
        video: true,
        rewind: true,
        smartSpeed: 400,
        nav: true,
        dots: true,
        navElement: 'i',
        navText: ['', ''],
        navClass: ['hero-prev nav-prev icon-angle-left', 'hero-next nav-next icon-angle-right'],
        onInitialized: function () {
            if ($(".owl-item.active video", this.$element).length) {
                $(".owl-item.active video", this.$element)[0].play();
                video_hero.trigger('stop.owl.autoplay');
                $(".owl-item.active video", this.$element).on('ended', function () {
                    video_hero.trigger('play.owl.autoplay')
                });
            }
            callback(video_hero)
        },
        onTranslated: function () {
            if ($(".owl-item.active video", this.$element).length) {
                $(".owl-item.active video", this.$element)[0].play();
                video_hero.trigger('stop.owl.autoplay');
                $(".owl-item.active video", this.$element).on('ended', function () {
                    video_hero.trigger('play.owl.autoplay')
                });
            }
            callback(video_hero)
        }
    });

    //owl
    var owl_hero = $('.owl-hero');
    owl_hero.owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        rewind: false,
        autoplayTimeout: 6000,
        smartSpeed: 300,
        nav: true,
        dots: true,
        navElement: 'i',
        navText: ['', ''],
        navClass: ['hero-prev nav-prev icon-arrow-left', 'hero-next nav-next icon-arrow-right'],
        onTranslated: function () {
            callback(owl_hero)
        },
        onInitialized: function () {
            callback(owl_hero)
        }
    });

    function callback(hero) {
        hero.find('.animate-cont-hidden').each(function () {
            $(this).hide();
        });
        var $animatingElems = hero.find('.owl-item.active').find("[data-animation ^= 'animated']").show();
        doAnimations($animatingElems);
    }

    var owl_multi = $('.owl-multi');
    owl_multi.owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        rewind: false,
        autoplayTimeout: 5000,
        smartSpeed: 300,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            // breakpoint from 480 up
            320: {
                items: 2
            },
            // breakpoint from 768 up
            768: {
                items: 4
            },
            992: {
                items: 6
            }
        }
    });

    var owl_single = $('.owl-single');
    owl_single.owlCarousel({
        items: 1,
        autoplay: true,
        loop: true,
        autoplayTimeout: 5000,
        smartSpeed: 300,
        nav: false,
        dots: true,
        navElement: 'i',
        navText: ['', ''],
        navClass: ['hero-prev nav-prev icon-arrow-left', 'hero-next nav-next icon-arrow-right'],
        onTranslated: function () {
            callback(owl_hero)
        },
        onInitialized: function () {
            callback(owl_hero)
        }
    });

//end sliders

    //redo animate
    function doAnimations(elems) {
        //Cache the animationend event in a variable
        var animEndEv = 'webkitAnimationEnd animationend';
        elems.each(function () {
            var $this = $(this),
                $animationType = $this.data('animation');
            $this.addClass($animationType).one(animEndEv, function () {
                $this.removeClass($animationType);
            });
        });
    }

    //tabs function
    $('.tabs-wrapper').on('click', '.switch-link', function () {
        $(this).addClass('active').siblings().removeClass('active');
        var tabItem = $($(this).data('target')),
            tabbg = $(this).data('bg');
        tabItem.show().addClass('.active').siblings().hide().removeClass('.active');
        $('#conferences_sec').delay(1500).css({
            'background': 'url("' + tabbg + '") left center no-repeat #f9f9f9',
            'background-size': 'cover'
        })
    });

    //count to function
    /*    var countFlag = false;
        $(window).scroll(function () {
            if (!countFlag) {
                isScrolledIntoView($('.timer'));
            }
        });

        function isScrolledIntoView(elem) {
            var docViewTop = $(window).scrollTop();
            var docViewBottom = docViewTop + $(window).height();

            var elemTop = $(elem).offset().top;
            var elemBottom = elemTop + $(elem).height();

            if ((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) {
                countFlag = true;
                $(".timer").countTo({
                    onComplete: function () {

                    }
                });
            }
        }*/
});
//end document ready


//start nice scroll
$("html").niceScroll({
    cursorcolor: '#febe0e',
    cursorwidth: '6px',
    cursorborder: '2px',
    cursorborderradius: '10px',
    zindex: '9999999999999',
    scrollspeed: 110,
    horizrailenabled: false,
    mousescrollstep: 40,
    touchbehavior: false,
    preventmultitouchscrolling: false
});
//end nice scroll

//wow js
new WOW().init();
//end wow js


