let __UserTransactions = null;

class UserTransactions {

    static getInstanse() {
        if (!__UserTransactions)
            __UserTransactions = new UserTransactions();
        return __UserTransactions;
    }

    constructor() {
        this.url = getUrl() + "/transactions";
        this.storgeKey = "UserTransactions";
    }

    appendData(data) {
        let oldData = this.getData();
        localStorage.setItem(this.storgeKey, JSON.stringify({dat: oldData.concat(data)}));
    }

    getData() {
        let data = JSON.parse(localStorage.getItem(this.storgeKey));
        if (!data)
            return [];

        return data.dat;
    }

    fetch() {
        return new Promise((res) => {
            if (this.url) {
                $.get(this.url, {}, (response) => {
                    this.url = response.next_page_url;
                    this.appendData(response.data);
                    res(true);
                });
            }
            res(true);
        })
    }
}