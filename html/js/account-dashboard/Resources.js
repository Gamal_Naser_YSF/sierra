let getEventsList = function (path, cb) {
    getEvent(path, "list", function (res) {
        cb(res.events);
    })
}
let getEventsCalender = function (path, cb) {
    getEvent(path, "calender", function (res) {
        cb(res.events);
    })
}
let getEvent = function (path, type, cb) {
    $.post(getUrl() + "/events/get-events", {_token: getToken(), type: path, ft: type}, (res) => {
        cb(res);
    })
}

let Resources = {
    user: {
        storgKey: "userInfo",
        update: function (data, cb) {
            data._token = getToken();
            $.post(getUrl() + "/profile/update", data, function (res) {
                if (res.status)
                    cb(true);
                else {
                    let v = "";
                    for (let inbutName in res.errors) {
                        let input = res.errors[inbutName];
                        for (let err in input) {
                            v += input[err] + "\n";
                        }
                    }
                    alert(v)
                    cb(false);
                }
            })
        },
        clear: function () {
            localStorage.removeItem(this.storgKey);
        },
        getInfo: function (cb) {
            let data, storgKey = this.storgKey;
            if (data = localStorage.getItem(storgKey)) {
                cb(JSON.parse(data));
            } else {
                $.post(getUrl() + "/profile/get", {_token: getToken()}, (res) => {
                    localStorage.setItem(storgKey, JSON.stringify(res.userInfo));
                    cb(res.userInfo);
                });
            }
        },
        updataLocalStorge: function (data) {
            localStorage.setItem(this.storgKey, JSON.stringify(data));
        }
    },
    calender: {
        getList: function (cb) {
            let data, storgKey = "myEvent";
            if (data = localStorage.getItem(storgKey)) {
                cb(JSON.parse(data));
            } else {
                getEventsCalender(storgKey, (res) => {
                    localStorage.setItem(storgKey, JSON.stringify(res));
                    cb(res);
                })
            }
        }
    },
    events: {
        getMyEvent: function (cb) {
            let data, storgKey = "myEvent";
            if (data = localStorage.getItem(storgKey)) {
                cb(JSON.parse(data));
            } else {
                getEventsList(storgKey, (res) => {
                    localStorage.setItem(storgKey, JSON.stringify(res));
                    cb(res);
                })
            }
        },
        getActiveEvent: function (cb) {
            let data, storgKey = "activeEvent";
            if (data = localStorage.getItem(storgKey)) {
                cb(JSON.parse(data));
            } else {
                getEventsList(storgKey, (res) => {
                    localStorage.setItem(storgKey, JSON.stringify(res));
                    cb(res);
                })
            }
        },
        getUpcomingEvents: function (cb) {
            let data, storgKey = "upcomingEvent";
            if (data = localStorage.getItem(storgKey)) {
                cb(JSON.parse(data));
            } else {
                getEventsList(storgKey, (res) => {
                    localStorage.setItem(storgKey, JSON.stringify(res));
                    cb(res);
                })
            }
        },
        deleteItem: function (id, cb) {

            $.post(getUrl() + "/events/un-subscribe", {id, _token: getToken()}, function (response) {
                if (response.status) {
                    let data, storgKey = "myEvent";
                    data = JSON.parse(localStorage.getItem(storgKey));
                    delete data[id]
                    localStorage.setItem(storgKey, JSON.stringify(data))
                    cb(false);
                }
            });


        }
    },
    reservations: {
        all: function (cb) {
            $.get(getUrl() + "/reservations", {}, function (response) {
                cb(response.data);
            });
        }
    }
}