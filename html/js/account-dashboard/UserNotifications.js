let __UserNotifications = null;

class UserNotifications {

    static getInstanse() {
        if (!__UserNotifications)
            __UserNotifications = new UserNotifications();
        return __UserNotifications;
    }

    constructor() {

        this.url = getUrl() + "/notifications";
        this.storgeKey = "UserNotifications";
    }

    appendData(data) {
        let oldData = this.getData();
        localStorage.setItem(this.storgeKey, JSON.stringify({dat: oldData.concat(data)}));
    }

    getData() {
        let data = JSON.parse(localStorage.getItem(this.storgeKey));
        if (!data)
            return [];

        return data.dat;
    }

    fetch() {
        return new Promise((res) => {
            if (this.url) {
                $.get(this.url, {}, (response) => {
                    this.url = response.next_page_url;
                    this.appendData(response.data);
                    res(true);
                });
            }
            res(true);
        })
    }
}