Events.on("updateUserInfo", function () {
    Resources.user.getInfo(function (userinfo) {
        $("#userNameInfo").html(userinfo.name)
        $(".user-img").attr("src", userinfo.avatar)
    })
});

Events.on("loadData", function (targetName) {
    console.log(targetName);
    SectionsLoader[targetName]((item) => {
        if (typeof item.runBefore !== "undefined")
            item.runBefore();

        $("#" + targetName).html(item.getHtml())

        if (typeof item.runAfter !== "undefined")
            item.runAfter();
    })
})


Events.on("updateEventsCount", function () {
    Resources.events.getMyEvent(function (eventList) {
        $("#myEventCounter").html(Object.keys(eventList).length)
    })
})

Events.on("deleteItem", function (options) {
    if (confirm('are you sure you want to delete this event ?!')) {
        Resources.events.deleteItem(options.id, function (err) {
            if (!err)
                Events.emit("updateEventsCount");

            options.done(!err)
        })
    }

})


Events.on("sendDataToServer", function () {
    let data = {
        name: $("#input_name").val(),
        email: $("#input_email").val(),
        phone: $("#input_phone").val(),
        address: $("#input_address").val(),
    }
    Resources.user.update(data, function (res) {
        if (res) {
            Resources.user.clear();
            // Events.emit("updateUserInfo")
            Events.emit("loadData","sec_profile")
        }
    })
});