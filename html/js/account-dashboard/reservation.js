let __ReservationInstance = null;

class ReservationData {

    static getInstanse() {
        if (!__ReservationInstance)
            __ReservationInstance = new ReservationData();
        return __ReservationInstance;
    }

    constructor() {
        console.log("init");
        this.url = getUrl() + "/reservations";
        this.storgeKey = "RservationData";
    }

    appendData(data) {
        let oldData = this.getData();
        localStorage.setItem(this.storgeKey, JSON.stringify({dat: oldData.concat(data)}));
    }

    getData() {
        let data = JSON.parse(localStorage.getItem(this.storgeKey));
        if (!data)
            return [];

        return data.dat;
    }

    fetch() {
        return new Promise((res) => {
            if (this.url) {
                $.get(this.url, {}, (response) => {
                    this.url = response.next_page_url;
                    this.appendData(response.data);
                    res(true);
                });
            }
            res(true);
        })
    }
}