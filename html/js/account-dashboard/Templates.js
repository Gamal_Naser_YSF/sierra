Templates = {
    sec_calender: function (div) {
        return `<div class="tab-wrapper calender-section">
            <div class="secondary-tabs">
                <div class="tab-content">
                   ${div}
                </div>
             </div>
        </div>`
    },
    sec_events: function (allEvents) {
        return `<div class="tab-wrapper events-section">
                    <div class="secondary-tabs">
                        <ul class="tab-list">
                            <li class="list-item">
                                <a href="#" data-target="#tab_active_events" class="tab_trigger link-item">
                                    Active Events
                                </a>
                            </li>
                            <li class="list-item">
                                <a href="#" data-target="#tab_upcoming_events" class="tab_trigger link-item">
                                    Upcoming Events
                                </a>
                            </li>
                            <li class="list-item active">
                                <a href="#" data-target="#tab_my_events" class="tab_trigger link-item">
                                    my Events
                                </a>
                                <span id="myEventCounter" class="my-events-counter counter">${allEvents.myEventsLength || ""}</span>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab_active_events" class="user-content tab-item">
                                <div class="event-list">
                                ${allEvents.activeEvents || ""}
                                </div>
                            </div>
                            <div id="tab_upcoming_events" class="user-content tab-item">
                                <div class="event-list">
                                    ${allEvents.upcomingEvents || ""}   
                                </div>
                            </div>
                            <div id="tab_my_events" class="user-content tab-item active">
                                <div class="event-list">
                                     ${allEvents.myEvents || ""}        
                                </div>
                            </div>
                        </div>
                    </div>
                </div>`;
    },
    deleteabelEvent: function (event) {
        return `<div class="single-event profile-ev">${Templates.row_event(event)}<div data-item-id="${event.id}" class="delete_item"><i class="icon-cross"></i></div></div>`;
    },
    unDeleteabelEvent: function (event) {
        return `<div class="single-event">${Templates.row_event(event)}</div>`;
    },
    row_event: function (event) {
        return `<div class="row">
                    <div class="col-lg-4">
                        <div class="img-cont">
                            <img src="${event.image || ""}" class="img-fluid w-100">
                            <div class="ev-date">
                                <span class="strong">${event.day || ""}</span>
                                <span>${event.mon || ""}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="ev-desc">
                            <div class="ev-title myDesc">
                                ${event.title || ""}
                            </div>
                            <div class="smallDesc">
                                ${event.desc || ""}
                            </div>
                            <div class="text-right">
                                <a class="btn-theme-main-reversed smallDesc" href="/event-details/${event.id}">
                                    show details
                                </a>
                            </div>
                        </div>
                    </div>
                </div>`;
    },
    sec_profile: function (userInfo, activaties) {
        return `<div class="tab-wrapper user-section">
                    <div class="secondary-tabs">
                       ${Templates.userSectionTabs()}
                        <div class="tab-content">
                           ${Templates.item_profile(userInfo, activaties)}
                           ${Templates.item_friends()}
                        </div>
                    </div>
                </div>`;
    },
    item_profile: function (userInfo, activaties) {
        return ` <div id="item_profile" class="user-content tab-item active">
                    ${Templates.userInfoTopSection(userInfo)}
                    <div class="account-info">
                        <div class="row">
                            <div class="col-md">
                                <div class="item-box">
                                    <div class="box-title">
                                        User Information
                                        <a data-form="#information_form" href="#" class="edit-btn form_edit">
                                            <i class="icon-edit3"></i>
                                        </a>
                                    </div>
                                    ${Templates.userform(userInfo)}
                                </div>
                            </div>
                            <!--<div class="col-md">-->
                                <!--<div class="item-box">-->
                                    <!--<div class="box-title">-->
                                        <!--Account Information-->
                                        <!--<a data-form="#account_form" href="#" class="edit-btn form_edit">-->
                                            <!--<i class="icon-edit3"></i>-->
                                        <!--</a>-->
                                    <!--</div>-->
                                     <!--$Templates.userAccountForm(userInfo)}                               -->
                                <!--</div>-->
                            </div>
                        </div>
                    </div>
                    ${Templates.activatesSection(activaties)}             
                </div>`;
    },
    item_friends: function () {
        return "";
        return ` <div id="item_friends" class="user-content tab-item">
                    Friends
                 </div>`;
    },
    userSectionTabs: function () {
        return `<ul class="tab-list">
                    <li class="list-item active">
                        <a href="#" data-target="#item_profile" class="tab_trigger link-item">
                            My Page
                        </a>
                    </li>
                    <!--<li class="list-item">-->
                        <!--<a href="#" data-target="#item_friends" class="tab_trigger link-item">-->
                            <!--Friends-->
                        <!--</a>-->
                    <!--</li>-->
                </ul>`;
    },
    userInfoTopSection: function (userInfo) {
        return `<div class="user-main">
                    <div class="media">
                        <div class="img-cont mr-3">
                            <img style="max-width: 220px" class="img-fluid user-img" src="${userInfo.avatar || ""}">
                            <a id="userProfileUploaderBtn" class="upload-btn " href="javascript:void(0)"><i class="icon-edit3"></i></a>
                        </div>
                        <div class="media-body">
                            <h2 class="smallHeading my-3"> ${userInfo.name || ""}</h2>
                            <div><i class="icon-location"></i> ${userInfo.address || ""} </div>
                        </div>
                    </div>
                </div>`;
    },
    userform: function (userInfo) {
        return `<form id="information_form" class="info-form smallDesc">
                    <div class="form-group row no-gutters">
                        <label for="input_name" class="col-sm-4 col-form-label">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="input_name" value="${userInfo.name || ""}" readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_company" class="col-sm-4 col-form-label">email</label>
                        <div class="col-sm-8">
                            <input type="email" class="form-control-plaintext"  id="input_email" value="${userInfo.email}" readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_title" class="col-sm-4 col-form-label">phone</label>
                        <div class="col-sm-8">
                            <input type="text"  class="form-control-plaintext"  id="input_phone" value="${userInfo.phone}"  readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_username" class="col-sm-4 col-form-label">address</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext"  id="input_address"   value="${userInfo.address}"  readonly>
                        </div>
                    </div>
                </form>`;
    },
    userAccountForm: function (userInfo) {
        return `<form id="account_form" class="info-form smallDesc">
                    <div class="form-group row no-gutters">
                        <label for="input_email" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="input_email" value="moradoo@gmail.com" readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_phone" class="col-sm-4 col-form-label">Company</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="input_phone" value="(685) 944-4673" readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_email" class="col-sm-4 col-form-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="input_email" value="moradoo@gmail.com" readonly>
                        </div>
                    </div>
                    <div class="form-group row no-gutters">
                        <label for="input_phone" class="col-sm-4 col-form-label">Company</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control-plaintext" id="input_phone" value="(685) 944-4673" readonly>
                        </div>
                    </div>
               </form>`;
    },
    activatesSection: function (activaties) {
        return "";
        return `<div class="activity-section">
                    <div class="sec-head">
                        <div class="row align-items-center">
                            <div class="col-md">
                                <div class="myDesc">Recent Activity</div>
                                <div class="smallDesc">From your History</div>
                            </div>
                            <div class="col-md text-md-right">
                                <select class="form-control" id="exampleFormControlSelect1">
                                    <option>Activity filter</option>
                                    <option>filter 1</option>
                                    <option>filter 2</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="sec-content">
                        <ul class="list-unstyled">
                            ${activaties || ""}
                        </ul>
                    </div>
                </div>`;
    },
    activityRow: function (activity) {
        return `<li class=" list-item">
                    <a class="media" href="#">
                        <div class="mr-3 item-head">
                            <div>12459</div>
                            <i class="icon-eye"></i>
                        </div>
                        <div class="media-body smallDesc">
                            <h4 class="smallDesc mb-1">
                                International Conference on
                                Economics
                                and Business
                                Research
                            </h4>
                            Thank you everyone for all of
                            your
                            supports.
                            Please notice days off schedule
                            ...
                        </div>
                    </a>
           </li>`;
    },
    reservationItem: function (item) {
        console.log(item);
        return `<div class="single-res">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="img-cont">
                                                <img src="upload/${item.image}" class="img-fluid w-100">
                                            </div>
                                        </div>
                                        <div class="col-lg-8">
                                            <div class="ev-desc">
                                                <div class="ev-title myDesc">
                                                   ${item.title || ""}
                                                </div>
                                                <div class="smallDesc">
                                                    <strong> Invoice No:</strong> ${item.id}<br>
                                                    <strong> Created:</strong> ${item.created_at}<br>
                                                    <strong> Fees:</strong> ${item.fees}<br>
                                                </div>
                                                <div class="text-right">
                                                    <a href="javascript:void(0)" class="btn-theme-second-reversed smallDesc show-res-invoice">Invoice Details</a>
                                                </div>
                                            </div>
                                        </div>
                                        ${Templates.reservationItemData(item, JSON.parse(item.details))}
                                    </div>
                                </div>`
    },
    reservationSeaction: function () {

        return `    
                <div class="tab-wrapper events-section">
                    <div class="secondary-tabs">
                        <div class="tab-content">
                            <h2 class="sec-head myDesc">My Reservations</h2>
                            <div class="reservations-list">
                               
                            </div>
                            <div>
                            <button id="moreReservation" class="btn btn-default btn-block">load mor</button>
</div>
                        </div>
                    </div>
                </div>`

    },
    reservationItemData: function (item, itemDetails) {
        let ht = ""
        for (let itm in itemDetails)
            ht += ` <strong>${itm}: </strong> ${itemDetails[itm]} <br>`


        return `
                <div class="col-lg-12">
                    <div id="" class="invoice-section" style="display:none;">
                        <h3 class="myDesc">Invoice INV16368</h3>
                        <div class="data-view smallDesc">
                            <div class="top-details">
                            ${ht}
                            </div>
                            <table class="table invoice-table">
                                <thead>
                                    <tr>
                                        <th scope="col">Detail</th>
                                        <th scope="col">Price</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <th scope="row">Subtotal</th>
                                        <td>USD ${item.fees}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">tax</th>
                                        <td>USD 0</td>
                                    </tr>
                                      <tr>
                                        <th scope="row">vat</th>
                                        <td>USD 0</td>
                                    </tr>
                                    <tr class="price">
                                        <th scope="row">Total</th>
                                        <td >USD ${item.fees}</td>
                                    </tr>
                                    <tr>
                                        <td>Reservation Fee Required:</td>
                                        <td >USD ${item.fees}</td>        
                                    </tr>
                                      <tr>
                                        <td colspan="2"><a href="${item.payment_url}" class="btn btn-warning btn-block">PAY NOW !</a></td>        
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>`;
    },
    notification: function () {
        return `
                <div class="tab-wrapper notification-section">
                    <div class="secondary-tabs">
                        <div class="tab-content">
                            <h3 class="myDesc">Notifications</h3>
                            <ul class="notifications-list">
                                                                                     
                            </ul>
                            <div class="text-center mt-5">
                                <a id="moreNotifiction" href="javascript:void(0)" class="btn btn-theme-main-reversed smallDesc">show more</a>
                            </div>
                        </div>
                    </div>
                </div>`;
    },
    notificationSingleItem: function (notif) {
        return `   
                    <li class="single-item">
                        <div class="notification-items">
                            <div class="myDesc notification-title">
                                ${notif.title}
                            </div>
                            <div class="smallDesc">
                                ${notif.message}
                            </div>
                            <!--<div class="text-right mt-3">-->
                                <!--<a href="#" class="btn btn-theme-main smallDesc">view item</a>-->
                            <!--</div>-->
                        </div>
                    </li>`;
    },
    payments: function () {
        return ` 
                <h4>Payment history</h4>
                <hr/>
                <div class="payment-history">
                   
                </div>
                <div class="text-center mt-5">
                    <a id="moreTransactions" href="javascript:void(0)" class="btn btn-theme-main-reversed smallDesc">show more</a>
                </div>
`;
    },
    getTransactionDetails: function (item) {
        // "object"
        let detailsHtml = "";
        for (let cont in item.contact) {
            if (typeof item.contact[cont] == "object") {
                for (let cont2 in item.contact[cont]) {
                console.log(item.contact[cont][cont2]);
                detailsHtml += Templates.transactionDetails(cont2, item.contact[cont][cont2])
                }
            } else {
                detailsHtml += Templates.transactionDetails(cont, item.contact[cont])
            }
        }

        for (let detail in item.details) {
            detailsHtml += Templates.transactionDetails(detail, item.details[detail])
        }
        return detailsHtml;
    },
    transactionItem: function (item) {
        let detailsHtml = Templates.getTransactionDetails(item);
        return `
        <div class="payment-item">
                        <div class="payment-details">
                            <h6>Transaction #${item.requestId}</h6>
                           ${Templates.transactionDetails("Submit on : ", item.time.date)}
                       ${detailsHtml}
                        </div>
                        <div class="payment-show">
                            <div>
                                <p><strong>Sub-total : <span> ${item.fees.subTotal}</span></strong></p>
                                <p>Discount : <span>${item.fees.discount}</span></p>
                                <p>VAT : <span> ${item.fees.VAT}</span></p>
                            </div>
                            <div>
                                <h2>USD ${item.fees.total}</h2>
                            </div>
                        </div>
                    </div>
`
    },
    transactionDetails: function ($key, $val) {
        return `
                    <p>
                        <span>${$key} </span>
                        <span class="payment-details-desc">${$val}</span>
                    </p>
        `;
    }
};