var SectionsLoader = {
    sec_profile: function (cb) {
        Resources.user.getInfo(function (userInfo) {
            cb({
                getHtml: function () {
                    return Templates.sec_profile(userInfo);
                },
                runBefore: function () {
                    $(".user-img").defaultImg("cdn/dashboard/user-def.png");
                },
                runAfter: function () {
                    Events.emit("updateUserInfo")

                    $("#userProfileUploaderBtn").click(function () {
                        $("#myModal").modal("show")
                    });

                    $("#uploadUserImage").click(function () {
                        let imageFile = $("[name=avatar]")[0].files[0]
                        if (imageFile == undefined) {
                            $("#uploaderError").text("please select image file first").show()
                            return;
                        }
                        $("#uploaderError").text("").hide()

                        let formData = new FormData();

                        formData.append("_token", getToken())
                        formData.append("avatar", imageFile)

                        $.ajax({
                            url: getUrl() + "/profile/upload-image",
                            data: formData,
                            type: 'POST',
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (res) {
                                console.log(res)
                                Resources.user.updataLocalStorge(res.userInfo);
                                Events.emit("updateUserInfo");
                                $("#myModal").modal("hide")
                            }
                        });
                    });
                },
            });
        })
    },
    sec_calender: function (cb) {

        Resources.calender.getList(function (res) {
            cb({
                runBefore: function () {

                },
                runAfter: function () {
                    $("#mycalendard").monthly({mode: 'event', dataType: 'json', events: {"monthly": res}});
                },
                getHtml: function () {
                    return Templates.sec_calender("<div class=\"monthly\" id=\"mycalendard\"></div>")
                },
            })
        })
    },
    sec_events: function (cb) {
        cb({
            // runBefore: function () {
            //
            // },
            runAfter: function () {
                Events.emit("loadData", "tab_my_events")
            },
            getHtml: function () {
                return Templates.sec_events({})
            },
        })
    },
    tab_upcoming_events: function (cb) {
        Resources.events.getUpcomingEvents(function (myEventObj) {
            let myEvemtHtml = "";

            for (let i in myEventObj)
                myEvemtHtml += Templates.unDeleteabelEvent(myEventObj[i]);

            cb(
                {

                    getHtml: function () {
                        return myEvemtHtml;
                    }
                }
            );

        });
    },
    tab_active_events: function (cb) {
        Resources.events.getActiveEvent(function (myEventObj) {
            let myEvemtHtml = "";

            for (let i in myEventObj)
                myEvemtHtml += Templates.unDeleteabelEvent(myEventObj[i]);

            cb(
                {
                    getHtml: function () {
                        return myEvemtHtml;
                    }
                }
            );

        });
    },
    tab_my_events: function (cb) {
        Resources.events.getMyEvent(function (myEventObj) {
            let myEvemtHtml = "";

            for (let i in myEventObj)
                myEvemtHtml += Templates.deleteabelEvent(myEventObj[i]);

            cb(
                {
                    runAfter: () => Events.emit("updateEventsCount"),
                    getHtml: () => myEvemtHtml
                }
            );

        });

    },
    sec_reservations: (cb) => cb({
        runAfter: () => {
            $("#moreReservation").click();
        },
        getHtml: () => Templates.reservationSeaction()
    }),

    sec_notification: (cb) => cb({
        runAfter: () => {
            history.pushState(null, '', '/account/#notification');
            $("#moreNotifiction").click();
        },
        getHtml: () => Templates.notification()
    }),
    sec_payments: (cb) => cb({
        runAfter: () => {
            history.pushState(null, '', '/account/#payments');
            $("#moreTransactions").click();
        },
        getHtml: () => Templates.payments()
    }),

}