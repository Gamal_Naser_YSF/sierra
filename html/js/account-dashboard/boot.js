dashboardLoad = function (setttings) {

    localStorage.clear();
    let reservationDB = ReservationData.getInstanse();
    let userNotification = UserNotifications.getInstanse();
    let userTransactions = UserTransactions.getInstanse();

    let AppendReservationData = function () {
        let ht = "";
        reservationDB.getData().forEach((it) => ht += Templates.reservationItem(it))
        $(".reservations-list").html(ht);
    };

    let appendNotificationdata = function () {
        let ht = "";
        userNotification.getData().forEach((it) => ht += Templates.notificationSingleItem(it))
        $(".notifications-list").html(ht);
    };

    let appendTransactions = function () {
        let ht = "";
        userTransactions.getData().forEach((it) => ht += Templates.transactionItem(it));
        $(".payment-history").html(ht);
    };

    // AppendReservationData();

    $("body")
        .on('click', '.tab_trigger', function (e) {
            e.preventDefault();
            let tabName = $(this).data('target');
            $(this).parent().addClass('active').siblings().removeClass('active');
            $(tabName).fadeIn().addClass('active').siblings().hide().removeClass('active');
            // lod section based on
            Events.emit("loadData", tabName.replace("#", ""))
        })
        .on('click', ".delete_item", function (e) {
            Events.emit("deleteItem", {
                "id": $(this).data("item-id"),
                done: (err) => {
                    $(this).parent('.single-event').remove();
                }
            })
        })
        .on('click', '.form_edit', function (e) {
            e.preventDefault();
            $(this).removeClass('form_edit').addClass('form_save').html('save');
            $($(this).data('form')).find('input').each(function () {
                $(this).attr('class', 'form-control').removeAttr('readonly')
            })
        })
        .on('click', '.form_save', function (e) {
            $(this).removeClass('form_save').addClass('form_edit').html('<i class="icon-edit3"></i>');
            e.preventDefault();
            $($(this).data('form')).find('input').each(function () {
                $(this).attr('class', 'form-control-plaintext').attr('readonly', 'true')
            })

            Events.emit("sendDataToServer")

        })
        .on("click", ".show-res-invoice", function () {
            $(this).closest(".single-res").find(".invoice-section").slideToggle()
        })
        .on("click", "#moreReservation", () => reservationDB.fetch().then(AppendReservationData))
        .on("click", "#moreNotifiction", () => userNotification.fetch().then(appendNotificationdata))
        .on("click", "#moreTransactions", () => userTransactions.fetch().then(appendTransactions))


// load first section user info

    $(".tab_trigger[data-target='#sec_" + (() => {
        let defaultSection = "profile";
        if (location.hash != "")
            defaultSection = location.hash.replace("#", "")
        return defaultSection;
    })() + "']").click();
    Events.emit("loadData", "sec_profile")

};
