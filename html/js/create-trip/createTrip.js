class NewRow {
    constructor() {
        this.formName = "rows";
        this.number = 0;
        this.countries = new countries();
    }

    makeSeelctRow(countries, type) {
        let options = "";
        for (let k in countries) {
            options += `<option value="${k}">${countries[k]}</option>`;
        }


        return `<select id="input_${type}" class="form-control" required name="${this.formName}[${this.number}][${type}]">${options}</select>`
    }

    createAriLins() {
        return `<div class="col-md">
                    <div class="form-group">
                        <select name="${this.formName}[${this.number}][airlines]" title="Hotel Rate" class="form-control custom-select" required>
                            <option value="0">Airline Rate (any)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>`;
    }

    createArraivalAndDepartal() {
        return `<div class="col-md">
                    <div class="check-date form-group">
                        <div class="input-group date datepicker">
                            <input name="${this.formName}[${this.number}][departure]" type="text" class="form-control " placeholder="Departure" readonly required>
                                <span class="input-group-addon icon-calendar"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="check-date form-group">
                        <div class="input-group date datepicker">
                            <input name="${this.formName}[${this.number}][arrival]" type="text" class="form-control " placeholder="Arrival" readonly required>
                                <span class="input-group-addon icon-calendar"></span>
                        </div>
                    </div>
                </div>`;
    }

    createHotel() {
        return `<div class="col-md">
                    <div class="form-group">
                        <select name="${this.formName}[${this.number}][hotel_rate]" title="Hotel Rate" class="form-control custom-select" required>
                            <option value="0">Hotel Rate (any)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>`;
    }

    createGuest() {
        return `<div class="col-md">
                    <div class="form-group">
                        <select name="${this.formName}[${this.number}][guest_number]" title="guest" class="form-control custom-select" required>
                            <option value="0">Guest  (none)</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                    </div>
                </div>`;
    }

    createCheckInOut() {
        return `<div class="col-md">
                    <div class="check-date form-group">
                        <div class="input-group date datepicker">
                            <input name="${this.formName}[${this.number}][check_in]" name="" type="text" class="form-control " placeholder="Check In"  readonly required>
                                <span class="input-group-addon icon-calendar"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="check-date form-group">
                        <div class="input-group date datepicker">
                            <input name="${this.formName}[${this.number}][check_out]" type="text" class="form-control " placeholder="Check Out" readonly required>
                                <span class="input-group-addon icon-calendar"></span>
                        </div>
                    </div>
                </div>`
    }

    createFromAndTo(countries) {
        return `<div class="col-md">
                    <div class="form-group">   
                        ${ this.makeSeelctRow(countries, "from")}
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-group">
                        ${ this.makeSeelctRow(countries, "to")}
                    </div>
                </div>`;
    }

    async getHtml() {

        this.number++;
        return await this.countries.get()
            .then((countries) => {

                return (`<div class="single-group">
                                <div class="row no-gutters group-one">
                                    <div class="col-xl-10"></div>
                                    <div class="col-xl-2 text-right">
                                        <button class="btn btn-danger btn-sm deleteSection">X</button>
                                    </div>
                                </div>
                                <div class="row no-gutters group-one">
                                    ${this.createFromAndTo(countries)}
                                </div>
                                <div class="row no-gutters group-tow">
                                    ${this.createCheckInOut()}
                                    ${this.createGuest()}
                                    ${this.createHotel()}
                                    ${this.createArraivalAndDepartal()}
                                    ${this.createAriLins()}
                                </div>
                            </div>`);
            });
    }
}

class countries {
    constructor() {
        this.data = null;
    }

    async featch() {

    }

    async get() {
        if (this.data == null)
            return await new Promise((res) => {
                fetch("countries")
                    .then(res => res.json())
                    .then(data => res(this.data = data.countries));
            });
        return await new Promise((res) => res(this.data));

    }


}


$(document).ready(function () {
    let newRwoCreator = new NewRow();
//
    newRwoCreator
        .getHtml()
        .then(html => $('#groupsWrapper').append(html))
        .then(() => {
            $('.datepicker').datepicker({autoclose: true, orientation: "bottom"});
            $('#tripForm')
                .on('click', '.duplicate_item', function () {
                    newRwoCreator
                        .getHtml()
                        .then(html => $('#groupsWrapper').append(html))
                        .then(() => $('.datepicker').datepicker({autoclose: true, orientation: "bottom"}))
                })
                .on("click", ".deleteSection", function () {
                    $(this).closest(".single-group").remove();
                })
        });


});