let bookEvent = (eventId) => {
        return {
            getPromis: () => {
                swal("Good job!", "Thanks for booking your event accommodation with SGS our customer experience team will contact you for your hotel reservation.", "success");
                return new Promise((resolve, reject) => {
                    $.post(baseUrl() + "/book-event", {_token: _token(), event_id: eventId},
                        (res) => resolve({status: res.status, server: res})
                    );
                });

            }
        }
    }
;