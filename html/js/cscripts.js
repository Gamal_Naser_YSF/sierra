// set default img for user img
// $(".user-img").defaultImg("cdn/dashboard/user-def.png");
function sirraAlert($message) {
    let cont = $(".float-pup")
    cont.html(`<div class="alert alert-dark">${$message}</div>`);
    cont.fadeIn();

    setTimeout(function () {
        cont.fadeOut();
    }, 3000)

}

isLogin = function (cb) {
    $.post(baseUrl() + "/is-login", {_token: _token()}, function (res) {
        if (!res.status) {
            $(".reg_show ").click()
        } else {
            cb(res);
        }
    });
}

setTimeout(function () {
    $(".float-pup").fadeOut()
}, 5000)
/********************************************** login section*/


$(".setRateHolder").rateItem({
    active: true,
    click: function (res) {
        let rateCount = $(res.item).index() + 1;
        let id = $(res.parentNode).data("id");
        isLogin(function (isLogin) {
            if (isLogin)
                R.events.setNewRate(id, rateCount, function (respond) {
                    if (respond.status) {
                        sirraAlert(respond.message);
                        res.updateRate(respond.newRate);
                    } else {
                        sirraAlert(respond.message);
                    }
                });
        })
    }
});


$("#loginTrigger").click(function () {
    let btn = $(this)
    btn.prop("disabled", true)
    let data = {
        _token: _token(),
        email: $("#login_email").val(),
        password: $("#login_password").val()
    }
    let request = $.ajax({url: baseUrl() + "/account/auth/login", type: "post", data: data})
    ;
    request.fail(function (ajax) {

    })
    request.done(function (res) {
        if (!res.status) {
            sirraAlert(res.message)
        } else {
            window.location.reload()
        }
    })
    request.always(function () {
        btn.prop("disabled", false)
    });
});
$("#registerTrigger").click(function () {
    let btn = $(this);
    btn.prop("disabled", true)
    let data = {
        _token: _token(),
        name: $("#reg_name").val(),
        email: $("#reg_email").val(),
        phone: $("#reg_phone").val(),
        password: $("#reg_password").val(),
        password_confirmation: $("#reg_password_confirmation").val()
    }
    if ($("#reg_accept").is(":checked"))
        data.accept = true;
    // Fire off the request to /form.php
    let request = $.ajax({url: baseUrl() + "/account/auth/register", type: "post", data: data});
    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR) {
        if (response.status) {
            sirraAlert("successfully registered please login")
            $('.loginReg_show').click();
        }

        $(".msg").html("")
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown) {
        let allErrors = jqXHR.responseJSON.errors;

        $(".msg").html("")

        for (let input in allErrors) {
            let inputErrors = allErrors[input];
            for (let err in inputErrors)
                $("#reg_" + input)
                    .closest(".form-group")
                    .find(".msg")
                    .append(`<div class="label label-danger label-sm">${inputErrors[err]}</div>`)
        }
//                alert(jqXHR.responseJSON.message)
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        btn.prop("disabled", false)
    });

});

// Rate Click
$('.user_rate .rate-item').click(function (e) {
    e.preventDefault();
    Events.emit("updateRate", $(this))
});


$("#seachTrigger").click(function () {
    $(".searchForm").toggleClass("openDiv")
});
$(".menu-btn").click(function () {
    $(".mnWraaper").slideToggle()
});


// -*-----------------------------------------------------*-

let loginForm = function () {
    let popub = $('.reg-popup');
    let body = $('.body-overlay');

    return {
        show: () => {
            popub.slideDown();
            $("#login_form_container").slideDown().siblings().hide();
            body.fadeIn();
        },
        hide: () => {
            popub.slideUp();
            $("#login_form_container").slideUp().siblings().hide();
            body.fadeOut();
        },
    }
};


let checkLogin = () => {
    return new Promise((resolve, reject) => {
        $.post(baseUrl() + "/is-login", {_token: _token()}, function (res) {
            if (!res.status) {
                loginForm().show();
                reject("user not login")
            } else {
                resolve(res);
            }
        });

    });
}


