$.fn.defaultImg = function ($imgSrc) {
    function checkImg(img) {
        if (img.naturalHeight <= 1 && img.naturalWidth <= 1)
            img.src = $imgSrc;
    }

    $(this).each(function () {

        if (this.complete) {

            checkImg(this);
        }
        else {
            try {
                $(this).load(function () {
                    checkImg(this);
                }).error(() => {
                    this.src = $imgSrc;
                });
            }
            catch (err) {
                checkImg(this);
            }
        }
    });
}


$.fn.rateItem = function (_opt) {
    let vars = {};
    vars.oneStarClass = "rate-item";
    vars.emptyClass = vars.oneStarClass + " icon-star-empty";
    vars.fullClass = vars.oneStarClass + " icon-star-full";


    let opt = $.extend({
        active: false, click: function (ele) {

        }
    }, _opt);

    let Helper = {
        updateRate: function (ele) {
            let parentNode = $(ele);
            return function (rate) {
                parentNode.attr("data-rate", rate);
                // Helper.setRate(ele, rate);
                Helper.makeItNotInteractive(ele);
                //  Helper.makeItInteractive(ele);
            }
        },
        addStars: function (ele, length) {
            let star = $(`<span class="${vars.emptyClass}"></span>`)
                .css({cursor: "pointer"}).click(function (e) {
                    opt.click({item: this, parentNode: ele, event: e, updateRate: Helper.updateRate(ele)});
                });

            for (let i = 0; i < length; i++)
                $(ele).append(star.clone(true))

            return $(ele);
        },
        setRate: function (ele, rate) {
            // console.log(ele, rate)
            let rateCount = rate - 1, rates_items = $(ele).find('.' + vars.oneStarClass);
            for (let i = 0; i <= rateCount; i++) {
                $(rates_items[i]).attr('class', vars.fullClass)
            }
        },
        makeItNotInteractive: function (ele) {
            window.reload();
        },
        makeItInteractive: function (ele) {

            $(ele).find('.' + vars.oneStarClass).hover(function () {
                let rateCount = $(this).index(), rates_items = $(this).parent().find('.' + vars.oneStarClass);
                rates_items.attr('class', vars.emptyClass);
                for (var i = 0; i <= rateCount; i++) {
                    $(rates_items[i]).attr('class', vars.fullClass)
                }
            }, function () {
                Helper.setRate(ele, $(ele).data("rate"))
            });
        }
    }
    $(this).each(function () {
        Helper.nod
        let rate = $(this).data("rate");

        Helper.addStars(this, 5)
        Helper.setRate(this, rate);
        if (opt.active)
            Helper.makeItInteractive(this);


    })
}

