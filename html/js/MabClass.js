class MabClass {
    static instance(options) {
        return new MabClass(options)
            .setAllInputs({
                lng: document.getElementById("event_location_long"),
                lat: document.getElementById("event_location_lat"),
                address: document.getElementById("address")
            })
            .drawMap()
            .addMarker("loc", options.position)
            .onClick((e, self) => {
                self.addMarker("loc", e.latLng.toJSON()).updatInputs(e.latLng);
            })
            .makeTextInputAsSearchBox((self) => {
                let pos = self.map.center;
                self.addMarker("loc", pos.toJSON()).updatInputs(pos);
            })
    }

    constructor(options) {
        this.markers = {};
        this.inputs = {lat: null, lng: null, address: null};
        for (let item in options) {
            this[item] = options[item];
        }
    }

    drawMap() {
        let ele = document.getElementById(this.target);
        ele.style.width = "100&";
        ele.style.height = "250px";
        this.map = new google.maps.Map(ele, {
            zoom: (this.zoomLevel) ? this.zoomLevel : 8,
            center: this.position
        });
        return this;
    }

    addMarker(id, position, cb) {
        let markerId = "mark" + id;
        if (this.markers[markerId])
            this.markers[markerId].setPosition(position)
        else
            this.markers[markerId] = new google.maps.Marker({position: position, map: this.map});

        if (cb) cb(this.markers[markerId]);
        return this;
    }

    onClick(cb) {
        this.map.addListener('click', (e) => {
            cb(e, this);
        });
        return this;
    }

    setLatInput(input) {
        this.inputs.lat = input;
        this.inputs.lat.readOnly = true;
        return this
    }

    setLngInput(input) {
        this.inputs.lng = input;
        this.inputs.lng.readOnly = true;
        return this
    }

    setAddressInput(input) {
        this.inputs.address = input;
        this.inputs.address.readOnly = true;
        return this
    }

    setAllInputs(inputs) {
        this.setLatInput(inputs.lat)
        this.setLngInput(inputs.lng)
        this.setAddressInput(inputs.address)
        return this
    }

    updatInputs(position) {
        new google.maps.Geocoder().geocode({'latLng': position}, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    this.inputs.lat.value = position.lat()
                    this.inputs.lng.value = position.lng()
                    this.inputs.address.value = results[0].formatted_address
                }
            }
        });
    }

    makeTextInputAsSearchBox(callBack) {
        let input = this.inputs.address;
        input.readOnly = false;
        let searchBox = new google.maps.places.SearchBox(input);
        searchBox.addListener('places_changed', () => {
            let places = searchBox.getPlaces();

            if (places.length == 0) return;

            // For each place, get the icon, name and location.
            let bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            this.map.fitBounds(bounds);
            if (callBack)
                callBack(this);
        });
        return this;
    }
}