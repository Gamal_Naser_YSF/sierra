<?php

use Symfony\Component\HttpFoundation\StreamedResponse;

Route::get("exp", function () {


    $response = new StreamedResponse(function () {
        // Open output stream
        $handle = fopen('php://output', 'w');

        $tables = DB::select('SHOW TABLES');
        foreach ($tables as $table) {
            $tbNm = $table->{"Tables_in_" . env("DB_DATABASE")};
            DB::table($tbNm)->chunk(1, function ($users) use ($handle) {
                foreach ($users as $user) {
                    fputs($handle, (string)$user);
                }
            });
        }

        // Close the output stream
        fclose($handle);
        rewind($handle);
    }, 200, [
        'Content-type' => 'application/json',
        'Content-Disposition' => 'attachment; filename="ex.json"',
    ]);

    return $response;

});

require_once "routes/front.php";
require_once "routes/account.php";
require_once "routes/admin.php";

Route::get("terms", function () {
    return "terms under constructions ";
});


Route::get("policy", function () {
    return "policy under constructions ";
});



//DB::listen(function ($d) {
//    dump($d->sql);
//});
//
//
