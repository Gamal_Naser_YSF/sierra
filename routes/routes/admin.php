<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 01:09 م
 */


Route::group(["prefix" => "/admin", "namespace" => "Admin"], function () {

    Route::group(["middleware" => \App\Http\Middleware\MustLogin::class . ":_AdminObj,adminLogin"], function () {

        Route::resource("reservations", "Reservations\Reservations");
        Route::get("emails/resend", "Emails\Emails@resend")->name("emails.resend");
        Route::resource("emails", "Emails\Emails");

        Route::post("reservations/{id}/change-status", "Reservations\Reservations@changeStatus")->name("changeTransactionStatus");

        Route::get('notifications', 'Notifications\Notifications@getIndex');
        Route::get('notifications/seen/{id}', 'Notifications\Notifications@seen');

        Route::resource("configs", "Configs\Configs");
        Route::get('configs/delete/{id}', 'Configs\Configs@delete');

        Route::resource("partners", "Partners\Partners");
        Route::post("partners/edit/", "Partners\Partners@update");
        Route::get("partners/delete/{id}", "Partners\Partners@delete");

        Route::get("/", "Dashboard\Dashboard@getIndex")->name("dashboard");
        Route::get("/auth/logout", "Auth\Auth@getLogout");

        Route::resource('/sliders', 'Sliders\Sliders');
        Route::group(["prefix" => "/sliders", "namespace" => "Sliders"], function () {
            Route::get('/delete/{id}', 'Sliders@delete');
            Route::post('/reorder', 'Sliders@reorder');
        });

        Route::group(["prefix" => "/bookings", "namespace" => "Bookings"], function () {
            Route::get("/events-booking", "Bookings@eventBookings")->name("bookingsIndex");
            Route::get("/packages-booking", "Bookings@packagesBookings")->name("packagesBookings");
            Route::get("/seen/{id}", "Bookings@seen");
            Route::get("/{id}/change-status/", "Bookings@changeStatus")->name("booking.changeStatus");
            Route::get("/package-seen/{id}", "Bookings@packageSeen");
        });
        Route::group(["prefix" => "/admins", "namespace" => "Admins"], function () {

            Route::post("/edit", "Admins@update");
            Route::get("/edit/{id}", "Admins@edit");

            Route::post("/create", "Admins@store");
            Route::get("/create", "Admins@create");


            Route::get("/roles", "Admins@getRoles");
            Route::post("/roles", "Admins@postRoles");

            Route::get("/active/{id}", "Admins@active");
            Route::get("/unactivated/{id}", "Admins@unactivated");

            Route::get("/change-user-password/{id}", "Admins@getChangeUserPassword");
            Route::post("/change-user-password/{id}", "Admins@postChangeUserPassword");

            Route::get("/", "Admins@getIndex")->name("adminsIndex");
        });

        Route::group(["prefix" => "/packages", "namespace" => "Packages"], function () {
            Route::get("/", "Packages@getIndex")->name("packagesIndex");
            Route::get("/create", "Packages@create");
            Route::get("/custom-packages", "Packages@customPackages")->name("customPackages");
            Route::get("/s/{status}/{id}", "Packages@makeCustomPackageDone");
            Route::post("/create", "Packages@save");
            Route::get("/edit/{id}", "Packages@edit");
            Route::post("/edit", "Packages@update");
            Route::get("/delete/{id}", "Packages@delete");

        });
        Route::group(["prefix" => "/trips", "namespace" => "Trips"], function () {
            Route::get("/", "Trips@customTrips")->name("UsersTripsIndex");
            Route::get("/trip-details/{id}", "Trips@tripDetails")->name("tripDetails");
            Route::get("/trip-done/{id}", "Trips@makeTripDone");
        });

        Route::group(["prefix" => "/plans", "namespace" => "Plans"], function () {
            Route::get("/", "Plans@index")->name("PlansIndex");
            Route::get("/delete/{id}", "Plans@delete");
            Route::get("/create", "Plans@create");
            Route::post("/create", "Plans@store");
            Route::get("/edit/{id}", "Plans@edit");
            Route::post("/edit", "Plans@update");
//            Route::post("/create", "Plans@store");

        });

        Route::group(["prefix" => "/roles", "namespace" => "Roles"], function () {

            Route::get("/create", "Roles@create");
            Route::post("/create", "Roles@store");

            Route::get("/delete/{id}", "Roles@getDelete");

            Route::post("/edit", "Roles@update");
            Route::get("/edit/{id}", "Roles@edit");

            Route::get("/", "Roles@getIndex")->name("groupsIndex");
        });

        Route::group(["prefix" => "/categories", "namespace" => "Categories"], function () {

            Route::get("/create", "Categories@create");
            Route::post("/create", "Categories@store");

            Route::get("/delete/{id}", "Categories@delete");

            Route::get("/users/{id}", "Categories@relatedUsers");

            Route::get("/edit/{id}", "Categories@edit");
            Route::post("/edit", "Categories@update");

            Route::post("/change-status", "Categories@changeStatus");

            Route::get("/", "Categories@getIndex")->name("categoriesIndex");
        });

        Route::group(["prefix" => "/events", "namespace" => "Events"], function () {
            Route::get("/", "Events@getIndex")->name("eventsIndex");
            Route::get("/create", "Events@create");
            Route::post("/create", "Events@store");
            Route::post("/edit", "Events@update");
            Route::get("/edit/{id}", "Events@edit");
            Route::get("/delete/{id}", "Events@delete");
            Route::post("/media/", "Events@media");
        });
        Route::group(["prefix" => "/media", "namespace" => "Media"], function () {
            Route::get("/delete/{id}", "Media@delete");
        });

        Route::group(["prefix" => "/users", "namespace" => "Users"], function () {
            Route::get("/", "Users@getIndex")->name("usersIndex");
            Route::get("/interest/{id}", "Users@getInterest");
            Route::post("/interest/{id}", "Users@postInterest");
            Route::get("/create", "Users@create");
            Route::post("/create", "Users@store");
            Route::get("/edit/{id}", "Users@edit");
            Route::post("/fetch/{id}", "Users@fetch");
        });


    });

    Route::group(["prefix" => "/auth", "namespace" => "Auth", "middleware" => \App\Http\Middleware\UserMustBeGuest::class . ":_AdminObj,dashboard"], function () {
        Route::get("/login", "Auth@getLogin")->name("adminLogin");
        Route::post("/login", "Auth@postLogin");
    });
});
