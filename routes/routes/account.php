<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 01:09 م
 */

Route::group(["prefix" => "/account", "namespace" => "Account"], function () {
    Route::group(["middleware" => [\App\Http\Middleware\setSharedViews::class, \App\Http\Middleware\MustLogin::class . ":_Account,accountLogin"]], function () {
        Route::get("/", "Dashboard\Dashboard@getIndex")->name("accountDashboard");
        Route::get("/auth/logout", "Auth\Auth@getLogout");
        Route::post("/profile/get", "Users\Users@getInfo");
        Route::post("/profile/update", "Users\Users@update");
        Route::post("/profile/upload-image", "Users\Users@updateImage");
        Route::group(["prefix" => "/events", "namespace" => "Events"], function () {
            Route::any("/get-events", "Events@getMyEvent");
            Route::any("/un-subscribe", "Events@unSubscribe");
        });

        Route::group(["prefix" => "/reservations", "namespace" => "Reservations"], function () {
            Route::any("", "Reservations@myReservation");
        });
        Route::group(["prefix" => "/notifications", "namespace" => "Notifications"], function () {
            Route::any("", "Notifications@myNotifications");
        });
        Route::group(["prefix" => "/transactions", "namespace" => "Transactions"], function () {
            Route::any("", "Transactions@myTransactions");
        });
    });

    Route::group(["prefix" => "/auth", "namespace" => "Auth",
        "middleware" => \App\Http\Middleware\UserMustBeGuest::class . ":_Account,accountDashboard"], function () {
        Route::get("/login", "Auth@getLogin")->name("accountLogin");
        Route::post("/login", "Auth@postLogin");
        Route::post("/register", "Auth@postRegister");
        Route::get("/forget-password", "Auth@getForgotPassword");
        Route::post("/forget-password", "Auth@postForgotPassword");
        Route::get("/reset-password/{token}", "Auth@getResetPassword");
        Route::post("/reset-password/{token}", "Auth@postResetPassword");
//        ----------------------------
        Route::get("{provider}/redirect", "FB@redirect")->name("fbLogin");
        Route::get('{provider}/callback', 'FB@callback');
    });

});
