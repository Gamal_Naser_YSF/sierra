<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 09/12/17
 * Time: 01:09 م
 */
Route::group(["middleware" => [\App\Http\Middleware\setSharedViews::class], "prefix" => "/", "namespace" => "Front"], function () {
    \Jlib\Auth\Auther::setKey("_Account");
    Route::get("/", "Home@getIndex");
    Route::get("/payment/init", "Home@generalPay")->name("pages.payment");
    Route::get("/search", "Home@search")->name("front.search");
    Route::get("/plans", "Home@plans")->name("pages.plans");
    Route::get("/pay-plan/{id}", "Home@payPlan");
    Route::get("/travel-package", "Home@travelPackage")->name("front.packages");
    Route::get("/package/{id}", "Home@onePackage")->name("f_package_details");
    Route::post("/create-custom-package", "Home@createCustomPackage");
    Route::post("/rate-package", "Home@ratePackage");
    Route::get("/create-trip", "Home@createTrip")->name("front.trip");
    Route::post("/create-trip", "Home@saveCreateTrip");
    Route::post("/is-login", "Home@isLogin");
    Route::post("/book-event", "Home@bookEvent");
    Route::post("/book-package", "Home@bookPackage");
    Route::get("/event-details/{id}", "Home@eventDetails")->name("f_event_details");
    Route::get("/payment/{status}", "Home@payment");
    Route::post("/rate-event", "Home@rateEvent");
    Route::any("/countries", "Home@getCountries");
    Route::any("/about-us", "Home@about")->name("pages.about");
    Route::any("/select-interesting", "Home@interesting")->name("pages.interesting");
    Route::post("/save-interest", "Home@saveInterest")->name("pages.saveInterest");
    Route::get("/search/{cat}", function ($cat) {
        return redirect()->route("front.search", ["categories" => $cat]);
    })->name("front.search.alias");
});