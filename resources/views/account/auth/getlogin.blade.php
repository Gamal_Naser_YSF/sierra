@extends("admin.loginLayout")

@section("content")

    <form class="form-signin" role="form" method="post">

        {!! csrf_field() !!}

        <div class="append-icon">
            <input type="text" name="email" id="name" class="form-control form-white username" placeholder="email"
                   required>
            <i class="icon-user"></i>
        </div>

        <div class="append-icon m-b-20">
            <input type="password" name="password" class="form-control form-white password" placeholder="Password"
                   required>
            <i class="icon-lock"></i>
        </div>

        <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button"
                data-style="expand-left">Sign In
        </button>

        {{--<div class="clearfix">--}}
            {{--<p class="pull-left m-t-20">--}}
                {{--<a href="{{$scope}}/auth/forget-password">Forgot password?</a></p>--}}

        {{--</div>--}}
    </form>


@stop
