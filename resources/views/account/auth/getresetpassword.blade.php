@extends("admin.loginLayout")

@section("content")

    <form  class="form-signin" role="form" method="post">

        {!! csrf_field() !!}

        <div class="append-icon">
            <input type="password" name="password" class="form-control form-white" placeholder="password" required>
        </div>
        <div class="append-icon">
            <input type="password" name="password_confirmation" class="form-control form-white" placeholder="password" required>
        </div>

        <br/>

        <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button"
                data-style="expand-left">set new password
        </button>

        {{--<div class="clearfix">--}}
        {{--<p class="pull-left m-t-20">--}}
        {{--<a href="{{$scope}}/auth/forget-password">Forgot password?</a></p>--}}

        {{--</div>--}}
    </form>


@stop
