@extends("admin.loginLayout")

@section("content")

    <form class="form-signin" role="form" method="post">

        {!! csrf_field() !!}

        <div class="append-icon">
            <input type="email" name="email" id="email" class="form-control form-white username" placeholder="email"
                   required>
            <i class="icon-user"></i>
        </div>
<br/>

        <button type="submit" id="submit-form" class="btn btn-lg btn-danger btn-block ladda-button"
                data-style="expand-left">reset password
        </button>

        {{--<div class="clearfix">--}}
        {{--<p class="pull-left m-t-20">--}}
        {{--<a href="{{$scope}}/auth/forget-password">Forgot password?</a></p>--}}

        {{--</div>--}}
    </form>


@stop
