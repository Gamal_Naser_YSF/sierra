@extends("front.layout")

@section("css")
    <style>
        .btn-black {
            background: #000;
        }

        #res-form {
            border: thin solid #adadad;
            overflow: hidden;
            height: 42px;
            border-radius: 7px;
            box-shadow: 2px 2px 6px #00000042;
        }

        #res-form input[type=text] {
            outline: none;
            border: none;
            margin: 0;
            float: left;
            height: 100%;
            padding-left: 5px;
            width: 80%;
            color: #969696;
        }

        #res-form button {
            outline: none;
            border: none;
            margin: 0;
            height: 100%;
            float: left;
            width: 20%;
            background: #000;
            color: #fff;
        }

        .res-serach {
            background: #f5f5f5;
            padding: 5px;
        }

        .res-item {
            cursor: pointer;
            padding: 11px;
            height: 42px;
            border-bottom: thin solid #e2e2e2;
        }

        .res-item:hover {
            background: #f5f5f5;
        }

        .res-list-wappre {
            position: relative;
            height: 305px;
            overflow-y: scroll;
        }

        .res-lis {

            border: thin solid #e2e2e2;
        }
        .gray{
            background: #e2e2e2;
            padding: 10px;
            height: 400px;
            overflow-y: scroll;
        }
    </style>
@stop
@section("content")




    <div class="profile-wrapper">
        <div class="container">
            <div class="profile-section">
                <div class="row align-items-stretch no-gutters">
                    <div class="col-1">
                        <!--main tab links (black sec)-->
                        <div class="controller-wrapper">
                            <div class="single-tap active">
                                <a href="#" class="tab_trigger" data-target="#sec_profile">
                                    <img class="img-fluid user-img" src="cdn/dashboard/user.png" title="profile">
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="single-tap">
                                <a href="#" class="tab_trigger" data-target="#sec_calender">
                                    <img class="img-fluid" src="cdn/dashboard/calender.png" title="calender">
                                    <span class="item-title">calender</span>
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="single-tap">
                                <a href="#" class="tab_trigger" data-target="#sec_events">
                                    <img class="img-fluid" src="cdn/dashboard/events.png" title="events">
                                    <span class="item-title">events</span>
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="single-tap">
                                <a href="#" class="tab_trigger" data-target="#sec_reservations">
                                    <img class="img-fluid" src="cdn/dashboard/res.png" title="Reservations">
                                    <span class="item-title">Reservations</span>
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="single-tap">
                                <a href="#" class="tab_trigger" data-target="#sec_trip">
                                    <img class="img-fluid" src="cdn/dashboard/mytrips.png" title="My Trips">
                                    <span class="item-title">My Trips</span>
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="single-tap">
                                <a href="#" class="tab_trigger" data-target="#sec_payments">
                                    <img class="img-fluid" src="cdn/dashboard/payment.png" title="payments History">
                                    <span class="item-title">payments History</span>
                                </a>
                                <img src="cdn/dashboard/points.png" class="img-fluid">
                            </div>
                            <div class="logout-btn">
                                <a href="{{url("/account/auth/logout")}}" data-target="profile_section">
                                    <img class="img-fluid" src="cdn/dashboard/logout.png" title="logout">
                                    <span class="item-title">logout</span>
                                </a>
                            </div>
                        </div>
                        <!--end main tab links (black sec)-->
                    </div>
                    <div class="col-11">
                        <div class="items-wrapper">
                            <!-- header filter , messages , notifications-->
                            <div class="head-filter">
                                <div class="row align-items-center">
                                    <div class="col-md-7">
                                        <form class="addon-form">
                                            <div class="input-group">
                                                <input type="email" class="form-control smallDesc"
                                                       placeholder="Search Events, Calendat, Conferences, Upcoming events, etc..">
                                                <button class="btn btn-arrow" type="button">
                                                    <i class="icon-find"></i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-5 text-md-right">
                                        <div class="header-items">
                                            <div class="row">
                                                {{--<div class="col">--}}
                                                    {{--<a href="#" class="tab_trigger item-cont"--}}
                                                       {{--data-target="#sec_messages">--}}
                                                        {{--Message--}}
                                                        {{--<i class="icon-envelope-o">--}}
                                                            {{--<span class="counter">5</span>--}}
                                                        {{--</i>--}}
                                                    {{--</a>--}}
                                                {{--</div>--}}
                                                <div class="col">
                                                    <a href="#" class="tab_trigger item-cont"
                                                       data-target="#sec_notification">
                                                        Notification
                                                        <i class="icon-notifications_none">
                                                            <span class="counter">{{@$userNotification->count}}</span>
                                                        </i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end header filter , messages , notifications-->
                            <div class="viewer-control">
                                <div class="row no-gutters">
                                    <div class="col-md-9">
                                        <!--main content body-->
                                        <div id="main_body" class="main-body">
                                            <!-- profile tab-->
                                            <div id="sec_profile" class="tab-item active">

                                            </div>
                                            <!-- calender tab-->
                                            <div id="sec_calender" class="tab-item">

                                            </div>
                                            <!-- events tab-->
                                            <div id="sec_events" class="tab-item">

                                            </div>
                                            <!-- events tab-->
                                            <div id="sec_reservations" class="tab-item">

                                            </div>
                                            <!-- events tab-->
                                            <div id="sec_trip" class="tab-item">
                                                <div class="tab-wrapper events-section">
                                                    <div class="secondary-tabs">
                                                        <div class="tab-content">
                                                            trips
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- events tab-->
                                            <div id="sec_payments" class="tab-item" style="padding: 15px">

                                            </div>
                                            <!-- notification tab-->
                                            <div id="sec_notification" class="tab-item">
                                                <div class="tab-wrapper notification-section">
                                                    <div class="secondary-tabs">
                                                        <div class="tab-content">
                                                            notification
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- messages tab-->
                                            {{--<div id="sec_messages" class="tab-item">--}}
                                                {{--<div class="tab-wrapper messages-section">--}}
                                                    {{--<div class="secondary-tabs">--}}
                                                        {{--<div class="tab-content">--}}
                                                            {{--messages--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                        <!--end main content body-->
                                    </div>
                                    <div class="col-md-3">
                                        <div class="widgets-container">
                                            <div class="logo-wrapper">
                                                <img class="img-fluid" src="cdn/dashboard/logo.png">
                                            </div>
                                            <div class="widget-body"></div>
                                            {{--<div class="widget-add fx-bottom">--}}
                                                {{--<a href="#">--}}
                                                    {{--<img class="img-fluid w-100" src="cdn/dashboard/add.png">--}}
                                                {{--</a>--}}
                                            {{--</div>--}}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true" style="z-index: 100000000">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Upload Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <p class="alert alert-danger " style="display: none" id="uploaderError"></p>
                            <input type="file" class="form-control" name="avatar">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button id="uploadUserImage" type="button" class="btn btn-primary">Upload</button>
                </div>
            </div>
        </div>
    </div>
@stop




@section("js")

    <script src="js/account-dashboard/boot.js"></script>
    <script src="js/account-dashboard/EventsMethods.js"></script>
    <script src="js/account-dashboard/Templates.js"></script>
    <script src="js/account-dashboard/sectionsLoader.js"></script>
    <script src="js/account-dashboard/Resources.js"></script>
    <script src="js/account-dashboard/reservation.js"></script>
    <script src="js/account-dashboard/UserNotifications.js"></script>
    <script src="js/account-dashboard/UserTransactions.js"></script>
    <script>
        $(document).ready(function () {

            getToken = function () {
                return "{{csrf_token()}}";
            }
            getUrl = function () {
                return "{{url("/account")}}";
            }
            dashboardLoad({})
        })
    </script>

@stop
