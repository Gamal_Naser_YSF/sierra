<div class="form-group " {{(@$hidden)?"hidden":""}}>
    <div class="col-sm-12">
        <div class="col-sm-9">
            {!! (@$required)?"<span style='color:red;'> * </span>":"" !!}
            <label class=" control-label ">  {{_t("admin.$name")}}</label>
            {!! $input !!}
        </div>
        @if(@$image)
            <div class="col-sm-3">
                {!! $image !!}
            </div>
        @endif
    </div>
</div>
<hr {{(@$hidden)?"hidden":""}}/>
