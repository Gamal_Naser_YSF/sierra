@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">

                        </div>
                    </div>

                </div>


                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.subject")}}</th>
                            <th>{{_t("admin.user email")}}</th>
                            <th>{{_t("admin.user name")}}</th>

                            <th>{{_t("admin.status")}}</th>
                            <th>{{_t("admin.resend")}}</th>
                            <th>{{_t("admin.details")}}</th>


                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        /**
                         * @var \App\Models\EmailLog $row
                         */
                        ?>
                        @foreach( $rows  as $row)
                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->getSubject()}}</td>
                                <td>{{@$row->getUserEmail()}}</td>
                                <td>{{@$row->getUserName()}}</td>
                                <td><span class="label label-primary">{{@$row->getStatusName()}}</span></td>
                                <td><a href="{{route("emails.resend",["email"=>$row->id])}}">resend</a></td>
                                <td><a href="{{route("emails.show",["email"=>$row->id])}}">show</a></td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop



