<div class="sidebar">
    <div class="logopanel">
        <h1>
            <a href="dashboard.html"></a>
        </h1>
    </div>
    <div class="sidebar-inner">
        <ul class="nav nav-sidebar">
            <li class="nav-active active">
                <a href="{{url()->route("dashboard")}}">
                    <i class="icon-home"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-parent">
                <a href="">
                    <i class="icon-user"></i>
                    <span>{{_t("admin.Adminstration")}} </span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("adminsIndex")}}">{{_t("admin.admins")}}</a></li>
                    <li><a href="{{url()->route("groupsIndex")}}">{{_t("admin.groups")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="icon-layers"></i><span>{{_t("admin.categories")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("categoriesIndex")}}">{{_t("admin.all categories")}}</a></li>

                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="icon-user"></i><span>{{_t("admin.User")}} </span><span class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("usersIndex")}}">{{_t("admin.all users")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.events")}}</span><span class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("eventsIndex")}}">{{_t("admin.all events")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.bookings")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("bookingsIndex")}}">{{_t("admin.Event Bookings")}}</a></li>
                    <li><a href="{{url()->route("packagesBookings")}}">{{_t("admin.Travel Packages Bookings")}}</a></li>
                    <li><a href="{{url()->route("customPackages")}}">{{_t("admin.custom travel packages request")}}</a>
                    </li>
                    <li><a href="{{url()->route("UsersTripsIndex")}}">{{_t("admin.all users trips")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.travel packages")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("packagesIndex")}}">{{_t("admin.travel packages")}}</a></li>
                    <li><a href="{{url()->route("customPackages")}}">{{_t("admin.custom travel packages")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.users trips")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("UsersTripsIndex")}}">{{_t("admin.all users trips")}}</a></li>
                </ul>
            </li>

            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.plans")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("PlansIndex")}}">{{_t("admin.plans")}}</a></li>
                </ul>
            </li>

            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.partners")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("partners.index")}}">{{_t("admin.partners")}}</a></li>
                </ul>
            </li>

            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.configs")}}</span><span class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("configs.index")}}">{{_t("admin.configs")}}</a></li>
                </ul>
            </li>

            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.reservations")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("reservations.index")}}">{{_t("admin.reservations")}}</a></li>
                    <li><a href="{{url()->route("reservations.create")}}">{{_t("admin.create reservations")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.emails")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("emails.index")}}">{{_t("admin.emails")}}</a></li>
                </ul>
            </li>
            <li class="nav-parent">
                <a href=""><i class="fa fa-table"></i><span>{{_t("admin.sliders")}}</span><span
                            class="fa arrow"></span></a>
                <ul class="children collapse">
                    <li><a href="{{url()->route("sliders.index")}}">{{_t("admin.sliders")}}</a></li>
                </ul>
            </li>
            {{------------------------------------------------------------------}}
            {{------------------------------------------------------------------}}
            {{------------------------------------------------------------------}}


            {{--<li class="nav-parent">--}}
            {{--<a href="#"><i class="icon-puzzle"></i><span>Builder</span> <span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a target="_blank" href="../../admin-builder/index.html"> Admin</a></li>--}}
            {{--<li><a href="page-builder/index.html"> Page</a></li>--}}
            {{--<li><a href="ecommerce-pricing-table.html"> Pricing Table</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<a href="#"><i class="icon-bulb"></i><span>Mailbox</span> <span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="mailbox.html"> Inbox</a></li>--}}
            {{--<li><a href="mailbox-send.html"> Send Email</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-screen-desktop"></i><span>UI Elements</span> <span--}}
            {{--class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="ui-buttons.html"> Buttons</a></li>--}}
            {{--<li><a href="ui-components.html"> Components</a></li>--}}
            {{--<li><a href="ui-tabs.html"> Tabs</a></li>--}}
            {{--<li><a href="ui-animations.html"> Animations CSS3</a></li>--}}
            {{--<li><a href="ui-icons.html"> Icons</a></li>--}}
            {{--<li><a href="ui-portlets.html"> Portlets</a></li>--}}
            {{--<li><a href="ui-nestable-list.html"> Nestable List</a></li>--}}
            {{--<li><a href="ui-tree-view.html"> Tree View</a></li>--}}
            {{--<li><a href="ui-modals.html"> Modals</a></li>--}}
            {{--<li><a href="ui-notifications.html"> Notifications</a></li>--}}
            {{--<li><a href="ui-typography.html"> Typography</a></li>--}}
            {{--<li><a href="ui-helper.html"> Helper Classes</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-note"></i><span>Forms </span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="forms.html"> Forms Elements</a></li>--}}
            {{--<li><a href="forms-validation.html"> Forms Validation</a></li>--}}
            {{--<li><a href="forms-plugins.html"> Advanced Plugins</a></li>--}}
            {{--<li><a href="forms-wizard.html"> <span class="pull-right badge badge-danger">low</span> <span>Form Wizard</span></a>--}}
            {{--</li>--}}
            {{--<li><a href="forms-sliders.html"> Sliders</a></li>--}}
            {{--<li><a href="forms-editors.html"> Text Editors</a></li>--}}
            {{--<li><a href="forms-input-masks.html"> Input Masks</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{----}}
            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-bar-chart"></i><span>Charts </span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="charts.html"> Charts</a></li>--}}
            {{--<li><a href="charts-finance.html"> Financial Charts</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-picture"></i><span>Medias</span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="medias-image-croping.html"> Images Croping</a></li>--}}
            {{--<li><a href="medias-gallery-sortable.html"> Gallery Sortable</a></li>--}}
            {{--<li><a href="medias-hover-effects.html"> <span class="pull-right badge badge-primary">12</span>--}}
            {{--Hover Effects</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-docs"></i><span>Pages </span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="page-timeline.html"> Timeline</a></li>--}}
            {{--<li><a href="page-404.html"> Error 404</a></li>--}}
            {{--<li><a href="page-500.html"> Error 500</a></li>--}}
            {{--<li><a href="page-blank.html"> Blank Page</a></li>--}}
            {{--<li><a href="page-contact.html"> Contact</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}

            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-basket"></i><span>eCommerce </span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="ecommerce-cart.html"> Shopping Cart</a></li>--}}
            {{--<li><a href="ecommerce-invoice.html"> Invoice</a></li>--}}
            {{--<li><a href="ecommerce-pricing-table.html"><span class="pull-right badge badge-success">5</span>--}}
            {{--Pricing Table</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
            {{--<li class="nav-parent">--}}
            {{--<a href=""><i class="icon-cup"></i><span>Extra </span><span class="fa arrow"></span></a>--}}
            {{--<ul class="children collapse">--}}
            {{--<li><a href="extra-fullcalendar.html"><span class="pull-right badge badge-primary">New</span>--}}
            {{--Fullcalendar</a></li>--}}
            {{--<li><a href="extra-widgets.html"> Widgets</a></li>--}}
            {{--<li><a href="page-coming-soon.html"> Coming Soon</a></li>--}}
            {{--<li><a href="extra-sliders.html"> Sliders</a></li>--}}
            {{--<li><a href="maps-google.html"> Google Maps</a></li>--}}
            {{--<li><a href="maps-vector.html"> Vector Maps</a></li>--}}
            {{--<li><a href="extra-translation.html"> Translation</a></li>--}}
            {{--</ul>--}}
            {{--</li>--}}
        </ul>
        <div class="sidebar-footer clearfix">
            <a class="pull-left footer-settings" href="#" data-rel="tooltip" data-placement="top"
               data-original-title="Settings">
                <i class="icon-settings"></i></a>
            <a class="pull-left toggle_fullscreen" href="#" data-rel="tooltip" data-placement="top"
               data-original-title="Fullscreen">
                <i class="icon-size-fullscreen"></i></a>
            <a class="pull-left" href="#" data-rel="tooltip" data-placement="top" data-original-title="Lockscreen">
                <i class="icon-lock"></i></a>
            <a class="pull-left btn-effect" href="#" data-modal="modal-1" data-rel="tooltip" data-placement="top"
               data-original-title="Logout">
                <i class="icon-power"></i></a>
        </div>
    </div>
</div>