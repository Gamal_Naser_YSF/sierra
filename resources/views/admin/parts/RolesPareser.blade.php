{{--{{dd($permissions)}}--}}
@if(!empty($permissions))
    @foreach($permissions as $key=> $per)
        <div class="col-lg-12">
            <h2>{{$key}}</h2>
        </div>
        <hr/>
        @foreach($per as  $val)


            <div class="col-sm-3">
                <label style="cursor: pointer">
                    {{Form::checkbox("per[$val->module][$val->action]",$val->id,null,["data-checkbox"=>"icheckbox_square-blue"])}}
                    {{$val->scope." | ".$val->module ."-". $val->action}}
                </label>
            </div>


        @endforeach
    @endforeach
@endif
<div class="clearfix"></div>
<hr/>

<div class="clearfix"></div>
@if(@$oldPermissions)
@section("js")
    @parent
    <script>

        function makePermissionsChecked(list) {
            for (var module in list) {
                for (var action in list[module]) {
console.log('[name="per[' + module + '][' + list[module][action] + ']"]');
                    $('[name="per[' + module + '][' + list[module][action] + ']"]').prop("checked", true).iCheck('update');
                }
            }
        }

        (function (list) {
console.log(list);
            makePermissionsChecked(list)
        })(<?=$oldPermissions?>)
    </script>
@stop
@endif