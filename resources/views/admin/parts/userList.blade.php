<div>
    {!! $userList->links() !!}
</div>

@if(!$userList->isEmpty())
    <table class="table table-hover table-dynamic">
        <thead>
        <tr>
            <th>#</th>
            <th>avatar</th>
            <th>name</th>
            <th>email</th>
            <th>phone</th>
            <th>phone</th>

            @if($module == "users")
                <th>{{_t("admin.commands")}}</th>
            @endif


        </tr>
        </thead>
        <tbody>

        @foreach( $userList  as $row)
            <tr>

                <td>{{@$row->id}}</td>
                <td>{!! _ren($row->avatar) !!} </td>
                <td>{{@$row->name}}</td>
                <td>{{@$row->email}}</td>
                <td>{{@$row->phone}}</td>

                @if($module == "users")
                    <td>

                        <a href="{{"$scope/users/edit/".$row->encyrptId()}}"
                           class="btn btn-primary">{{_t("admin.edit")}}</a>

                        <a href="{{"$scope/users/interest/".$row->encyrptId()}}"
                           class="btn btn-primary">{{_t("admin.add interest")}}</a>

                        {{--<a href="{{"$scope/users/delete/".$row->encyrptId()}}"--}}
                           {{--class="btn btn-danger">{{_t("admin.delete")}}</a>--}}
                    </td>
                @endif

            </tr>
        @endforeach
        </tbody>
    </table>
@else
    <div class="col-lg-12">
        <strong class="alert alert-warning col-lg-12">{{_t("admin.no data to show")}}</strong>
    </div>
@endif
<div>
    {!! $userList->links() !!}
</div>