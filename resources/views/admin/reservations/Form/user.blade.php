<div class="col-lg-12 ">
    <div class="panel">
        <div class="panel-content">
            <div class="row">
                <div class="col-lg-6">
                    <label>User ID
                        <small>enter ID then hit <code>ENTER</code> or click <code>fetch data</code></small>
                    </label>
                    {!! Form::number("uid",request()->uid,["class"=>"form-control","id"=>"uidHolder"]) !!}
                    <hr/>
                    <button id="_fetchUserData" type="button" class="btn btn-success">fetch data</button>
                    <button  id="_changeUID" type="button" class="btn btn-default">change</button>
                </div>
                <div class="col-lg-6" id="infoSec" style="display: none">
                    <label>User Details </label>
                    <p><strong>Name : </strong><span id="_username">sdf sd</span></p>
                    <p><strong>email : </strong><span id="_email">sdf sd</span></p>
                    <p><strong>phone : </strong><span id="_phone">sdf sd</span></p>
                    <p><strong>address : </strong><span id="_address">sdf sd</span></p>
                </div>

            </div>
        </div>

    </div>
</div>