<div class="col-lg-12 moreDetails" style="display: none">
    <div class="panel">
        <div class="panel-content">
            <div class="row">

                <div class="col-lg-8">
                    <label>Fees</label>
                    {!! Form::text("fees",request()->fees,["class"=>"form-control"]) !!}
                </div>
                <div class="col-lg-4">
                    <label>Type</label>
                    {!! Form::select("type",$types,request()->type,["class"=>"form-control","style"=>"width:100%"]) !!}
                </div>
                <div class="col-lg-8">
                    <label>Title</label>
                    {!! Form::text("title",request()->title,["class"=>"form-control"]) !!}
                </div>
                <div class="col-lg-4">
                    <label>Image</label>
                    {!! Form::file("image",["class"=>"form-control","style"=>"width:100%"]) !!}
                </div>

            </div>
        </div>

    </div>
</div>