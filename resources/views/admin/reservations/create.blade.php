@extends("admin.layout")

@section('Title', "Create New Reservation")

@section('content')
    <div class="row">
      <form id="transForm" action="{{route("reservations.store")}}" method="post">
          {!! csrf_field() !!}
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header">
                    <h2> Transaction Details</h2>
                </div>

            </div>
        </div>


          @includeIf("admin.reservations.Form.btn")
          @includeIf("admin.reservations.Form.user")
          @includeIf("admin.reservations.Form.type")
          @includeIf("admin.reservations.Form.more")
          @includeIf("admin.reservations.Form.btn")
      </form>

    </div>
@stop
@section("js")
<script src="dashboard/Jlib/Loader.js"></script>
<script src="dashboard/Jlib/Reservations/AddDetails.js"></script>
<script src="dashboard/Jlib/Reservations/Boot.js"></script>
@stop
