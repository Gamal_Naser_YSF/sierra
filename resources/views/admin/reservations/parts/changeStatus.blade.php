<?php
/**
 * @var \App\Models\Transaction $transaction
 */

?>
<form method="post" action="{{route("changeTransactionStatus",["id"=>$reservation->id])}}">
    {!! csrf_field() !!}
    {!! Form::select("new_status",$statusesList,$reservation->status,["style"=>"min-width:150px; width:100%"]) !!}
    <button class="btn btn-success btn-sm btn-block">change status</button>
</form>
