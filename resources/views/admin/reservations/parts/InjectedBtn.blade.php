<?php
//dd($model->package);
$show = true;
/**
 * @var \App\Models\Event @$model->event
 */
switch ($type) {
    case "event":
        $userId = @$model->user->id;
        $text = "Event [" . @$model->event->event_name . "][$model->id] will be on [" . @$model->event->start_date .
            "]  in [" . @$model->event->address . "] and to see more details please see this <a href='" . route("f_event_details", ["id" => @$model->event->encyrptId()]) . "'>link</a>";
        break;

    case "travel":
        $userId = @$model->user->id;
        $text = "package [" . @$model->package->name . "][$model->id] and to see more details please see this <a href='" . route("f_package_details", ["id" => @$model->package->encyrptId()]) . "'>link</a>";
        break;
    case "custom_travel":
        $userId = @$model->user->id;
        $text = "custom travel id :  $model->id";
        break;
    case "trip":
        $userId = @$model->user->id;
        $text = "trip id :  $model->id";
        break;
    default:
        $show = false;
        break;

}


?>

@if($show)
    <a class="btn" style="background: black;color: #fff !important;"
       href="{{route("reservations.create",["uid"=>$userId,"type"=>$type,"text"=>$text])}}">Make Reservation</a>
@endif