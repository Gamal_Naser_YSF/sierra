@extends("admin.layout")

@section('Title', "Reservation #$row->id")

@section('content')
    <div class="row">
        <div class="col-lg-8 ">
            <div class="panel">
                <div class="panel-header">
                    <h2> Details</h2>
                </div>
                <div class="panel-content pagination2 table-responsive">

                    <table class="table table-responsive table-hover">

                        <tbody>
                        <?php
                        /**
                         * @var \App\Models\Transaction $row
                         */
                        ?>
                        <tr>
                            <td>ID</td>
                            <td>{{$row->id}}</td>
                        </tr>
                        <tr>
                            <td>created at</td>
                            <td>{{$row->getDate()}}</td>
                        </tr>

                        <tr>
                            <td>Fees</td>
                            <td>{{$row->getFees()}}</td>
                        </tr>
                        <tr>
                            <td>User</td>
                            <td><a href="{{route("usersIndex",["i"=>@$row->user->id])}}">{{@$row->user->getName()}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>
                                <span class="label lb-{{$row->getType()}}">{{$row->getType()}}</span>
                            </td>
                        </tr>
                        <tr>
                            <td>Current Status</td>
                            <td>
                                <span class="label label-{{$row->getStatusLevel()}}">{{$row->getStatusText()}}</span>
                            </td>
                        </tr>
                        @foreach($row->getDetails() as $k=> $detail)
                            <tr>
                                <td>{{$k}}</td>
                                <td>
                       {!! $detail !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="col-lg-4 ">
            <div class="panel">
                <div class="panel-header">
                    <h2> Operations</h2>
                </div>
                <div class="panel-content pagination2 table-responsive">
                    <p>Change status</p>
                    @includeIf("admin.reservations.parts.changeStatus",["reservation"=>$row,"statusesList"=>$statuses])

                </div>
            </div>
        </div>
    </div>
@stop
