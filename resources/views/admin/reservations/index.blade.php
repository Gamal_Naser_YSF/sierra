@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"/$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.$module")}}</a>
                    <hr/>
                </div>



                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-responsive table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>created at</th>
                            <th>Fees</th>
                            <th>for user</th>
                            <th>Type</th>
                            <th>current Status</th>
                            <th>commands</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        /**
                         * @var \App\Models\Reservation $reservation
                         */
                        ?>
                        @foreach( $rows as $reservation)
                            <tr>
                                <td>{{$reservation->id}}</td>
                                <td>{{$reservation->getDate()}}</td>
                                <td>{{$reservation->getFees()}}</td>
                                <td>
                                    <a href="{{route("usersIndex",["i"=>@$reservation->user->id])}}">{{@$reservation->user->name}}</a>
                                </td>
                                <td>
                                    <span class="label label-default lb-{{$reservation->getType()}}">{{$reservation->getType()}}</span>
                                </td>
                                <td>
                                    <span class="label label-{{$reservation->getStatusLevel()}}">{{$reservation->getStatusText()}}</span>
                                </td>
                                <td width="2">
                                    <a class="btn btn-primary" href="{{route("reservations.show",["reservation"=>$reservation->id])}}">show details</a>
                                    @includeIf("admin.reservations.parts.changeStatus",["reservation"=>$reservation,"statusesList"=>$statuses])
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
