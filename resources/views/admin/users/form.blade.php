

<?php $input = 'name'; ?>
@include("parts.inputForm",["name"=>$input,"input"=>  Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

<?php $input = 'email'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::email($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

<?php $input = 'phone'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::number($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

@if(request()->is("$scope/$module/create"))
    <?php $input = 'password'; ?>
    @include("parts.inputForm",["name"=>$input,"input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])
@endif

<?php $input = 'avatar'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::file($input,['class'=>'form-control '])  ])

