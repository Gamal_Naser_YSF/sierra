@extends("admin.layout")

@section('Title',_t("admin.edit")." ". _t("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{_t("admin.edit")." "._t("admin.$module")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">
            {!! Form::model($row,['url' => $scope.'/'.$module.'/edit', 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}

            {!! Form::hidden("id",$row->id) !!}

            @include($scope.'.'.$module.'.form')

            <hr/>
            {!! Form::submit(_t('admin.Save') ,['class' => 'btn btn-success']) !!}

            {!! Form::close() !!}
        </div>
    </div>

@stop

