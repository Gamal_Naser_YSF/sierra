@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>

                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <strong>{{_t("admin.user interest")}} </strong>
                                <hr/>
                                <p><strong>username</strong> {{@$row->username}}</p>
                            </h3>
                        </div>
                    </div>

                </div>


                <div class="panel-content ">
                    <div class="row">
                        {!! Form::open(["method"=>"post"]) !!}
                        <div class="col-sm-12">
                            <input type="submit" value="save" class="btn btn-success">
                            <hr/>
                        </div>
                        @foreach($categories as $id => $cat)
                            <div class="col-sm-2">
                                {!! Form::checkbox("interested[]",$id,(in_array($id,$interested))?true:false) !!}
                                : {{$cat}}
                            </div>
                        @endforeach
                        <div class="col-sm-12">
                            <hr/>
                            <input type="submit" value="save" class="btn btn-success">
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
