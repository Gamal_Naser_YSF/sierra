<?php
/**
 * @var \App\Models\Booking $row
 */

?>
@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('css')
    <style>
        #myModal {
            z-index: 1000000;
        }

        .popup_cont {
            z-index: 850;
        }
    </style>
@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>
                    <div>
                        <form>
                            <a class="btn btn-primary btn-inline"
                               href="{{request()->fullUrlWithQuery(["s"=>""])}}">All</a>
                            {!! Form::select("s",$statuses,request()->s,["onchange"=>"this.form.submit()","style"=>"width:200px"]) !!}
                        </form>
                    </div>

                </div>


                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.customer  name")}}</th>
                            <th>{{_t("admin.customer  email")}}</th>
                            <th>{{_t("admin.customer  phone")}}</th>
                            <th>{{_t("admin.customer  address")}}</th>

                            <th>{{_t("admin.event")}}</th>
                            <th>{{_t("admin.currentStatus")}}</th>
                            <th>{{_t("admin.change status")}}</th>
                            <th>{{_t("admin.make reservation")}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)

                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->user->name}}</td>
                                <td>{{@$row->user->email}}</td>
                                <td>{{@$row->user->phone}}</td>
                                <td>{{@$row->user->address}}</td>

                                <td>
                                    {{$row->event->event_name}}
                                    <a class="showEvent" href="javascript:void(0)"> [show] </a>

                                    <div class="eventDetails hidden">
                                        @include("admin/events/shared/singleEvent",["event"=>$row->event])
                                    </div>
                                </td>
                                <td>
                                    @include("admin/bookings/sheared/eventStatus",["status"=>$row->seen])
                                </td>
                                <td>
                                    <form action="{{route("booking.changeStatus",["id"=>$row->id])}}">
                                        {!! Form::select("status",$statuses,$row->seen,["onchange"=>"this.form.submit()","style"=>"width:200px"]) !!}
                                    </form>
                                </td>
                                <td>
                                    @includeIf("admin.reservations.parts.InjectedBtn",["model"=>@$row,"type"=>"event"])
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- Modal -->
    <div id="myModal" class="modal fade " role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@stop

@section("js")
    <script>
        $(".showEvent").click(function () {
            $("#myModal").find(".modal-body").html($(this).closest("td").find(".eventDetails").html());
            $("#myModal").modal("show")
        })
    </script>
@stop
