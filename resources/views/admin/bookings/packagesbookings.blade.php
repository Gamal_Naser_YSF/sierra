@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>


                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.booked at")}}</th>
                            <th>{{_t("admin.user")}}</th>
                            <th>{{_t("admin.package")}}</th>
                            <th>{{_t("admin.make reservation")}}</th>
                            <th>{{_t("admin.commands")}}</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)
                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->created_at}}</td>
                                <td>{{@$row->user->name}}</td>
                                <td>
                                    <a href="{{route("packagesIndex",["i"=>@$row->package->id])}}">{{@$row->package->name}}</a>
                                </td>
                                <td>   @includeIf("admin.reservations.parts.InjectedBtn",["model"=>@$row,"type"=>"travel"])</td>
                                <td>
                                    @if(!$row->isSeen())
                                        <a class="btn btn-success btn-sm btn-block"
                                           href="{{"$scope/$module/package-seen/".$row->encyrptId()}}">
                                            {{_t("admin.make it seen")}}
                                        </a>
                                    @endif
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop

