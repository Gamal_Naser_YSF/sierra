<!DOCTYPE html>
<html lang="en">
<head>
    <base href="{{url(env("APP_BASE_URL"))}}">
    <meta content="{{csrf_token()}}" name="csrf_token">
    <meta content="{{url("/")}}" name="url">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="description">
    <meta name="author" content="yousif aziz">
    <link rel="shortcut icon" href="dashboard/global/images/favicon.png" type="image/png">
    <title>{{_t("admin.dashboard")}} | {{env("APP_NAME")}}</title>
    <link href="dashboard/global/css/style.css" rel="stylesheet">
    <link href="dashboard/global/css/theme.css" rel="stylesheet">
    <link href="dashboard/global/css/ui.css" rel="stylesheet">
    <link href="dashboard/en/layout4/css/layout.css" rel="stylesheet">
    <!-- BEGIN PAGE STYLE -->
    <link href="dashboard/global/plugins/metrojs/metrojs.min.css" rel="stylesheet">
    {{--<link href="dashboard/global/plugins/maps-amcharts/ammap/ammap.css" rel="stylesheet">--}}
    <link href="dashboard/style.css" rel="stylesheet">
    <link href="pupbup/popup.css" rel="stylesheet">
    <link href="lib/sweetalert2/dist/sweetalert2.css" rel="stylesheet">
    <!-- END PAGE STYLE -->
    {{--<script src="dashboard/global/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>--}}
    <style>
        .lb-event {
            background-color: #319db5;
        }

        .lb-payment {
            background-color: #3db585;
        }

        .lb-travel {
            background-color: #a5b549;
        }

        .lb-trip {
            background-color: #b58383;
        }

        .lb-all {
            background-color: #70b592;
        }

        .lb-other {
            background-color: #b58989;
        }

        .lb-filter {
            background-color: #9871b5;
        }

        .btn {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }
    </style>
    @yield("css")
</head>
<body class=" fixed-topbar fixed-sidebar theme-sdtl color-default dashboard">
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<section>
    <!-- BEGIN SIDEBAR -->
@include("admin.parts.topMenu")
<!-- END SIDEBAR -->
    <div class="main-content">
        <!-- BEGIN TOPBAR -->
        `@include("admin.parts.TopBar")
    <!-- END TOPBAR -->
        <!-- BEGIN PAGE CONTENT -->
        <div class="page-content page-thin">
            @include("shared.flash")

            <h2 style="margin-bottom: 10px;text-transform: capitalize"> - @yield("Title")</h2>
            @yield("content")
            <div class="footer">
                <div class="copyright">
                    <p class="pull-left sm-pull-reset">
                        <span>Copyright <span class="copyright">©</span> 2016 </span>
                        <span>THEMES LAB</span>.
                        <span>All rights reserved. </span>
                    </p>
                    <p class="pull-right sm-pull-reset">
                        <span><a href="#" class="m-r-10">Support</a> |
                            <a href="#" class="m-l-10 m-r-10">Terms of use</a> |
                            <a href="#" class="m-l-10">Privacy Policy</a>
                        </span>
                    </p>
                </div>
            </div>
        </div>
        <!-- END PAGE CONTENT -->
    </div>
    <!-- END MAIN CONTENT -->
</section>


<!-- BEGIN PRELOADER -->
<div class="loader-overlay">
    <div class="spinner">
        <div class="bounce1"></div>
        <div class="bounce2"></div>
        <div class="bounce3"></div>
    </div>
</div>
<!-- END PRELOADER -->
<a href="#" class="scrollup"><i class="fa fa-angle-up"></i></a>
<script src="dashboard/global/plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="dashboard/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="dashboard/global/plugins/jquery-ui/jquery-ui.min.js"></script>
<script src="dashboard/global/plugins/gsap/main-gsap.min.js"></script>
<script src="dashboard/global/plugins/tether/js/tether.min.js"></script>
<script src="dashboard/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="dashboard/global/plugins/appear/jquery.appear.js"></script>
{{--<script src="dashboard/global/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->--}}
{{--<script src="dashboard/global/plugins/jquery-block-ui/jquery.blockUI.min.js"></script>--}}
<!-- simulate synchronous behavior when using AJAX -->
<script src="dashboard/global/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
{{--<script src="dashboard/global/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>--}}
<!-- Custom Scrollbar sidebar -->
{{--<script src="dashboard/global/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>--}}
<!-- Show Dropdown on Mouseover -->
{{--<script src="dashboard/global/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->--}}
<script src="dashboard/global/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
<script src="dashboard/global/plugins/select2/dist/js/select2.full.min.js"></script> <!-- Select Inputs -->
<script src="dashboard/global/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
{{--<script src="dashboard/global/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->--}}
{{--<script src="dashboard/global/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>--}}
<!-- Animated Progress Bar -->
<script src="dashboard/global/js/builder.js"></script> <!-- Theme Builder -->
<script src="dashboard/global/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
<script src="dashboard/global/js/application.js"></script> <!-- Main Application Script -->
<script src="dashboard/global/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
<script src="dashboard/global/js/widgets/notes.js"></script> <!-- Notes Widget -->
<script src="dashboard/global/js/quickview.js"></script> <!-- Chat Script -->
<script src="dashboard/global/js/pages/search.js"></script> <!-- Search Script -->
<script src="dashboard/en/layout4/js/layout.js"></script>
<script src="dashboard/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="https://maps.googleapis.com/maps/api/js?key={{env("MAP_KEY")}}&libraries=places&"></script>
<script src="dashboard/Jlib/MapApp.js"></script>
<script src="lib/sweetalert2/dist/sweetalert2.js"></script>
<script src="//cdn.ckeditor.com/4.8.0/full/ckeditor.js"></script>
<script src="pupbup/jquery.popup.min.js"></script>
@include("parts.LaravelErrors")
<script>
    var url = "{{url("/")}}"
    var csrf_token = "{{csrf_token()}}"

    CKEDITOR.replaceClass = 'ckeditor';
    $(document).ready(function () {
        $("label.control-label").each(function () {
            $(this).text($(this).text().split("_").join(" "))
        })
        setTimeout(function () {
            $(".flashMessage").fadeOut();
        }, 3000);
        $("[data-type=deleteBtn]").click(function (e) {
            let self = $(this);
            e.preventDefault();
            // console.log(self.attr("href"));
            // return;
            swal({
                title: 'Are you sure? delete this record',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#c75757',
                cancelButtonColor: '#122b40',
                confirmButtonText: 'Yes, delete it!'
            }).then(function (result) {
                if (result.value) {
                    window.location.href = self.attr("href");
                }
            })
        });
        $('#flash-overlay-modal').modal();
        $(".control-label").each(function () {

            $(this).css({"text-transform": "capitalize"})


        });
    });
</script>
@yield("js")
</body>
</html>
