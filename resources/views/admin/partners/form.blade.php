<?php $input = 'name'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])


<?php $input = 'logo'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "input"=> Form::file($input,['class'=>'form-control ']),
    "image"=>_ren($row->{$input})
])
