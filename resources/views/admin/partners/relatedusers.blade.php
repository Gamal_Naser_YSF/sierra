@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>


                <div class="panel-content pagination2 table-responsive">
                    @include("admin.parts.userList",["userList"=>$rows])
                </div>

            </div>
        </div>
    </div>
@stop
