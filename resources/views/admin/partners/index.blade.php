@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                            {{--<i class="fa fa-table"> </i>--}}
                            {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                            {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.".\Illuminate\Support\Str::singular($module))}}</a>
                    <hr/>
                </div>
                <form>

                    <div class="panel-header filter-wrapper">

                    </div>

                    <div class="panel-content pagination2 table-responsive">
                        <div>
                            {!! $rows->links() !!}
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{_t("admin.logo")}}</th>
                                <th>{{_t("admin.name")}}</th>
                                <th>{{_t("admin.commands")}}</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach( $rows  as $row)
                                <tr>
                                    <td>{{@$row->id}}</td>
                                    <td>{!! _ren(@$row->logo,null,["style"=>"max-width:80px"]) !!}</td>
                                    <td>{{@$row->name}}</td>

                                    <td>
                                        <a style="width: 100px" class="btn btn-primary btn-sm btn-block"
                                           href="{{ route("partners.edit",["pardner"=>$row->encyrptId()]) }}">{{_t("admin.edit")}}</a>
                                        <a style="width: 100px" data-type="deleteBtn" class="btn btn-danger btn-sm btn-block"
                                           href="{{"$scope/$module/delete/".$row->encyrptId()}}">{{_t("admin.delete")}}</a>
                                    </td>


                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $rows->links() !!}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section("js")
    <script>
        $("[name=changeStatus]").on("ifChanged", function () {
            console.log()
            $.post("{{url("$scope/categories/change-status")}}", {
                _token: "{{csrf_token()}}",
                id: $(this).val()
            }, function (res) {
                console.log(res)
            })
        })
    </script>
@stop
