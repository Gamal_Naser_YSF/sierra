@extends("admin.layout")

@section('Title', 'Requests')

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"/$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.$module")}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    @if($rows->count())
                        <table class="table table-hover table-dynamic">

                            <thead>
                            <tr>
                                <th>#</th>

                                <th>{{_t("admin.name")}}</th>
                                <th>{{_t("admin.commends")}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach( $rows  as $row)
                                <tr>

                                    <td>{{@$row->id}}</td>

                                    <td>{{@$row->name}}</td>

                                    <td>
                                        <a href="{{"$scope/$module/edit/".$row->encyrptId()}}"
                                           class="btn btn-primary ">Edit</a>
                                        <a href="{{"$scope/$module/delete/".$row->encyrptId()}}"
                                           class="btn btn-danger ">Delete</a>

                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="alert alert-warning">
                            {{_t('admin.sorry there is no data for now.')}}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        function aproveRequest($id) {
            window.location.href = "<?= url("/{$scope}/{$module}/status?s=1&id=") ?>" + $id;
        }
    </script>
@stop