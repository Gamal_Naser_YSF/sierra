<!DOCTYPE html>
<html>
<head>
    <base href="{{url(env("APP_BASE_URL"))}}">
    <meta charset="utf-8">
    <title>Themes Lab - Creative Laborator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description"/>
    <meta content="themes-lab" name="author"/>
    <link rel="shortcut icon" href="dashboard/global/images/favicon.png">
    <link href="dashboard/global/css/style.css" rel="stylesheet">
    <link href="dashboard/global/css/ui.css" rel="stylesheet">
</head>
<body class="account separate-inputs" data-page="login">
<!-- BEGIN LOGIN BOX -->
<div class="container" id="login-block">
@include("shared.flash")
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
                <i class="user-img icons-faces-users-03"></i>
                @yield("content")
            </div>
        </div>
    </div>
    <p class="account-copyright">
        <span>Copyright © {{date("Y")}} </span><span>{{env("APP_NAME")}} </span>. <span>All rights reserved.</span>
    </p>


</div>
<script src="dashboard/global/plugins/jquery/jquery-3.1.0.min.js"></script>
<script src="dashboard/global/plugins/jquery/jquery-migrate-3.0.0.min.js"></script>
<script src="dashboard/global/plugins/gsap/main-gsap.min.js"></script>
<script src="dashboard/global/plugins/tether/js/tether.min.js"></script>
<script src="dashboard/global/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="dashboard/global/plugins/backstretch/backstretch.min.js"></script>
<script src="dashboard/global/js/pages/login-v1.js"></script>
<script>  setTimeout(function(){$(".flashMessage").fadeOut();},3000);</script>
<script src="js/LaravelError.js"></script>
<script>
    (function () {
        let errors = @json(@$errors->getMessages())     ;
        LaravelErrors(errors).showHints();
    })()
</script>
</body>
</html>