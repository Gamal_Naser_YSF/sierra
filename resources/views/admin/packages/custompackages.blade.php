@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>


                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.created by")}}</th>
                            <th>{{_t("admin.hotel")}}</th>
                            <th>{{_t("admin.airlines")}}</th>
                            <th>{{_t("admin.departure date")}}</th>
                            <th>{{_t("admin.adults")}}</th>
                            <th>{{_t("admin.children")}}</th>
                            <th>{{_t("admin.infant")}}</th>
                            <th>{{_t("admin.make reservation")}}</th>
                            <th>{{_t("admin.commands")}}</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)
                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->user->name}}</td>
                                <td>{{@$row->hotel->name}}</td>
                                <td>{{@$row->getAirlines->name}}</td>
                                <td>{{@$row->departure_date}}</td>
                                <td>{{@$row->adults}}</td>
                                <td>{{@$row->children}}</td>
                                <td>{{@$row->infant}}</td>

                                <td>   @includeIf("admin.reservations.parts.InjectedBtn",["model"=>@$row,"type"=>"custom_travel"])</td>
                                <td>
                                    @if(!$row->is_done)
                                        <a href="{{"$scope/$module/s/done/{$row->encyrptId()}"}}">
                                            Make It Done!
                                        </a>
                                    @else
                                        <a href="{{"$scope/$module/s/pending/{$row->encyrptId()}"}}">
                                            Make It pending!
                                        </a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop


