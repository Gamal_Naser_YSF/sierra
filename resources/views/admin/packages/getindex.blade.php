@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                            {{--<i class="fa fa-table"> </i>--}}
                            {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                            {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"/$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.".\Illuminate\Support\Str::singular($module))}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.image")}}</th>
                            <th>{{_t("admin.name")}}</th>
                            <th>{{_t("admin.destination")}}</th>
                            <th>{{_t("admin.duration in days")}}</th>
                            <th>{{_t("admin.hotel")}}</th>
                            <th>{{_t("admin.airlines")}}</th>
                            <th>{{_t("admin.description")}}</th>
                            <th>{{_t("admin.video")}}</th>
                            <th>{{_t("admin.rate")}}</th>
                            <th>{{_t("admin.tour type")}}</th>
                            <th>{{_t("admin.command")}}</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)
                            <tr>
                                <td><a target="_blank" href="{{"package/".$row->encyrptId()}}">view</a></td>
                                <td>{!! _ren(@$row->getFirstImageName(),null,["style"=>"max-width:80px"]) !!}</td>
                                <td>{{@$row->name}}</td>
                                <td>{{@config("countries.$row->destination")}}</td>
                                <td>{{@$row->duration_in_days}}</td>
                                <td>{{@$row->getHotel->name}}</td>
                                <td>{{@$row->getAirlines->name}}</td>
                                <td>{{@$row->description}}</td>
                                <td><a href="{{@$row->video}}" target="_blank">view <i class="fa fa-external-link"></i></a>
                                </td>
                                <td>
                                    <div data-rate="{{$row->rate}}" class="rate-wrapper rate_generate">

                                        <span class="rate-item fa fa-star-o"></span>
                                        <span class="rate-item fa fa-star-o"></span>
                                        <span class="rate-item fa fa-star-o"></span>
                                        <span class="rate-item fa fa-star-o"></span>
                                        <span class="rate-item fa fa-star-o"></span>

                                    </div>
                                </td>
                                <td>{{@$row->tour_type}}</td>
                                <td>
                                    <a class="btn btn-primary btn-sm btn-block"
                                       href="{{"$scope/$module/edit/".$row->encyrptId()}}">{{_t("admin.edit")}}</a>
                                    <a data-type="deleteBtn" class="btn btn-danger btn-sm btn-block"
                                       href="{{"$scope/$module/delete/".$row->encyrptId()}}">{{_t("admin.delete")}}</a>
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop




@section("js")
    <script>
        $('.rate_generate').each(function () {
            var rateCount = $(this).data('rate') - 1,
                rates_items = $(this).find('.rate-item');
            rates_items.css({color: "#ffa200"});
            for (var i = 0; i <= rateCount; i++) {
                $(rates_items[i]).attr('class', 'rate-item fa fa-star')
            }
        });
    </script>
@stop
