<?php $input = 'name'; ?>
@include("parts.inputForm",[
    "name"=>$input,
           "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])

<?php $input = 'destination'; ?>
@include("parts.inputForm",[
    "name"=>$input,

           "input"=> Form::select($input,config("countries"),@$row->$input,['class'=>'form-control '])
])

<?php $input = 'hotel'; ?>
@include("parts.inputForm",[
    "name"=>$input,

    "input"=> Form::select($input,$hotels,@$row->$input,['class'=>'form-control '])
])

<?php $input = 'airlines'; ?>
@include("parts.inputForm",[
    "name"=>$input,

    "input"=> Form::select($input,$airlines,@$row->$input,['class'=>'form-control '])
])



<?php $input = 'duration_in_days'; ?>
@include("parts.inputForm",[
    "name"=>$input,

    "input"=> Form::number($input,@$row->$input,['class'=>'form-control '])
])


<?php $input = 'tour_type'; ?>
@include("parts.inputForm",[
    "name"=>$input,

    "input"=> Form::text($input,@$row->$input,['class'=>'form-control '])
])

<?php $input = 'video'; ?>
@include("parts.inputForm",[
    "name"=>$input." -- Youtube url ",

    "input"=> Form::text($input,@$row->$input,['class'=>'form-control '])
])

<?php $input = 'image'; ?>
@include("parts.inputForm",[
    "name"=>$input,
           "required"=>true,
    "image"=>_ren($row->images()),
    "input"=> Form::file($input."[]",['class'=>'form-control ','multiple'=>true])."<div name='$input' ></div>"
])

<?php $input = 'description'; ?>
@include("parts.inputForm",[
    "name"=>$input,
        "required"=>true,
    "input"=> Form::textarea($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])
