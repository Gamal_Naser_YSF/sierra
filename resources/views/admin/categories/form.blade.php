<?php $input = 'name'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])

<?php $input = 'type'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "input"=> Form::select($input,$categoriesType,$row->{$input},['class'=>'form-control ']),
    "image"=>_ren($row->{$input})
])

<?php $input = 'show_in_home_page'; ?>

@include("parts.inputForm",[
    "name"=>"show in home page",
    "input"=> Form::checkbox($input,$row->{$input},$row->isShowedInHomePage(),['class'=>'form-control ']),

])

<?php $input = 'image'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "input"=> Form::file($input,['class'=>'form-control ']),
    "image"=>_ren($row->{$input})
])

<?php $input = 'description'; ?>

@include("parts.inputForm",[
    "name"=>$input,
     "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])

<?php $input = 'category_id'; ?>

@include("parts.inputForm",[
    "name"=>"parent category",
    "input"=> Form::select($input,$categories,@$row->$input,['class'=>'form-control '])
])