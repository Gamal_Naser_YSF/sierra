<div class="row">
    <div class="col-lg-4">
        <?php $input = 'event_name'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-4">
        <?php $input = 'start_date'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::date($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-4">
        <?php $input = 'end_date'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::date($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?php $input = 'organized_by'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-4">
        <?php $input = 'event_coordinator'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-4">
        <?php $input = 'event_serial'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
            "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <?php $input = 'profile_picture'; ?>
        @include("parts.inputForm",[
            "name"=>"Photo",
            "required"=>true,
            "input"=> Form::file($input."[]",['class'=>'form-control ','multiple'=>true])."<div name='profile_picture' ></div>"
        ])
    </div>

    <div class="col-lg-4">
        <?php $input = 'proposal_end_date'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::date($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-4">
        <?php $input = 'event_website'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control '])
        ])
    </div>
</div>

<div class="row">
    <div class="col-lg-6">

        <?php $input = 'category_id'; ?>
        @include("parts.inputForm",[
            "name"=>"category",
             "required"=>true,
            "input"=> Form::select($input."[]",$categoriesList,$row->relatedCategoryAsIds(),['multiple'=>true,'class'=>'form-control ',"required"=>true])
        ])

    </div>
    <div class="col-lg-6">
        <?php $input = 'tags'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control select-tags '])
        ])
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <?php $input = 'country'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::select($input,config("countries"),@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-6">
        <?php $input = 'city'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
        ])
    </div>
</div>
<div class="row">
    <div class="col-lg-5">
        <?php $input = 'address'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,["id"=>$input,'class'=>'form-control ',"required"=>true])
        ])
        <?php $input = 'event_location_lat'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,["id"=>$input,'class'=>'form-control ',"required"=>true])
        ])
        <?php $input = 'event_location_long'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::text($input,@$row->$input,["id"=>$input,'class'=>'form-control ',"required"=>true])
        ])
    </div>
    <div class="col-lg-7">
        <div id="eventMap"></div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <?php $input = 'details'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::textarea($input,@$row->$input,['class'=>'form-control ckeditor',"required"=>true])
        ])
    </div>
    <div class="col-lg-12">
        <?php $input = 'event_highlightes'; ?>
        @include("parts.inputForm",[
            "name"=>$input,
             "required"=>true,
            "input"=> Form::textarea($input,@$row->$input,['class'=>'form-control ckeditor',"required"=>true])
        ])
    </div>
</div>


@section("js")
    {{--@parent--}}
    <script src="js/MabClass.js"></script>
    <script>
        let currentLocation = {
            getLat: function () {
                return parseFloat("{{($row->event_location_lat)?$row->event_location_lat:30.0596185}}")
            },
            getLng: function () {
                return parseFloat("{{($row->event_location_long)?$row->event_location_long:31.1884234}}")
            },
        }
        MabClass.instance({
            target: "eventMap",
            position: {
                lat: currentLocation.getLat(),
                lng: currentLocation.getLng(),
            }
        })
    </script>
@stop