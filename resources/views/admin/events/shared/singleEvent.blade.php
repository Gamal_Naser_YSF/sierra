<div class="col-lg-12 events-single-wrapper ">
    <div class="col-lg-10">
        <div class="col-lg-12">
            <div class="col-lg-9">
                <h2>
                    {{$event->event_name}}
                    <label class=" label label-warning labels">{{$event->overall_rating}}
                        <i class="glyphicon glyphicon-star"></i></label>
                </h2>
                <label class="label label-default labels">
                    <strong>Start date : </strong>{{$event->startDate()}}
                </label>
                <label class="label label-default labels">
                    <strong>End date : </strong>{{$event->endDate()}}
                </label>
            </div>
            <div class="col-lg-3 text-right text-right" style="padding-top: 20px">
            </div>
            <div class="col-lg-12">
                <hr/>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="col-lg-3">
                <div class="address-wrapper">
                    <img class="img-responsive"
                         src="dashboard/global/images/flags/{{$event->country}}.png"/>
                    <h2>{{$event->country}}</h2>
                    <strong>{{$event->city}}</strong>
                    <hr/>
                    <p>
                        <strong>Address : </strong> {{@$event->address}}
                    </p>
                    <p>
                        <strong>Lat : </strong> {{@$event->event_location_lat}}
                    </p>
                    <p>
                        <strong>Lng : </strong> {{@$event->event_location_long}}
                    </p>
                    <a href="{{"https://www.google.com/maps/search/?api=1&query=$event->event_location_lat,$event->event_location_long"}}"
                       target="_blank">see in google maps</a>
                </div>
            </div>
            <div class="col-lg-9">

                <div class="col-lg-12">
                    <h3><strong>Tags</strong></h3>
                    @if(!$event->getTags()->isEmpty())
                        @foreach($event->getTags() as $tag)
                            <label class=" label label-info labels">{{$tag}}</label>
                        @endforeach
                    @else
                        <p class="alert alert-warning">sorry no tags to shown</p>
                    @endif
                </div>
                <div class="col-lg-12">
                    <h3><strong>Categories</strong></h3>
                    @if(!$event->categories->isEmpty())
                        @foreach($event->categories as $category)
                            <label class=" label label-success labels">{{$category->name}}</label>
                        @endforeach
                    @else
                        <p class="alert alert-warning">sorry no categories to shown</p>
                    @endif
                </div>
                <div class="col-lg-4">
                    <strong class="label label-default" style="margin: 2px">
                        Event Coordinator :
                    </strong>
                    {{$event->event_name}}
                </div>
                <div class="col-lg-4">
                    <strong class="label label-default" style="margin: 2px">
                        Event serial :
                    </strong>
                    {{$event->event_serial}}
                </div>
                <div class="col-lg-4"><strong class="label label-default"
                                              style="margin: 2px"> Event
                        organized
                        by : </strong> {{$event->organized_by}}</div>
                <div class="col-lg-4"><strong class="label label-default"
                                              style="margin: 2px"> Event website
                        : </strong> {{$event->event_website}}</div>

                <div class="col-lg-4"><strong class="label label-default"
                                              style="margin: 2px"> Event
                        proposal
                        end date
                        : </strong> {{$event->proposalEndDate()}}</div>
                <div class="col-lg-12">
                    <h3><strong>highlightes</strong></h3>
                    <div>


                        {!! $event->event_highlightes !!}

                    </div>
                    <h3><strong>details</strong></h3>
                    <div>
                        {!! $event->details !!}
                    </div>
                </div>
            </div>
        </div>

    </div>
    @if(@$editable)
        <div class="col-lg-2">
            <button data-id="{{$event->encyrptId()}}"
                    class="btn btn-warning btn-sm btn-block seeMedia">see media items
            </button>
            <a href="{{"$scope/$module/edit/".$event->encyrptId()}}"
               class="btn btn-primary btn-sm btn-block">edit</a>
            <a data-type="deleteBtn" href="{{"$scope/$module/delete/".$event->encyrptId()}}"
               class="btn btn-danger btn-sm btn-block">delete</a>
            {{--<div class="statusWrapper ">--}}
            {{--<p>change status</p>--}}
            {{--{!! Form::select("status".$event->id,@$statuses,$event->status->id,["class"=>"form-control changeStatusHolder"]) !!}--}}
            {{--<hr/>--}}
            {{--<button class="btn btn-block btn-success changeStatusTrigger">Save</button>--}}
            {{--</div>--}}
        </div>
    @endif
</div>