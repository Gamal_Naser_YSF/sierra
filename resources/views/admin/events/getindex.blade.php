@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                            {{--<i class="fa fa-table"> </i>--}}
                            {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                            {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.".\Illuminate\Support\Str::singular($module))}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    @if(!$rows->isEmpty())
                        <div class="row">
                            @foreach($rows as $row)
                                @include("admin/events/shared/singleEvent",["event"=>$row,"editable"=>true])
                            @endforeach
                        </div>
                    @else
                        <p class="alert alert-warning">
                            there no {{$module}} to show , you can create new item
                            <a href="{{"$scope/$module/create"}}">here</a>
                        </p>
                    @endif
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ev-model">
        {{--<div class="ev-overlay"></div>--}}
        <div class="ev-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 ev-data">

                    </div>
                    <strong class="xBtn">X</strong>

                </div>
            </div>
        </div>
    </div>
@stop

@section("css")
    <style>
        .ev-model {
            display: none;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            background: rgba(0, 0, 0, 0.79);
            z-index: 2000000;
        }

        .ev-content {
            width: 90%;
            height: 90%;
            top: 5%;
            left: 5%;
            position: absolute;
            background: #f3f3f3;
            border-radius: 8px;
            overflow: auto;
            box-shadow: 0 0 19px rgba(255, 255, 255, 0.32);
        }

        .xBtn {
            position: absolute;
            right: 6px;
            top: 6px;
            background: rgb(236, 236, 236);
            border: thin solid #cccccc;
            padding: 7px;
            border-radius: 7px;
            cursor: pointer;
        }

        .ev-data {
            padding: 20px;
        }

        .ev-data button {
            margin-top: 5px;
        }

        .events-single-wrapper {
            background: #fff;
            /*width: 100%;*/
            margin: 8px auto;
            padding-top: 10px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.17);
        }

        .labels {
            font-size: 14px;
            display: inline-block;
            margin-top: 10px;
        }

        .address-wrapper {
            border-right: thin solid rgba(0, 0, 0, 0.09);
        }

        .statusWrapper {
            border-radius: 8px;
            border: thin solid rgba(0, 0, 0, 0.09);
            width: 100%;
            padding: 10px;
        }
    </style>
@stop
@section("js")
    <script>
        $(document).ready(function () {

            $(".changeStatusTrigger").click(function () {
                alert("sd")
            })
//--------------------------------------------------------------
//--------------------------------------------------------------
//--------------------------------------------------------------
            let temp = `<div class="col-lg-3 ev-item">
                            <img class="img-responsive img-rounded" src="upload/1.jpg">
                            <button data-id="" class="btn btn-danger btn-block deleteImg">Delete</button>
                        </div>`;

            $(".seeMedia").click(function () {
                getData($(this).data("id"))
                $(".ev-model").fadeIn()
            });

            $(".xBtn").click(function () {
                clearData()
                $(".ev-model").fadeOut()
            });

            $(".ev-data").on("click", ".deleteImg", function (e) {
                let btn = $(e.target);
                if (confirm("are yo sure you want to delete this image"))
                    $.get("{{url("admin/media/delete/")}}/" + btn.data("id"), {}, function (res) {
                        if (res.status) btn.closest(".ev-item").fadeOut()
                    })
            });

            function clearData() {
                $(".ev-data").html("")
            }

            function getData(id) {
                $.post("{{url("admin/events/media")}}", {_token: "{{csrf_token()}}", id: id}, function (res) {
                    if (res.data.length > 0) {

                        for (let i in res.data) {
                            let item = res.data[i];
                            let newItem = $(temp);
                            newItem.find("img").attr("src", `upload/${item.file_name}`)
                            newItem.find("button").attr("data-id", item.id)
                            $(".ev-data").append(newItem)
                        }
                    } else {
                        $(".ev-data").append(`<div class="alert alert-warning">No media items exist</div>`)
                    }
                    console.log();
                })
            }
        })
    </script>
@stop
