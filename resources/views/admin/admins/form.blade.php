

<?php $input = 'is_super_admin'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::checkbox($input,null,@$row->$input,["data-checkbox"=>"icheckbox_square-blue"]) ])


<?php $input = 'full_name'; ?>
@include("parts.inputForm",["name"=>$input,"input"=>  Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

<?php $input = 'email'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::email($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

@if(request()->is("$scope/$module/create"))
    <?php $input = 'password'; ?>
    @include("parts.inputForm",["name"=>$input,"input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])
@endif

<?php $input = 'phone'; ?>
@include("parts.inputForm",["name"=>$input,"input"=> Form::number($input,@$row->$input,['class'=>'form-control ',"required"=>true])  ])

