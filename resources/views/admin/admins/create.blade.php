@extends("admin.layout")

@section('Title', _t("admin.create")." "._t("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{_t("admin.create")." "._t("admin.$module")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">
            {!! Form::model($row,['url' => $scope.'/'.$module.'/create', 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}

            @include($scope.'.'.$module.'.form')

            {!! Form::submit(_t('admin.Save') ,['class' => 'btn btn-success']) !!}

            {!! Form::close() !!}


        </div>


    </div>

@stop

