@extends("admin.layout")

@section('Title',_t("admin.edit")." ". _t("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{_t("admin.change")." "._t("admin.$module")." "._t("admin.permissions")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">

            {!! Form::open(['method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}
            <input type="hidden" name="id" value="{{request()->id}}">
            <label>{{_t("admin.Select Role")}}</label>
            {{Form::select("role_id",$roles,null,['class'=>'form-control',"multiple"=>true,"style"=>"min-height:150px;"])}}
            @include("admin.parts.RolesPareser",["permissions"=>$permissions,"oldPermissions"=>$oldPermissions])
            <br/>
            {!! Form::submit(_t('admin.Save') ,['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}


        </div>


    </div>

@stop
@section("js")
    @parent
    <script>

      var  wholeList = <?=$permissionsData?>;
        function changeSelection(ele) {

            $('[name^="per"]').prop("checked", false).iCheck("update");
            for (var i in $(ele).val()) {
                makePermissionsChecked(wholeList[$(ele).val()[i]]);
            }
        }
        $("[name=role_id]").change(function () {
            changeSelection(this);
        });
        $("select2").change(function () {
            changeSelection(this);
        });
    </script>
@stop

