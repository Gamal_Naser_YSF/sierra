@extends("admin.layout")

@section('Title',_t("admin.edit")." ". _t("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{_t("admin.change")." "._t("admin.$module")." "._t("admin.password")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">

            {!! Form::open([ 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}

            <?php $input = 'password';?>
            @include("parts.inputForm",["name"=>$input,"input"=> Form::text($input,null,['class'=>'form-control removeArrow']) ])

            <br/>

            {!! Form::submit(_t('admin.Save') ,['class' => 'btn btn--light']) !!}

            {!! Form::close() !!}


        </div>

    </div>

@stop

