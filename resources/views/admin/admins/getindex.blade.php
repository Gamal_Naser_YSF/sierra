@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{_t("admin.Filter")}} </strong>
                                {{_t("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"/$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.$module")}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.email")}}</th>
                            <th>{{_t("admin.phone")}}</th>
                            <th>{{_t("admin.is_super_admin")}}</th>
                            <th>{{_t("admin.create on")}}</th>
                            <th>{{_t("admin.commends")}}</th>
                            <th>{{_t("admin.activation")}}</th>

                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)
                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->email}}</td>
                                <td>{{@$row->phone}}</td>
                                <?php
                                if ($row->is_super_admin) {
                                    $class = "success";
                                    $word = _t("admin.yes");
                                } else {
                                    $word = _t("admin.no");
                                    $class = "danger";
                                }
                                ?>
                                <td><span class="label label-{{$class}}">{{$word}}</span></td>
                                <td>{{@$row->created_at->format("Y-m-d")}}</td>

                                <td>
                                    <a href="{{ url("/$scope/$module/edit/".@$row->encyrptId()) }}"
                                       class="btn btn-primary ">
                                        <span>{{_t("admin.edit")}}</span>
                                    </a>
                                    <a href="{{ url("/$scope/$module/change-user-password/".@$row->encyrptId()) }}"
                                       class="btn btn-primary ">
                                        <span>{{_t("admin.change password")}}</span>

                                    </a>
                                    @if (!$row->is_super_admin)
                                        <a href="{{ url("/$scope/$module/roles/?id=".@$row->encyrptId()) }}"
                                           class="btn btn-primary ">
                                            <span>{{_t("admin.roles")}}</span>
                                        </a>
                                    @endif
                                </td>
                                <td>

                                    &nbsp;&nbsp;
                                    @if($row->active)
                                        <a href="{{ url("/$scope/$module/unactivated/".@$row->encyrptId()) }}"
                                           class="btn btn-danger ">{{_t("admin.deactive")}}
                                            <i class="glyphicon glyphicon-warning-sign"></i>
                                        </a>
                                    @else
                                        <a href="{{ url("/$scope/$module/active/".@$row->encyrptId()) }}"
                                           class="btn btn-success ">{{_t("admin.active")}}
                                            <i class="glyphicon glyphicon-saved"></i>
                                        </a>
                                    @endif
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop
