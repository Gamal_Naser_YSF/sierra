@extends("admin.layout")
@section('Title', __("admin.$module Page"))

@section('content')


    <div class="row">

        <div class="col-lg-10 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3><i class="fa fa-table"> </i>
                                <strong>{{__("admin.Congigs")}} </strong> {{__("admin.Page ")}}
                                <br/>
                                <small>click on any value to edit it</small>
                            </h3>

                        </div>
                    </div>

                </div>

                <div class="panel-content">
                    <div class="row">
                        <form method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="col-lg-2">
                                <input type="submit" class="btn btn-success" value="save">
                            </div>
                            <div class="col-lg-10">

                                <div id="sor1">
                                    @foreach($rows as $row)
                                        <div id="{{$row->id}}" class="conf-fields col-lg-12" style="margin-bottom: 2px"
                                             data-id="{{$row->id}}">
                                            <label style="margin-left: 2px;text-transform: capitalize">{{$row->label}}</label>

                                            @switch($row)

                                                @case($row->type=="textarea")
{{--                                                {{ dd($row->key,$row->value) }}--}}
                                                {!! Form::textarea(($row->key),$row->value,["class"=>"form-control ckeditor"]) !!}
                                                @break

                                                @case($row->type=="file")
                                                {!! Form::file(($row->key),["class"=>"form-control"]) !!}
                                                {!! _ren($row->value)!!}
                                                @break


                                                @case($row->type=="map")
                                                {{--<div id="map"></div>--}}
                                                {{--{!! Form::text("event_location_long",null,["id"=>"event_location_long","class"=>"form-control"]) !!}--}}
                                                {{--{!! Form::text("event_location_lat",null,["id"=>"event_location_lat","class"=>"form-control"]) !!}--}}
                                                {{--{!! Form::text("address",null,["id"=>"address","class"=>"form-control"]) !!}--}}
                                                {{--{!! Form::text("event_location_long",$row->value,["id"=>"d","class"=>"form-control"]) !!}--}}
                                                {!! Form::input($row->type,($row->key),$row->value,["class"=>"form-control"]) !!}

                                                @break

                                                @default
                                                {!! Form::input($row->type,($row->key),$row->value,["class"=>"form-control"]) !!}
                                            @endswitch
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>

        {{--<div class="col-lg-4 ">--}}
        {{--<div class="panel">--}}

        {{--<div class="panel-content">--}}
        {{--<div class="row">--}}
        {{--@include(\Plugins\Media\BootFile::getView())--}}
        {{--</div>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--</div>--}}
    </div>

@stop



@section("js")
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyC0IRqKRHpNKOHLUb1nlC82DiLO6FB9M7Y"></script>
    <script src="front/js/MabClass.js"></script>
    <script>
        //        MabClass.instance({position: {lat: 20, lng: 20}, target: "map"});
    </script>
@stop