@extends("layouts.master")

@section('Title',__("admin.edit")." ". __("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{__("admin.edit")." ".__("admin.$module")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">
            {!! Form::model($row,['route' => ["{$module}.update",$row->id], 'method' => 'PUT','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}

            {!! Form::hidden("id",$row->id) !!}

            @include($scope."_".$module.'::form')

            <hr/>
            {!! Form::submit(__('admin.Save') ,['class' => 'btn btn-success']) !!}

            {!! Form::close() !!}
        </div>
    </div>

@stop