@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                                {{--<i class="fa fa-table"> </i>--}}
                                {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                                {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div>
                    <div class="row">
                        <div class="col-md-8" style="padding: 15px">
                            <h3><strong>trip id : </strong> {{@$trip->id}}</h3>
                            <h3><strong>created on : </strong> {{@$trip->created_at->format("Y-m-d")}}</h3>
                            <h3><strong>User : </strong> {{@$trip->user->name}}</h3>
                        </div>
                        <div class="col-md-4" style="padding: 15px">
                            <a class="btn btn-success" href="{{"$scope/$module/trip-done/{$trip->encyrptId()}"}}">Make Trip Done</a>
                          @includeIf("admin.reservations.parts.InjectedBtn",["model"=>@$trip,"type"=>"trip"])
                        </div>
                    </div>
                </div>

                <div class="panel-content pagination2 table-responsive">

                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>location from</th>
                            <th>location to</th>
                            <th>check in</th>
                            <th>check out</th>
                            <th>guest number</th>
                            <th>hotel rate</th>
                            <th>departure</th>
                            <th>arrival</th>
                            <th>airlines rate</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $tripInfo  as $row)
                            <tr>
                                <td>#</td>
                                <td>{{config("countries.$row->location_from")}}</td>
                                <td>{{config("countries.$row->location_to")}}</td>
                                <td>{{$row->check_in}}</td>
                                <td>{{$row->check_out}}</td>
                                <td>{{$row->guest_number}}</td>
                                <td>{{$row->hotel_rate}}</td>
                                <td>{{$row->departure}}</td>
                                <td>{{$row->arrival}}</td>
                                <td>{{$row->airlines}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

            </div>
        </div>
    </div>
@stop

