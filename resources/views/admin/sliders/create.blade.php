@extends("admin.layout")


@section('Title', __("admin.create")." ".__("admin.$module"))

@section('content')

    <div class="panel">
        <div class="panel-header">
            <i class="icon-check"></i>

            <h2>{{__("admin.create")." ".__("admin.$module")}}</h2>
            <hr/>
        </div>
        <div class="panel-content">
            {!! Form::model($row,['route' => "{$module}.store", 'method' => 'post','class'=>'form-horizontal style-form','enctype'=>'multipart/form-data'] ) !!}

            @include($scope.'.'.$module.'.form')

            {!! Form::submit(__('admin.Save') ,['class' => 'btn btn-success']) !!}

            {!! Form::close() !!}


        </div>


    </div>

@stop

