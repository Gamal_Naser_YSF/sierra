@extends("admin.layout")


@section('Title', __("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>
                                <i class="fa fa-table"> </i>
                                <strong>{{__("admin.Filter")}} </strong>
                                {{__("admin.table")}}
                            </h3>
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{url("/$scope/$module/create")}}"
                       class="btn btn-success">{{__("admin.create")." ".__("admin.".\Illuminate\Support\Str::singular($module))}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__("admin.image")}}</th>
                            <th>{{__("admin.text_ar")}}</th>
                            <th>{{__("admin.text_en")}}</th>
                            <th>{{__("admin.link")}}</th>
                            <th>{{__("admin.link_type")}}</th>
                            <th>{{__("admin.create on")}}</th>
                            <th>{{__("admin.edit")}}</th>
                            <th>{{__("admin.delete")}}</th>


                        </tr>
                        </thead>
                        <tbody class="s-row">

                        @foreach( $rows  as $row)
                            <tr id="{{$row->id}}">
                                <td>
                                    <div class="handle">...</div>
                                </td>
                                <td>{!! _ren(@$row->image) !!}</td>
                                <td>{{@$row->text_ar}}</td>
                                <td>{{@$row->text_en}}</td>
                                <td><a target="_blank" href="{{@$row->link}}">{{@$row->link}}</a></td>

                                <td>{{@$row->getTypeName()}}</td>

                                <td>{{@$row->created_at->format("Y-m-d")}}</td>
                                <td>
                                    <a href="{{ url("/$scope/$module/{$row->id}/edit") }}"
                                       class="btn btn-primary btn-sm ">
                                        <span>{{__("admin.edit")}}</span>
                                    </a>
                                </td>
                                <td>
                                    <a data-type="deleteBtn" href="{{ url("/$scope/$module/delete/".@$row->id) }}"
                                       class="btn btn-danger btn-sm">{{__("admin.delete")}}
                                        <i class="glyphicon glyphicon-saved"></i>
                                    </a>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@stop

<style>
    .handle {
        cursor: pointer;
    }
</style>

@section("js")
    <script>
        $(".s-row").sortable({
            handle: ".handle",
            placeholder: "ui-state-highlight",
            update: function (event, ui) {
                $.post("{{url("/$scope/$module/reorder")}}", {_token: "{{csrf_token()}}", rows: $(this).sortable('toArray')}, function () {

                })
            }
        }).disableSelection();
    </script>
@stop
