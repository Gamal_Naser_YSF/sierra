<?php $input = 'name'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])


<?php $input = 'description'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::textarea($input,@$row->$input,['class'=>'form-control ckeditor',"required"=>true])
])


<?php $input = 'price'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::number($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])

<?php $input = 'discount'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::number($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])
<?php $input = 'package_type'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::text($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])
<?php $input = 'start_date'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::date($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])

<?php $input = 'end_date'; ?>

@include("parts.inputForm",[
    "name"=>$input,
    "required"=>true,
    "input"=> Form::date($input,@$row->$input,['class'=>'form-control ',"required"=>true])
])
