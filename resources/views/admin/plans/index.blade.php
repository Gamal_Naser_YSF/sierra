@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                            {{--<i class="fa fa-table"> </i>--}}
                            {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                            {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{"/$scope/$module/create"}}"
                       class="btn btn-success">{{_t("admin.create")." "._t("admin.".\Illuminate\Support\Str::singular($module))}}</a>
                    <hr/>
                </div>

                <div class="panel-content pagination2 table-responsive">
                    <div>
                        {!! $rows->links() !!}
                    </div>
                    <table class="table table-hover table-dynamic">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{_t("admin.name")}}</th>
                            <th>{{_t("admin.description")}}</th>
                            <th>{{_t("admin.start date")}}</th>
                            <th>{{_t("admin.end date")}}</th>
                            <th>{{_t("admin.package type")}}</th>
                            <th>{{_t("admin.discount")}}</th>

                            <th>{{_t("admin.command")}}</th>


                        </tr>
                        </thead>
                        <tbody>

                        @foreach( $rows  as $row)
                            <tr>
                                <td>{{@$row->id}}</td>
                                <td>{{@$row->name}}</td>
                                <td>{{@$row->description}}</td>
                                <td>{{@$row->start_date}}</td>
                                <td>{{@$row->end_date}}</td>
                                <td>{{@$row->package_type}}</td>
                                <td>{{@$row->discount}}</td>

                                <td>
                                    <a class="btn btn-primary btn-sm btn-block"
                                       href="{{"$scope/$module/edit/".$row->encyrptId()}}">{{_t("admin.edit")}}</a>
                                    <a data-type="deleteBtn" class="btn btn-danger btn-sm btn-block"
                                       href="{{"$scope/$module/delete/".$row->encyrptId()}}">{{_t("admin.delete")}}</a>
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div>
                        {!! $rows->links() !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop



