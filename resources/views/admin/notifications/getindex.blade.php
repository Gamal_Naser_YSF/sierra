@extends("admin.layout")

@section('Title', _t("admin.$module list"))

@section('css')

@stop
@section('content')
    <div class="row">
        <div class="col-lg-12 ">
            <div class="panel">
                <div class="panel-header ">
                    <div class="row">
                        <div class="col-md-12">
                            {{--<h3>--}}
                            {{--<i class="fa fa-table"> </i>--}}
                            {{--<strong>{{_t("admin.Filter")}} </strong>--}}
                            {{--{{_t("admin.table")}}--}}
                            {{--</h3>--}}
                        </div>
                    </div>

                </div>
                <div class="panel-header text-right">
                    <hr/>
                    <a href="{{request()->fullUrlWithQuery(["status"=>null])}}" class="btn btn-primary">All</a>
                    <a href="{{request()->fullUrlWithQuery(["status"=>1])}}" class="btn btn-success">Seen</a>
                    <a href="{{request()->fullUrlWithQuery(["status"=>0])}}" class="btn btn-warning">Not Seen</a>

                    <hr/>
                    <p class="label lb-filter">filter results by type</p>
                    <br/>
                    <a href="{{request()->fullUrlWithQuery(["type"=>null])}}"
                       class="label lb-all label-lg">all</a>
                    <a href="{{request()->fullUrlWithQuery(["type"=>"event"])}}"
                       class="label lb-event label-lg">events</a>
                    <a href="{{request()->fullUrlWithQuery(["type"=>"travel_packages"])}}"
                       class="label lb-travel label-lg">travel packages</a>
                    <a href="{{request()->fullUrlWithQuery(["type"=>"trip"])}}" class="label lb-trip label-lg">create
                        your own trip</a>
                    <a href="{{request()->fullUrlWithQuery(["type"=>"payment"])}}" class="label lb-payment label-lg">payment</a>
                    <a href="{{request()->fullUrlWithQuery(["type"=>"other"])}}"
                       class="label lb-other label-lg">other</a>
                </div>
                <form>

                    <div class="panel-header filter-wrapper">

                    </div>

                    <div class="panel-content pagination2 table-responsive">
                        <div>
                            {!! $rows->links() !!}
                        </div>
                        <table class="table table-hover table-dynamic">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{_t("admin.type")}}</th>
                                <th>{{_t("admin.title")}}</th>
                                <th>{{_t("admin.message")}}</th>

                                <th>{{_t("admin.status")}}</th>

                                <th>{{_t("admin.commands")}}</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            /**
                             * @var \App\Models\Notification $row
                             */

                            ?>
                            @foreach( $rows  as $row)
                                <tr>
                                    <td>{{@$row->id}}</td>
                                    <td><a class="label lb-{{@$row->getType()}} label-lg">{{@$row->getType()}}</a></td>
                                    <td>{{@$row->getTitle()}}</td>
                                    <td>{!! $row->getMessage() !!}</td>
                                    <td>{!! $row->getStatusText() !!}</td>
                                    <td>
                                        @if(!$row->isSeen())
                                            <a class="btn btn-primary btn-sm btn-block"
                                               href="{{"$scope/$module/seen/".$row->encyrptId()}}">
                                                {{_t("admin.seen")}}
                                            </a>
                                        @endif

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>
                            {!! $rows->links() !!}
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop


@section("js")
    <script>
        $("[name=changeStatus]").on("ifChanged", function () {
            console.log()
            $.post("{{url("$scope/categories/change-status")}}", {
                _token: "{{csrf_token()}}",
                id: $(this).val()
            }, function (res) {
                console.log(res)
            })
        })
    </script>
@stop
