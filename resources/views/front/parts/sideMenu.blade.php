<div id="menu_left" class="nav-left">
    <div class="dis-table">
        <div class="cell-mid">
            <div class="temp-sitting">
                <ul class="menu-list">
                    <li class="menu-item ">
                        <a class="menu-link" href="{{url("/")}}">Home</a>
                    </li>

                    <li class="menu-item ">
                        <a class="menu-link" href="{{route("pages.about")}}">About US</a>
                    </li>
                    <li class="menu-item ">
                        <a class="menu-link" href="{{route("front.search")}}">Events</a>
                    </li>
                    <li class="menu-item ">
                        <a class="menu-link" href="{{route("front.packages")}}">Travel Packages</a>
                    </li>
                    <li class="menu-item ">
                        <a class="menu-link" href="{{route("front.trip")}}">Trips</a>
                    </li>
                    <li class="menu-item ">
                        <a class="menu-link" href="{{route("pages.plans")}}">Plans</a>
                    </li>
                </ul>
            </div>
        </div>
        <div id="menu_close" class="close-menu">
            <i class="icon-cross"></i>
        </div>
    </div>

</div>