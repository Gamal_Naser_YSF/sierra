<footer>
    <div class="container">
        <div class="upper-footer">
            <div class="row">
                <div class="col-6 col-sm-3 col-md-2">
                    <h6 class="footer-header">Sitemap</h6>
                    <ul class="footer-list">
                        <li><a href="{{url("/")}}">Home</a></li>
                        <li><a href="{{route("front.search")}}">Events</a></li>
                        <li><a href="{{route("front.packages")}}">Travel Packages</a></li>
                        <li><a href="{{route("front.trip")}}">Trip</a></li>
                    </ul>
                </div>
                <div class="col-6 col-sm-3 col-md-2">
                    <h6 class="footer-header">Info</h6>
                    <ul class="footer-list">
                        <li><a href="{{route("pages.about")}}">Company</a></li>
                        <li><a href="{{route("pages.about")}}">Mission</a></li>
                        <li><a href="{{route("pages.about")}}">Vision</a></li>
                        <li><a href="{{route("pages.about")}}">About us</a></li>
                        <li><a href="{{route("pages.about")}}">Contact us</a></li>
                    </ul>
                </div>
                <div class="col-6 col-sm-3 col-md-2">
                    {{--<h6 class="footer-header">Shop</h6>--}}
                    {{--<ul class="footer-list">--}}
                        {{--<li><a href="#">Delivery</a></li>--}}
                        {{--<li><a href="#">Catalog</a></li>--}}
                    {{--</ul>--}}
                </div>
                <div class="col-6 col-sm-3 col-md-2">
                    {{--<h6 class="footer-header">company</h6>--}}
                    {{--<ul class="footer-list">--}}
                        {{--<li><a href="#">Mission</a></li>--}}
                        {{--<li><a href="#">Vision</a></li>--}}
                        {{--<li><a href="#">Strategy</a></li>--}}
                        {{--<li><a href="#">Updates</a></li>--}}
                    {{--</ul>--}}
                </div>
                <div class="col-md-4">
                    <h6 class="footer-header">PAYMENT METHODS</h6>
                    <div class="methods row no-gutters align-content-around">
                        <div class="col col-md-2 single-img">
                            <img class="img-fluid" src="img/master.png">
                        </div>
                        <div class="col col-md-2 single-img">
                            <img class="img-fluid" src="img/visa.png">
                        </div>
                        {{--<div class="col col-md-2 single-img">--}}
                            {{--<img class="img-fluid" src="img/paypal.png">--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer">
            <div class="row">
                <div class="col-sm">
                    <div class="footer-logo">
                        <img style="width: 138px;" src="cdn/footer-logo.png">
                    </div>
                </div>
                <div class="col-sm text-md-right">
                    <div class="d-inline-block text-left">
                        <div class="ic-cont ">
                            <a href="https://www.facebook.com/Sierra-Global-Solutions-135706340237265/" target="_blank">
                                <div class="myico facebook-wrapper">
                                    <i class="icon-facebook" aria-hidden="true"></i>
                                </div>
                            </a>
                            <a href="https://twitter.com/sierra_global" target="_blank">
                                <div class="myico twitter-wrapper">
                                    <i class="icon-twitter" aria-hidden="true"></i>
                                </div>
                            </a>
                            <a href="https://www.linkedin.com/company/sierra-global-solution/" target="_blank">
                                <div class="myico linkedin-wrapper">
                                    <i class="icon-linkedin" aria-hidden="true"></i>
                                </div>
                            </a>
                            <a href="https://www.youtube.com/channel/UCN3LsnWqEQSYiEkPi4S6a6w?view_as=subscriber" target="_blank">
                                <div class="myico youtube-wrapper">
                                    <i class="icon-youtube" aria-hidden="true"></i>
                                </div>
                            </a>
                            <a href="https://www.instagram.com/sgs_agency/" target="_blank">
                                <div class="myico google-plus-wrapper">
                                    <i class="icon-instagram2" aria-hidden="true"></i>
                                </div>
                            </a>
                        </div>
                        <div class="rights smallDesc">
                            &copy; Copyright | SIERRA {{date("Y")}}. All rights reserved.
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>