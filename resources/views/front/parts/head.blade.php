<head>
    <base href="{{url(env("APP_BASE_URL"))}}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"><!-- Mobile Metas -->
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title></title>
    <meta name="description" content=" Website"/><!--Desc-->

    <link rel="icon" href="favicon.ico" type="image/x-icon"/><!--browser icon-->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/><!--browser icon-->
    <link rel="apple-touch-icon-precomposed" href="favicon.png">
    <!--icons style-->
    <link rel="stylesheet" href="fonts/icons/style.css">
    <!--website style-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/payment.css">
    <link rel="stylesheet" href="css/costume.css">
    <link rel="stylesheet" href="css/topBarStyle.css">
    <link rel="stylesheet" href="css/hamburgers.min.css">
    <link href="lib/sweetalert2/dist/sweetalert2.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/r29/html5.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
    <![endif]-->
    @yield("css")
</head>