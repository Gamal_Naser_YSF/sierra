<div class="reg-popup">
    <div class="row">
        <div class="col-md-10 ml-auto">
            <div id="login_message" class="myDesc login-msg">You must login to enter this page</div>
            <div class="alert  hidden">
                <p>This form seems to be invalid </p>
            </div>
            <div class="alert  hidden">
                <p>Thank you. Your submission has been received </p>
            </div>
            <div class="section-wrapper">
                <div class="forms">
                    <div id="login_form_container" class="foremSection pop-show">
                        <div class="row align-items-center">
                            <div class="col-sm-5 hidden-xs-down align-self-stretch d-none d-md-block">
                                <div class="img-cont"
                                     style="background: url('cdn/contact-bg.png')center center no-repeat;background-size: cover">
                                    <div class="distable">
                                        <div class="cellbottom">
                                            <div class="responsive_head">Hello
                                                <div class="user">User.</div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="heading switcher-cont">
                                    <div class="link-header">
                                        <a data-form="#login_form_container" href="#"
                                           class="loginReg_show  myDesc active">Login</a>
                                    </div>
                                    <div class="link-header">
                                        <a data-form="#register_form_container" href="#"
                                           class="loginReg_show  myDesc">Register</a>
                                    </div>
                                </div>
                                <div class="smallDesc new-acc">Don`t have an account?
                                    <a data-form="#register_form_container" href="#"
                                       class="loginReg_show btn-link">Create your account</a>
                                    , it takes less than a minute
                                </div>
                                <div class="registration-form">
                                    <form data-parsley-validate="" id="login_form">
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control"
                                                   id="login_email"
                                                   placeholder="Email Address" required>
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control"
                                                   id="login_password"
                                                   placeholder="Password ****" required>
                                        </div>

                                        <div class="text-center">
                                            <a href="{{url("account/auth/forget-password")}}">forget your password</a><br/>
                                            <button id="loginTrigger" type="submit" class="btn btn-theme smallDesc">
                                                Login
                                            </button>
                                        </div>
                                    </form>
                                    <div class="social-login">
                                        <div class="smallDesc text-center">Login with social media</div>
                                        <div class="social-buttons">
                                            <div class="row">
                                                @include("front.parts.soicla")

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="register_form_container" class="foremSection">
                        <div class="row align-items-center">
                            <div class="col-sm-5 hidden-xs-down align-self-stretch d-none d-md-block">
                                <div class="img-cont"
                                     style="background: url('cdn/contact-bg.png')center center no-repeat;background-size: cover">
                                    <div class="distable">
                                        <div class="cellbottom">
                                            <div class="responsive_head">Hello
                                                <div class="user">User.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="heading switcher-cont">
                                    <div class="link-header">
                                        <a data-form="#login_form_container" href="#"
                                           class="loginReg_show  myDesc">Login</a>
                                    </div>
                                    <div class="link-header">
                                        <a data-form="#register_form_container" href="#"
                                           class="loginReg_show  myDesc active">Register</a>
                                    </div>
                                </div>
                                <div class="registration-form">
                                    <form data-parsley-validate="" id="reg_form">
                                        <div class="form-group">
                                            <input type="text" name="name" class="form-control"
                                                   id="reg_name"
                                                   placeholder="Name"
                                                   required>
                                            <div class="msg"></div>

                                        </div>
                                        <div class="form-group">
                                            <input type="email" name="reg_email" class="form-control"
                                                   id="reg_email"
                                                   placeholder="Email Address" required>
                                            <div class="msg"></div>

                                        </div>
                                        <div class="form-group">
                                            <input type="text" name="reg_phone" class="form-control" id="reg_phone"
                                                   placeholder="Phone no."
                                                   data-parsley-type="digits" required>
                                            <div class="msg"></div>

                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control"
                                                   id="reg_password"
                                                   placeholder="Password ****" required>
                                            <div class="msg"></div>

                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control"
                                                   id="reg_password_confirmation"
                                                   placeholder="Confirm Password" required>
                                            <div class="msg"></div>

                                        </div>
                                        <div class="form-check smallDesc form-group">
                                            <label class="custom-control custom-checkbox mb-2 mr-sm-2 mb-sm-0">
                                                <input type="checkbox" name="accept" id="reg_accept"
                                                       class="custom-control-input" required>
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">I Accept terms and conditions & privacy policy</span>
                                            </label>
                                            <div class="msg"></div>
                                        </div>
                                        <div class="text-center">
                                            <button id="registerTrigger" class="btn btn-theme smallDesc btnReg">
                                                Register
                                            </button>
                                        </div>
                                    </form>
                                    <div class="social-login">
                                        <div class="smallDesc text-center">Login with social media</div>
                                        <div class="social-buttons">
                                            <div class="row">
@include("front.parts.soicla")
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="close-popup"><i class="icon-cross"></i></div>
            </div>
        </div>
    </div>
</div>


@section("js")
    <script>

    </script>
@stop