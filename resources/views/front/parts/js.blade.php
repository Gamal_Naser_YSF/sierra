<script>
    baseUrl = function () {
        return "{{url("/")}}";
    }
    _token = function () {
        return "{{csrf_token()}}";
    }
</script>
<!--jquery core v 3.2.1-->
<script src="js/vendors/jquery-3.2.1.min.js"></script>
<script src="js/plugines.js"></script>
<script src="js/Event.js"></script>
<!-- bootstrap -->
<!--<script src="js/vendors/jquery-ui.min.js"></script>-->
<script src="js/vendors/popper.min.js"></script>
<script src="js/vendors/bootstrap.min.js"></script>
<script src="js/vendors/bootstrap-datepicker.min.js"></script>
<!--<script src="js/vendors/bootstrap-datepicker.min.js"></script>-->
<!-- nice scroll -->
<script src="js/vendors/jquery.nicescroll.min.js"></script>
<script src="js/vendors/jquery.lazyload.min.js"></script>
<!-- smooth scroll -->
<script src="js/vendors/jquery.smooth-scroll.min.js"></script>
<!-- fancy box -->
<script src="js/vendors/jquery.fancybox.min.js"></script>

<script src="js/vendors/masonry.pkgd.min.js"></script>
<script src="js/vendors/wookmark.min.js"></script>
<!-- chosen select plugin -->
<script src="js/vendors/chosen.jquery.min.js"></script>
<script src="js/vendors/monthly.js"></script>
<!-- countTo -->
<script src="js/vendors/mixitup.min.js"></script>
<!--<script src="js/vendors/jquery.countTo.js"></script>-->
<!-- parsley -->
<script src="js/vendors/parsley.min.js"></script>
<!--<script src="js/vendors/parsleylang/ar.js"></script>-->
<!-- sliders -->
<script src="js/vendors/owl.carousel.min.js"></script>
<!-- wow animate -->
<script src="js/vendors/wow.min.js"></script>
<!--<script src="js/vendors/particles.min.js"></script>-->
<!-- my custom scripts -->
<script src="js/scripts.js"></script>
<script src="js/Resourses.js"></script>
<script src="js/cscripts.js"></script>
<script src="js/requests.js"></script>
<script src="js/Angular.js"></script>
<script src="dashboard/global/plugins/bootstrap-tags-input/bootstrap-tagsinput.min.js"></script> <!-- Select Inputs -->
<script src="js/LaravelError.js"></script>
<script src="lib/sweetalert2/dist/sweetalert2.js"></script>
<script>
    (function () {






        let errors = @json(@$errors->getMessages())     ;
        LaravelErrors(errors).showHints();
    })()
</script>

<script>

</script>