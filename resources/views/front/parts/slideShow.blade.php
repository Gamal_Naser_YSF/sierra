@section("css")
    @parent
    <style>
        #msli .item img {
            display: block;
            width: 100%;
            height: 600px;
        }
        .mLogo{
            position: absolute;
            top: 29px;
            left: 97px;
            z-index: 10;
            width: 155px;
        }
    </style>
@stop
<div style="position: relative;">
    <div id="msli" class="owl-carousel owl-theme">
        @foreach($sliders as $item)
            <div class="item"><img src="{{$item->getImageAsLink()}}" alt="The Last of us"></div>
        @endforeach
    </div>
    <img class="mLogo" src="cdn/tr_logo.png">
</div>

@section("js")
    @parent
    <script>
        $(document).ready(function () {

            $("#msli").owlCarousel({

//                navigation: false, // Show next and prev buttons
                slideSpeed: 300,
                paginationSpeed: 400,
                items: 1,
                loop: true,
                rewind: false,
                smartSpeed: 300,
//                nav: true,
                dots: true,
//                singleItem: true,

//                 singleItem:true
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    </script>
@stop