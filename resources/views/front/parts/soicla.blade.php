<div class="col-sm socialLogin">
    <a href="{{url()->route("fbLogin",["provider"=>"facebook"])}}" class="btn btn-facebook">
        <i class="icon-facebook"></i>facebook
    </a>
    <a href="{{url()->route("fbLogin",["provider"=>"twitter"])}}" class="btn btn-twitter">
        <i class="icon-twitter"></i>twitter
    </a>
    <a href="{{url()->route("fbLogin",["provider"=>"google"])}}" class="btn btn-google-plus">
        <i class="icon-google-plus"></i>Google +
    </a>
</div>







@section("js")
    {{--@parent--}}
    <script>
        $.oauthpopup = function (options) {
            let w = 800;
            let h = 400;
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var top = ((height / 2) - (h / 2)) + dualScreenTop;


            options.windowName = options.windowName || 'loading'; // should not include space for IE
            options.windowOptions = options.windowOptions || 'location=0,status=0,scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left;
            options.callback = options.callback || function () {
                window.location.reload();
            };
            var that = this;
//            console.log(options.path);
            that._oauthWindow = window.open(options.path, options.windowName, options.windowOptions);
            that._oauthInterval = window.setInterval(function () {
                if (that._oauthWindow.closed) {
                    window.clearInterval(that._oauthInterval);
                    options.callback();
                }
            }, 1000);
        };

        $(".socialLogin").find("a").each(function () {
            $(this).click(function (e) {
                e.preventDefault();
                $.oauthpopup({path: $(this).attr("href")});
            })
        })

    </script>
@stop