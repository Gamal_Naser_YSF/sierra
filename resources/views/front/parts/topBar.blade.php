<div style="position: fixed;width: 100%;    z-index: 100;">
    <div class="container-fluid topBarWrapper">
        <div class="row">
            <div class="col-2 " style="padding-top: 5px">
                <a class="float-right menu-btn">
                    <i class="icon-menu"></i>
                </a>
                <div class="mnWraaper">
                    @if(!request()->is("/"))
                        <div class="logoItem">
                            <a href="/">
                                <img src="cdn/logo.png">
                            </a>
                        </div>
                    @endif

                    <div class="meItem">
                        <a href="{{route("front.search")}}"><i class="icon-arrow-right"></i> Events</a>
                    </div>

                    <div class="meItem">

                        <a href="{{route("front.packages")}}"> <i class="icon-arrow-right"></i> Travel Packages</a>
                    </div>
                    <div class="meItem">
                        <a href="{{route("front.trip")}}"> <i class="icon-arrow-right"></i>Trip</a>
                    </div>
                    <div class="meItem">
                        <a href="{{route("pages.about")}}"> <i class="icon-arrow-right"></i>about us</a>
                    </div>


                </div>
            </div>

            <div class="col-7 pt text-right" style="position: relative">
                <i class="icon-phone3 myDesc mr-1"></i>
                <span class="tb-phone-info">
                {{_con("main_phone")}}
            </span>
            </div>
            <div class="col-2 infSec" style="{{(!auther()::islogin())?"padding-top: 7px;":""}}">
                @if(!auther()::islogin())
                    <a data-form="#login_form_container" href="#" class="float-right reg_show ">
                        <span>Login or create account</span>
                        <i class="icon-user pull-right"></i>
                    </a>
                @else
                    <a class="accountLink float-right" href="{{url("/account")}}">
                        <img class="topImag" src="{{@auther()::getUser()->getAvatar()}}"/>
                        @if(@$userNotification->count>0)
                            <span></span>
                        @endif
                    </a>
                    <div class="float-right" style="position:relative;display: inline-block">
                        <span class="usrName">

                        {{str_limit(@auther()::getUser()->name,20)}}
                        </span>
                        <a class="logOutLink" href="{{url("/account/auth/logout")}}">Logout</a>
                    </div>
                @endif
            </div>
            <div class="col-1 pt searchWappre">
                <a id="seachTrigger">
                    <i class="icon-find"></i>
                </a>
            </div>
        </div>
    </div>

</div>
<div class="searchForm">
    <form action="/search">
        <input name="q" placeholder="What is in you mind?">
    </form>
</div>
<div style="    height: 50px;background: red">

</div>
@include("front.parts.register")
