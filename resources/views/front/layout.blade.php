<!DOCTYPE html>
<html lang="en">
@include("front.parts.head")
<body>
<div class="fixed-loading">
    <div class="dis-table">
        <div class="cell-mid">
            <div class="content">
                <p>Loading...</p>
                <div class="loading">
                    <div class="l1"></div>
                    <div class="l2"></div>
                    <div class="l3"></div>
                </div>
            </div>
        </div>
    </div>

</div>
@includeIf("front.parts.topBar")


<!--over page layer class dim to disable close the form -->
<div class="container">
</div>
<div id="body_overlay" class="body-overlay"></div>
@yield("content")
<div class="float-pup">
    @include('flash::message')
</div>
@include("front.parts.footer")
<!-- to Top Button -->
<a id="back-to-top" href="#" class="btn btn-Warning btn-sm back-to-top">
    <span class="icon-angle-double-up"></span>
</a>
@include("front.parts.js")
@yield("js")
</body>

</html>
