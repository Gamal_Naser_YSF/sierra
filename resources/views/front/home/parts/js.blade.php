<script>
    class request {
        construct() {
        }

        _getQueryString() {
            let qs = decodeURI(window.location.search.split("?")[1]);
            if (!qs) return {};
            let $rt = {};
            qs.split("&").forEach((item) => {
                let [key, val] = item.split("=");
                //if key is array
                if (key.endsWith("[]")) {
                    let noBracketKey = key.replace("[]", "");
                    if (!$rt[noBracketKey])
                        $rt[noBracketKey] = [];
                    $rt[noBracketKey].push(val);
                } else { // if key is string
                    $rt[key] = val;
                }
            })
            return $rt
        }

        execute() {
            let loc = window.location;
            $.get(loc.protocol + loc.pathname, this._getQueryString(), function (res) {

            })
        }
    }


    let post = new request();

    post.execute()

    //    let se = function () {
    //        var self = this;
    //        self.eventWrapper = $(".event-list");
    //        self.updateQueryStringParameter = function (uri, key, value) {
    //            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    //            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    //            return (uri.match(re)) ? (uri.replace(re, '$1' + key + "=" + value + '$2')) : (uri + separator + key + "=" + value);
    //        }
    //
    //
    //    }

    //    $("#orderSelect").change(function () {
    //            window.location.href = updateQueryStringParameter(window.location.href, "order", $(this).val());
    //        }
    //    )

    //    $('.select-tags').each(function () {
    //        $(this).tagsinput({
    //            tagClass: 'label label-primary'
    //
    //        });
    //    });

</script>