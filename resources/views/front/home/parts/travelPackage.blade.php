<div class="{{(@$smallView)?"col-lg-6 col-xl-4":"col-md-6 col-lg-4 col-xl-3"}}">
    <div class="item-card wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
        <a href="{{url("/package/".$package->encyrptId())}}">
            <div class="img-cont">
                <img class="img-fluid w-100" src="upload/{{$package->getFirstImageName()}}">
                <div class="message bg-sale bg-stock">save 20%</div>
            </div>
            <div class="disc-cont">
                <h2 class="item-title myDesc">
                    {{$package->name}}
                </h2>
                <div class="country">
                    {{$package->destination}}
                </div>
                <div class="item-desc smallDesc">
                    {{$package->description}}
                </div>
                <div class="row no-gutters align-items-center">
                    <div class="col">
                        <div data-rate="{{$package->rate}}"  class="rate-wrapper rate_generate">
                            <span class="rate-item icon-star-empty"></span>
                            <span class="rate-item icon-star-empty"></span>
                            <span class="rate-item icon-star-empty"></span>
                            <span class="rate-item icon-star-empty"></span>
                            <span class="rate-item icon-star-empty"></span>
                        </div>
                    </div>
                    <div class="col text-right">
                        <div class="book">
                            <a href="{{url("/package/".$package->encyrptId())}}" class="buy-btn">
                                <i class="calender icon-calendar"></i>
                                Buy Now
                                <i class="icon-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

</div>


