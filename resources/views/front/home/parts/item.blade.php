<?php //dd($category); ?>
<div class="grid-item event-grid">
    <div class="item-wrapper">
        <img class="img-fluid" src="upload/{{$category->image}}">
        <div class="main-overlay">
            <div class="dis-table">
                {{--
                <div class="cell-mid">--}}
                    {{--<h3 class="smallHeading">{{$category->name}}</h3>--}}
                    {{--
                </div>
                --}}
            </div>
        </div>
        <div class="hover-overlay">
            <div class="dis-table">
                <div class="cell-mid">
                    <h3 class="smallDesc">{{$category->name}}</h3>
                    <div class="myDesc">
                        {{$category->description}}
                    </div>
                    <a href="{{url("search?categories[]=".$category->name)}}"
                       class="btn btn-theme-second smallDesc">View
                        Details</a>
                </div>
            </div>
        </div>
    </div>
</div>

