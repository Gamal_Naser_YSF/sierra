<div id="tab_{{$typeName}}" class="col-12  single-item {{($first)?"active":""}}">
    <div class="row">
        @foreach($categoriesType as $category)
            <div class="col-md-4 col-12 " style="margin-bottom: 25px">

                <div class="cat-holder hasAnim">
                    <div class="cat-img-wapper">
                        <img class="hasAnim" src="upload/{{$category->image}}"/>
                    </div>
                    <div class="cat-info hasAnim">
                        <h3>{{$category->name}}</h3>
                        <p>{{str_limit($category->description,160)}}</p>
                        <a href="{{route("front.search.alias",["cat"=>$category->name])}}"
                           class="btn btn-theme-second smallDesc">View
                            Details</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>