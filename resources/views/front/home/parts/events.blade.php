<div class="section-switcher">
    <?php $first = true; ?>
    @foreach($types as $type )
        <div data-target="#tab_{{$type->name}}" data-bg="cdn/bsbg.png" class="switch-link {{($first)?"active":""}}">
            {{$type->name}}
        </div>
        <?php $first = false; ?>
    @endforeach
    <?php unset($first); ?>
</div>
<div class="tabs-content wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
    <div class="row">
        <div class="col-md-12 mx-auto">
            <div class="row">

                <?php $first = true; ?>
                @foreach($categoriesTypes as $typeName => $categoriesType)
                    {{--              @include("front.home.parts.homeCategorySingle")--}}
                    @include("front.home.parts.newHomeCategorySingle")
                    <?php $first = false; ?>
                @endforeach
                <?php unset($first); ?>

            </div>

        </div>
    </div>
</div>