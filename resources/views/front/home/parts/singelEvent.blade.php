<?php
/**
 * @var \App\Models\Event $event
 */
?>
<div class="single-event">
    <a href="event-details/{{$event->encyrptId()}}">
        <div class="row">
            <div class="col-lg-4">
                <div class="img-cont">
                    <img src="upload/{{$event->getFirstImageName()}}" class="img-fluid w-100">
                    <div class="ev-date">
                        <span class="strong">{{$event->getDay()}}</span>
                        <span>{{$event->getMonth()}}</span>
                        ----------
                        <span>{{$event->getYear()}}</span>


                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="ev-desc">
                    <div class="ev-title myDesc">
                        {{$event->event_name}}
                    </div>
                    <div class="smallDesc">
                        {{str_limit(strip_tags($event->details),450)}}
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>