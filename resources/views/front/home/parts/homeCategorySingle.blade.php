<div id="tab_{{$typeName}}" class="single-item {{($first)?"active":""}}">
    <div class="gallery-grid">
        <div class="myElementContainer grid switch-grid">
            @foreach($categoriesType as $category)
                @include("front.home.parts.item",["event"=>@$category])
            @endforeach
        </div>
    </div>
    <div class="text-center">
        <a href="{{url("/search")}}" class="btn-theme-second-reversed smallDesc">Discover More</a>
    </div>
</div>