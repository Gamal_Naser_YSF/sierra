@extends("front.layout",["title"=>"Welcome age"])



@section("content")
    @include("front.parts.slideShow")

    <div id="page_wrapper" class="page-wrapper homepage-wrapper">

        <section id="conferences_sec" class="conferences-section  ">
            <div class="container">
                <div class="sec-header">
                    <div class="ic-wrapper"><i class="icon-Conferences"></i></div>
                    <div class="title-wrapper">
                        <h2 class="heading">Conferences and <br>Exhibition.</h2>
                        <div class="smallDesc disclaimer">Dream. Explore. Discover.</div>
                    </div>
                </div>
                <div class="tabs-wrapper">
                    @include("front.home.parts.events")
                </div>
            </div>
        </section>

        <section class="packages-section">
            <div class="container">
                <div class="sec-header">
                    <div class="ic-wrapper"><i class="icon-packages"></i></div>
                    <div class="title-wrapper">
                        <h2 class="heading">Sierra travel<br>Packages.</h2>
                        <div class="smallDesc disclaimer">You've come to the right place!</div>
                    </div>
                </div>
                <div class="packages-gallery">
                    <div class="row">
                        @if(@$travelPackages)
                            @foreach($travelPackages as $package)
                                @include("front.home.parts.travelPackage",["package"=>$package])
                            @endforeach
                        @endif
                    </div>
                    <div class="text-center">
                        <a href="{{url("/travel-package/")}}" class="btn-theme-second-reversed smallDesc">
                            View All Packages
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="trip-section">
            <div class="container">
                <div class="sec-header">
                    <div class="ic-wrapper"><i class="icon-icon_4"></i></div>
                    <div class="title-wrapper">
                        <h2 class="heading">Create your own<br>Trip.</h2>
                        <div class="smallDesc disclaimer">You've come to the right place!</div>
                    </div>
                </div>
                <div class="box-msg row">
                    <div class="msg-wrapper col-md-5 mx-auto">
                        <div class="row align-items-center">
                            <div class="col-9">
                                <div class="text-cont myDesc">
                                    don't think twice, explore every part in the world by few clicks
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="btn-cont">
                                    <a href="/create-trip">
                                        <i class="icon-arrow-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <div class="empty-space"></div>

        <section class="price-section">
            <div class="container">
                <div class="sec-header">
                    <div class="title-wrapper">
                        <h2 class="heading">A different plans to suit<br>every need.</h2>
                        <div class="smallDesc disclaimer">Relax. You’re with us! We make it simple.</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="price-table wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
                            <div class="row">
                                @foreach($plans as $plan)
                                    <div class="col-md-4 single-offer">
                                        <div class="item-wrapper">
                                            <h2 class="item-title myDesc">{{$plan->name}}</h2>
                                            <div class="price-cont">
                                                <span class="currency">$</span>
                                                <span class="price-result">{{$plan->price}}</span>
                                            </div>
                                            <div class="item-desc">
                                                {!! $plan->description !!}
                                            </div>
                                            <a href="{{url("/plans")}}" class="btn-theme-flat smallDesc">Discover
                                                More</a>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="sponsors-section">
            <div class="container">
                <div class="owl-multi owl-carousel owl-theme">


                    @foreach($partners as $partner)
                        <div class="single-item">
                      {!! _ren($partner->logo,"upload/",["class"=>"img-fluid"]) !!}
                        </div>
                    @endforeach


                </div>
            </div>
        </section>

        <section class="newsletter-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="section-wrapper wow fadeInUp" data-wow-duration=".5s" data-wow-delay=".4s">
                            <div class="row no-gutters align-items-stretch">
                                <div class="col-md-4">
                                    <img class="img-fluid" src="img/newsletter.jpg">
                                </div>
                                <div class="col-md-8">
                                    <div class="news-form">
                                        <div class="myDesc"><span>Sign up</span> for latest Updates <span>&amp;</span>
                                            Offers
                                        </div>
                                        <form class="search-form" data-parsley-validate="">
                                            <div class=" alert alert-danger hidden">
                                                <p>This form seems to be invalid </p>
                                            </div>

                                            <div class="alert alert-success hidden">
                                                <p>Thank you. Your submission has been received </p>
                                            </div>
                                            <div class="form-group">
                                                <input type="text" class="form-control smallDesc" name="email"
                                                       id="news_email"
                                                       placeholder="Type in your Email Address" required>
                                            </div>
                                            <button type="submit" class="btn btn-theme-main smallDesc">Search</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>




@stop
