@extends("front.layout",["title"=>"Welcome age"])
@section("css")
    <style>
        * {
            box-sizing: border-box;
        }

        .cat-wapper {
            cursor: pointer;
            background: #23295e;
            width: 100%;
            height: 100px;
            position: relative;
            overflow: hidden;
        }

        .img-wapper {
            width: 100%;
            height: 100%;
            display: flex;
            justify-content: center;
            overflow: hidden;
            transition: all cubic-bezier(1, 0.02, 0, 1.01) .3s;

        }

        .img-wapper img {
            display: block;
        }

        .cat-wapper i {
            display: none;
        }

        .cat-active {
            border: 7px solid #ffa200;
        }

        .cat-active i {
            right: 0;
            display: block;
            position: absolute;
            top: 0;
            background: #ffa200;
            padding: 5px;
            font-size: 14px;
            color: #fff;
        }

        .cat-wapper .title {
            font-family: Montserrat, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
            background: #000000ab;
            color: #fff;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            position: absolute;
            width: 100%;
            height: 100%;
            transition: all cubic-bezier(1, 0.02, 0, 1.01) .3s;
            top: 0;
        }

        .cat-wapper:hover .title {
            top: 120%;
        }

        h1 {
            font-family: Montserrat, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
        }

        .cat-wapper:hover .img-wapper {
            transform: scale(1.1);
        }
    </style>
@stop


@section("content")

    <div class="row" style="margin-top: 20px;padding: 20px;">
        <div class="col-lg-12 text-center">
            <h1>Please select your interested</h1>
            <hr/>
        </div>
        @foreach($categories as $category )
            <div class="col-lg-3" style="margin-bottom: 10px;">
                <div data-item-id="{{$category->id}}" class="cat-wapper ">
                    <div class="img-wapper">
                        <img src="upload/{{$category->image}}">
                    </div>
                    <div class="title">
                        <span>{{$category->getTypeName()}}</span>

                        <strong>{{$category->name}}</strong>

                    </div>

                    <i class="icon-favorite"></i>
                </div>
            </div>

        @endforeach
        <div class="col-lg-12">
            <button id="saveInter" class="btn btn-lg btn-block" style="background: #ffa200">Save and complete</button>
        </div>
    </div>
@stop

@section("js")
    <script>
        (function () {
            localStorage.clear();
            let db = {
                getData: function () {
                    let oldData = JSON.parse(localStorage.getItem("favCat"));
                    if (!oldData)
                        oldData = {};
                    return oldData;
                },
                insert: function (id) {
                    let oldData = this.getData();
                    oldData["T" + id] = id;
                    localStorage.setItem("favCat", JSON.stringify(oldData));
                },
                remove: function (id) {
                    let oldData = this.getData();
                    delete oldData["T" + id];
                    localStorage.setItem("favCat", JSON.stringify(oldData));
                }
            }


            $(".cat-wapper").click(function () {

                if ($(this).hasClass("cat-active")) {
                    $(this).removeClass("cat-active");
                    db.remove($(this).data("item-id"))
                } else {
                    $(this).addClass("cat-active");
                    db.insert($(this).data("item-id"))
                }

            });

            $("#saveInter").click(function () {
                $.ajax({
                    method: "POST",
                    url: baseUrl() + "/save-interest",
                    data: {_token: _token(), data: db.getData()},
                    success: function (res) {
                        if(res.status)
                            window.location.reload();
                    }, error: function (res) {
                        sirraAlert(res.responseJSON.message)
                    }
                });
            });
        })()
    </script>
@stop
