@extends("front.layout")
@section("content")

    <div class="inner-wrapper even-details-wrapper">
        <section class="even-details-section">
            <div class="container">
                <div class="text-center">
                    <h2 class="smallHeading line-bottom page-title">{{$row->event_name}}</h2>
                </div>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="details-wrapper">
                            <div class="img-cont">
                                <div class="owl-single owl-carousel owl-theme">
                                    @foreach($row->media as $media)
                                        <div class="single-item">
                                            <img style="height: 360px" src="upload/{{$media->file_name}}"
                                                 class="img-fluid w-100">
                                        </div>
                                    @endforeach
                                </div>
                                <div class="ev-date">
                                    <span class="strong">{{$row->getDay()}}</span>
                                    <span>{{$row->getMonth()}}</span>
                                </div>
                            </div>
                            <div class="ev-desc">

                                <div class="smallDesc">
                                    <h3><strong>Highlightes</strong></h3>
                                    <div>
                                        {!! $row->event_highlightes !!}
                                    </div>
                                    <h3><strong>Details</strong></h3>
                                    <div>
                                        {!! $row->details !!}
                                    </div>
                                    <hr/>
                                </div>
                            </div>
                        </div>
                        <div class="list-details-wrapper">
                            <div class="row">
                                <div class="col-6 col-md-4">
                                    <h3 class="myDesc line-bottom item-title">Details</h3>
                                    <ul class="details-list smallDesc">
                                        <li>
                                            <strong>Start date</strong>
                                            <p>{{$row->start_date}}</p>
                                        </li>
                                        <li>
                                            <strong>End Date</strong>
                                            <p>{{$row->end_date}}</p>
                                        </li>
                                        <li>
                                            <strong>Category</strong>
                                            <br/>
                                            @foreach($row->categories as $category)
                                                <span class="label label-primary">{{$category->name}}</span>
                                            @endforeach
                                        </li>
                                        <li>
                                            <strong>Place</strong>
                                            <br/>
                                            <p>{{@config("countries.$row->country")}}</p>
                                            <p>{{$row->city}}</p>
                                            <p>{{$row->address}}</p>
                                            <div><strong>Rate Us : </strong>
                                                <div data-id="{{$row->encyrptId()}}"
                                                     data-rate="{{$row->overall_rating}}" class="setRateHolder"></div>
                                            </div>
                                            <p hidden>
                                                <span id="ev-lat">{{$row->event_location_lat}}</span>
                                                <span id="ev-lng">{{$row->event_location_long}}</span>
                                            </p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-6 col-md-4">
                                    <h3 class="myDesc line-bottom item-title">Organizer</h3>
                                    <ul class="details-list smallDesc">
                                        <li>
                                            <strong>organized by</strong>
                                            <p>{{$row->organized_by}}</p>
                                        </li>
                                        <li>
                                            <strong>coordinator</strong>
                                            <p>{{$row->event_coordinator}}</p>
                                        </li>
                                        <li>
                                            <strong>website</strong>
                                            <p>{{@$row->event_website}}</p>
                                        </li>

                                    </ul>
                                </div>
                                <div class="col-6 col-md-4">
                                    <h3 class="myDesc line-bottom item-title">Insights</h3>
                                    <ul class="details-list smallDesc">
                                        <li>
                                            <strong>rating</strong>
                                            <br/>
                                            <label class=" label label-warning labels">{{$row->overall_rating}}
                                                <i class="glyphicon glyphicon-star"></i></label>
                                        </li>
                                        <li>
                                            <strong>event_serial</strong>
                                            <p>{{$row->event_serial}}</p>
                                        </li>
                                        <li>
                                            <strong>Tags</strong>
                                            <br/>
                                            @foreach($row->getTags() as $tag)
                                                <span class="label label-info">{{$tag}}</span>
                                            @endforeach
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 text-center">
                            @if($row->isBookedForUser(auther()::getUser()))
                                <button disabled class="btn-theme-reg smallDesc">Already booked</button>
                            @else
                                <button id="bookTrigger" data-item-id="{{$row->encyrptId()}}"
                                        class="btn-theme-reg smallDesc">book now
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="location" class="location-sections">
            <div id="map" style="height: 500px"></div>
        </section>
        <div class="empty-space"></div>
    </div>

@stop



@section("js")
    @parent
    <script src="js/pages/bookEvent.js"></script>
    <script>



        var map;

        function initMap() {
            var map;
            var place = {lat: parseFloat($("#ev-lat").text()), lng: parseFloat($("#ev-lng").text())};
            map = new google.maps.Map(document.getElementById('map'), {
                center: place,
                zoom: 11,
                scrollwheel: false,
                animation: google.maps.Animation.DROP,
                styles: [
                    {
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#f5f5f5"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#bdbdbd"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#757575"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#dadada"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#616161"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.line",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#e5e5e5"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#eeeeee"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#c9c9c9"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#9e9e9e"
                            }
                        ]
                    }
                ]
            });
            map.addListener('click', function () {
                map.set('scrollwheel', true);
            });
            map.addListener('mouseout', function () {
                map.set('scrollwheel', false);
            });
            var image = 'cdn/mark.png';
            var beachMarker = new google.maps.Marker({
                position: place,
                map: map,
                icon: image
            });
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{env("MAP_KEY")}}&callback=initMap">
    </script>


@stop