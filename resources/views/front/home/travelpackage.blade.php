@extends("front.layout")

@section("content")
    <section class="hero inner-hero">
        <div class="img-cont"
             style="background-size:cover;background:url('img/slider/01.jpg')  center center no-repeat;background-size: cover">
            <img src="img/inner-slider/01.jpg">
            <div class="overlay">
                <div class="dis-table">
                    <div class="cell-mid">
                        <div class="container">
                            <div class="hero-caption ">
                                <div class="smallHeading line-bottom">Packages List.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="inner-wrapper even-list-wrapper packages-wrapper">
        <section class="even-list-section">
            <div class="sec-header">
                <div class="ic-wrapper"><i class="icon-packages"></i></div>
                <div class="title-wrapper">
                    <h2 class="heading">Sierra travel<br>Packages.</h2>
                    <div class="smallDesc disclaimer">You've come to the right place!</div>
                </div>

            </div>
            <div class="container">
                <form>
                    <div class="section-wrapper">
                        <div class="row">
                            <div class="col-md-5 col-lg-3">
                                <div class="filter-wrapper">
                                    <h3 class="filter-title myDesc"><i class="icon-search3"></i> Advanced Search</h3>
                                    <div class="filter-form smallDesc">
                                        <div class="form-group">
                                            <label>type your Deals</label>
                                            {!! Form::text("q",request()->q,["class"=>"form-control"]) !!}
                                        </div>
                                        <div class="form-group">
                                            <label>filter by holiday days</label>
                                            {!! Form::select("days",getDays("asArray"),request()->days,["class"=>"form-control chosen-selectf p-form-controller"]) !!}

                                        </div>

                                        <div class="form-group">
                                            <label>Choose by Location</label>
                                            {!! Form::select("location",config("countries"),request()->location,["class"=>"form-control"]) !!}
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-theme-second-reversed smallDesc">Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-9">
                                <div class="head-filter">
                                    <div class="row">
                                        <div class="col-md">
                                            Sort By |
                                            {!! Form::select("sort",["rate"=>"rate","views"=>"views"],request()->sort,["class"=>"form-control p-form-controller"]) !!}

                                        </div>
                                        <div class="col-md text-md-right">
                                            Order |
                                            {!! Form::select("order",["asc"=>"A-Z","desc"=>"Z-A"],request()->order,["id"=>"exampleFormControlSelect1","class"=>"form-control p-form-controller"]) !!}
                                            show |
                                            {!! Form::select("limit",["1"=>"1","5"=>"5","15"=>"15","25"=>"25","50"=>"50",],request()->order?:"15",["id"=>"exampleFormControlSelect1","class"=>"form-control p-form-controller"]) !!}

                                        </div>
                                    </div>
                                </div>
                                <div class="travel-list">
                                    <div class="row">
                                        @foreach($packages as $package)
                                            @include("front.home.parts.travelPackage",["$package"=>$package,"smallView"=>true])
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <nav class="page-pagination">
                            {!! $packages->links() !!}
                        </nav>
                    </div>
                </form>
            </div>
        </section>

        <section class="popular-destinations">
            <div class="container">
                <h2 class="smallHeading lined-header line-bottom"><strong>Popular</strong> destinations</h2>
                <div class="items-shuffle">
                    <div class="filter-list row no-gutters d-none d-md-flex">
                        <div class="col-md">
                            <a class="link-item" data-filter="all">All</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".africa">Africa</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".asia">Asia</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".europe">Europe</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".north-america">North America</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".oceania">Oceania</a>
                        </div>
                        <div class="col-md">
                            <a class="link-item" data-filter=".south-america">South America</a>
                        </div>
                    </div>
                    <div class="filter-list d-md-none">
                        <div class="btn-group dropdown">
                            <button class="btn btn-theme-main dropdown-toggle" data-filter="all" type="button"
                                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">filter
                            </button>
                            <div class="dropdown-menu">
                                <div class="drop-link-item" data-filter="all">All</div>
                                <div class="drop-link-item" data-filter=".africa">Africa</div>
                                <div class="drop-link-item" data-filter=".asia">Asia</div>
                                <div class="drop-link-item" data-filter=".europe">Europe</div>
                                <div class="drop-link-item" data-filter=".north-america">North America</div>
                                <div class="drop-link-item" data-filter=".oceania">Oceania</div>
                                <div class="drop-link-item" data-filter=".south-america">South America</div>
                            </div>
                        </div>
                    </div>
                    <div class="items-cont row">
                        <div class="single-item col-md-4 mix africa">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/1.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Africa</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix asia">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/2.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Middle East</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix europe">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/3.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Middle East</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix north-america">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/4.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Australia</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix africa">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/5.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Europe</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix oceania">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/6.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Africa</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix south-america">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/7.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">south-america</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix africa">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/8.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">Africa</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="single-item col-md-4 mix south-america">
                            <a href="#">
                                <div class="img-cont">
                                    <img class="img-fluid" src="img/packages/popular/9.jpg">
                                    <div class="overlay">
                                        <div class="myDesc">south-america</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="best-offer-section">
            <div class="container">
                <h2 class="smallHeading lined-header line-bottom"><strong>Best</strong> offers</h2>
                <div class="row">
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/1.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/2.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/3.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/4.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/5.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a href="#">
                            <div class="item-card">
                                <img src="img/packages/offers/6.jpg" class="img-fluid">
                                <div class="myDesc"><span class="offer">15% OFF</span> Nights of London</div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop



@section("js")

    <script>
        $(".p-form-controller").change(function () {
            $(this)[0].form.submit();
        })
        $(document).ready(function () {
            var mixer = mixitup('.items-shuffle', {
                selectors: {
                    target: '.single-item'
                },
                animation: {
                    duration: 300
                }
            });
        });


    </script>

@stop












