@extends("front.layout")


@section("content")
    <section class="hero inner-hero">
        <div class="img-cont"
             style="background-size:cover;background:url('img/slider/01.jpg')  center center no-repeat;background-size: cover">
            <img src="img/inner-slider/01.jpg">
            <div class="overlay">
                <div class="dis-table">
                    <div class="cell-mid">
                        <div class="container">
                            <div class="hero-caption ">
                                <div class="smallHeading line-bottom">Events List.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <form>
        <div class="inner-wrapper even-list-wrapper">
            <section class="even-list-section">
                <div class="container">
                    <div class="tag-filter">
                        <label>Search by Tags</label>
                        {!! Form::text("tags",request()->tags,['class'=>'form-control select-tags']) !!}
                    </div>
                    <div class="section-wrapper">
                        <div class="row">
                            <div class="col-md-5 col-lg-3">
                                <div class="filter-wrapper">
                                    <h3 class="filter-title myDesc"><i class="icon-search3"></i> Advanced Search</h3>
                                    <div class="filter-form smallDesc">
                                        {{--{!! Form::hidden("order",request()->order) !!}--}}
                                        {{--{!! Form::hidden("tags",request()->tags) !!}--}}

                                        <div class="form-group">
                                            <label>type a word</label>
                                            {!! Form::text("q",request()->q,[ "class"=>"form-control"]) !!}
                                        </div>
                                        <div class="form-group">
                                            <label>Choose your Category</label>
                                            {!! Form::select("categories[]",$categoriesList,null,[
                                            "title"=>"conferences","multiple"=>true,"type"=>"text", "class"=>"form-control
                                            chosen-select"]) !!}
                                        </div>


                                        <div class="form-group">
                                            <label>Type a place</label>
                                            {!! Form::select("country",$countries,request()->country,[ "class"=>"form-control"]) !!}
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-theme-second-reversed smallDesc">Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 col-lg-9">
                                <div class="head-filter">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h3 class="myDesc line-bottom">Upcoming Events</h3>
                                        </div>
                                        <div class="col-md-8 text-right">
                                            {!!
                                            Form::select("date",["desc"=>"newer","asc"=>"older"],request()->date,["placeholder"=>"order
                                            by date","onchange"=>"this.form.submit()","class"=>"form-control smallDesc"])
                                            !!}
                                            {!! Form::select("rate",["desc"=>"higher rate","asc"=>"lower
                                            rate"],request()->rate,["placeholder"=>"order by
                                            rate","onchange"=>"this.form.submit()","class"=>"form-control smallDesc"]) !!}
                                            {!!
                                            Form::select("views",["most"=>"Most Viewed","latest"=>"Latest Created"],request()->views,["placeholder"=>"order
                                            by views or latest","onchange"=>"this.form.submit()","class"=>"form-control
                                            smallDesc"]) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="event-list ">
                                    @each("front.home.parts.singelEvent",$eventsList,"event")
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <div class="text-center d-inline-block">
                            {!! $eventsList->links() !!}
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </form>
@stop
@section("js")

    {{--@selude("front.home.parts.js")--}}

    <script>

        $('.select-tags').each(function () {
            $(this).tagsinput({tagClass: 'label label-primary'});
        });

        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }

        //        $("#orderSelect").change(function () {
        //                window.location.href = updateQueryStringParameter(window.location.href, "order", $(this).val());
        //            }
        //        )
        $("body").on("click", ".remove", function (e) {
            window.location.href = updateQueryStringParameter(window.location.href, "tags", $("[name=tags]").val());
        })
        $("body").on("keyup", ".bootstrap-tagsinput input", function (e) {
                if (e.keyCode == 13)
                    window.location.href = updateQueryStringParameter(window.location.href, "tags", $("[name=tags]").val());
            }
        )


    </script>
@stop
