@extends("front.layout")


@section("content")

    <div class="inner-wrapper  packages-wrapper">
        <section class=" packages-details-section">
            <div class="container">

                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="text-center gallery-sec">
                            <div class="secondary-tabs">
                                <ul class="tab-list">
                                    <li class="list-item active">
                                        <a href="#" data-target="#item_gallery"
                                           class="tab_trigger link-item">Gallery</a>
                                    </li>
                                    <li class="list-item">
                                        <a href="#" data-target="#item_videos"
                                           class="tab_trigger link-item">Video</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div id="item_gallery" class="img-content tab-item active">
                                        <div class="img-cont">
                                            <div class="owl-single owl-carousel owl-theme">
                                                @foreach($row->images() as $img)
                                                    <div class="single-item">
                                                        <img src="upload/{{$img}}" class="img-fluid w-100">
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                    <div id="item_videos" class="video-content tab-item">
                                        <div class="img-cont">
                                            <div class="owl-single owl-carousel owl-theme">
                                                <div class="single-item">
                                                    <a href="{{$row->video}}"
                                                       data-fancybox="">
                                                        <img src="upload/{{$row->image}}" class="img-fluid w-100">
                                                        <div class="overlay">
                                                            <i class="icon-youtube"></i>
                                                        </div>
                                                    </a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="details-wrapper">
                            <div class="packages-info">
                                <div class="row">
                                    <div class="col-md">
                                        <h2>{{$row->name}}</h2>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md">
                                        <div class="single-row">
                                            <div class="d-inline-block">
                                                <i class="icon-info"></i>
                                            </div>
                                            <div class="d-inline-block item-desc">
                                                Tour type: <span>{{$row->tour_type}}</span>
                                            </div>
                                        </div>
                                        <div class="single-row">
                                            <div class="d-inline-block">
                                                <i class="icon-calendar"></i>
                                            </div>
                                            <div class="d-inline-block item-desc">
                                                Duration: <span>{{$row->duration_in_days}}</span>
                                            </div>
                                        </div>

                                        <div class="single-row">
                                            <div class="d-inline-block">
                                                <i class="icon-location"></i>
                                            </div>
                                            <div class="d-inline-block item-desc">
                                                location: <span>{{config("countries.$row->destination")}}</span>
                                            </div>
                                        </div>
                                        <div class="single-row">
                                            <div class="d-inline-block">
                                                <i class="icon-star-full"></i>
                                            </div>
                                            <div class="d-inline-block item-desc">
                                                Rate:
                                            </div>
                                            <div data-rate="{{$row->rate}}" class="rate-wrapper rate_generate">
                                                <span class="rate-item icon-star-empty"></span>
                                                <span class="rate-item icon-star-empty"></span>
                                                <span class="rate-item icon-star-empty"></span>
                                                <span class="rate-item icon-star-empty"></span>
                                                <span class="rate-item icon-star-empty"></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md">
                                        <a href="#" class="btn-link item-viewer">customize your package</a>
                                        <div class="item-view">
                                            <form data-parsley-validate="" action="create-custom-package" method="post">
                                                {!! csrf_field() !!}
                                                <div class="check-date form-group">
                                                    <label>Departure Date</label>
                                                    <div class="input-group date">
                                                        <input name="departure_date" type="text"
                                                               class="form-control datepicker"
                                                               value="{{date("m-d-Y")}}"
                                                               readonly>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::select("hotel_id",$hotels,request()->hotel_id,["placeholder"=>"please select hotel", "class"=>"form-control", "required"=>true]) !!}
                                                </div>
                                                <div class="form-group">
                                                    {!! Form::select("airlines",$airlines,request()->airlines,["placeholder"=>"please select airlines", "class"=>"form-control", "required"=>true]) !!}
                                                </div>
                                                <div class="p-count">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label>Adults</label>
                                                                {!! Form::number("adults",1,["class"=>"form-control","min"=>1]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label>Children</label>
                                                                {!! Form::number("Children",0,["class"=>"form-control"]) !!}
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="form-group">
                                                                <label>Infant</label>
                                                                {!! Form::number("Infant",0,["class"=>"form-control"]) !!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center btn-cont">
                                                    <button type="submit" class="btn-theme-second">Submit</button>
                                                    <button type="button" class="btn-theme-main close-sec">cancel
                                                    </button>
                                                </div>
                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="ev-desc">
                                <h2 class="myDesc line-bottom">Overview</h2>
                                <div class="smallDesc">
                                    {{$row->description}}

                                </div>
                            </div>

                            <div class="user-rate">
                                <h2 class="myDesc line-bottom">Rate Us</h2>
                                <div data-rate-value="{{$row->getRateForUser(\Jlib\Auth\Auther::getUser())}}"
                                     class="rate-wrapper user_rate">
                                    <span class="rate-item icon-star-empty"></span>
                                    <span class="rate-item icon-star-empty"></span>
                                    <span class="rate-item icon-star-empty"></span>
                                    <span class="rate-item icon-star-empty"></span>
                                    <span class="rate-item icon-star-empty"></span>
                                </div>
                            </div>

                            {{--<div class="tour-program-section">--}}
                                {{--<h2 class="myDesc line-bottom">Tour Program</h2>--}}
                                {{--<div id="accordion" role="tablist">--}}
                                    {{--<div class="card">--}}
                                        {{--<div class="card-header" role="tab" id="headingOne">--}}
                                            {{--<h5 class="mb-0">--}}
                                                {{--<a data-toggle="collapse" href="#transportation" aria-expanded="true"--}}
                                                   {{--aria-controls="transportation">--}}
                                                    {{--Transportation--}}
                                                    {{--<i class="icon-chevron-thin-up"></i>--}}
                                                {{--</a>--}}
                                            {{--</h5>--}}
                                        {{--</div>--}}

                                        {{--<div id="transportation" class="collapse show" role="tabpanel"--}}
                                             {{--aria-labelledby="headingOne" data-parent="#accordion">--}}
                                            {{--<div class="card-body">--}}
                                                {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus--}}
                                                {{--terry--}}
                                                {{--richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                                                {{--dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf--}}
                                                {{--moon--}}
                                                {{--tempor, sunt aliqua put a bird on it squid single-origin coffee nulla--}}
                                                {{--assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer--}}
                                                {{--labore--}}
                                                {{--wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur--}}
                                                {{--butcher--}}
                                                {{--vice lomo. Leggings occaecat craft beer farm-to-table, raw denim--}}
                                                {{--aesthetic--}}
                                                {{--synth nesciunt you probably haven't heard of them accusamus labore--}}
                                                {{--sustainable VHS.--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="card">--}}
                                        {{--<div class="card-header" role="tab" id="headingTwo">--}}
                                            {{--<h5 class="mb-0">--}}
                                                {{--<a class="collapsed" data-toggle="collapse" href="#collapseTwo"--}}
                                                   {{--aria-expanded="false" aria-controls="collapseTwo">--}}
                                                    {{--media--}}
                                                    {{--<i class="icon-chevron-thin-up"></i>--}}
                                                {{--</a>--}}
                                            {{--</h5>--}}
                                        {{--</div>--}}
                                        {{--<div id="collapseTwo" class="collapse" role="tabpanel"--}}
                                             {{--aria-labelledby="headingTwo"--}}
                                             {{--data-parent="#accordion">--}}
                                            {{--<div class="card-body">--}}
                                                {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus--}}
                                                {{--terry--}}
                                                {{--richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                                                {{--dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf--}}
                                                {{--moon--}}
                                                {{--tempor, sunt aliqua put a bird on it squid single-origin coffee nulla--}}
                                                {{--assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer--}}
                                                {{--labore--}}
                                                {{--wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur--}}
                                                {{--butcher--}}
                                                {{--vice lomo. Leggings occaecat craft beer farm-to-table, raw denim--}}
                                                {{--aesthetic--}}
                                                {{--synth nesciunt you probably haven't heard of them accusamus labore--}}
                                                {{--sustainable VHS.--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                    {{--<div class="card">--}}
                                        {{--<div class="card-header" role="tab" id="headingThree">--}}
                                            {{--<h5 class="mb-0">--}}
                                                {{--<a class="collapsed" data-toggle="collapse" href="#collapseThree"--}}
                                                   {{--aria-expanded="false" aria-controls="collapseThree">--}}
                                                    {{--Accommodation--}}
                                                    {{--<i class="icon-chevron-thin-up"></i>--}}
                                                {{--</a>--}}
                                            {{--</h5>--}}
                                        {{--</div>--}}
                                        {{--<div id="collapseThree" class="collapse" role="tabpanel"--}}
                                             {{--aria-labelledby="headingThree" data-parent="#accordion">--}}
                                            {{--<div class="card-body">--}}
                                                {{--Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus--}}
                                                {{--terry--}}
                                                {{--richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard--}}
                                                {{--dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf--}}
                                                {{--moon--}}
                                                {{--tempor, sunt aliqua put a bird on it squid single-origin coffee nulla--}}
                                                {{--assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer--}}
                                                {{--labore--}}
                                                {{--wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur--}}
                                                {{--butcher--}}
                                                {{--vice lomo. Leggings occaecat craft beer farm-to-table, raw denim--}}
                                                {{--aesthetic--}}
                                                {{--synth nesciunt you probably haven't heard of them accusamus labore--}}
                                                {{--sustainable VHS.--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="share-section">--}}
                                {{--<div class="ic-cont ">--}}
                                    {{--<div class="d-inline-block mr-3">share</div>--}}
                                    {{--<a href="#" target="_blank">--}}
                                        {{--<div class="myico facebook-wrapper">--}}
                                            {{--<i class="icon-facebook" aria-hidden="true"></i>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" target="_blank">--}}
                                        {{--<div class="myico twitter-wrapper">--}}
                                            {{--<i class="icon-twitter" aria-hidden="true"></i>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" target="_blank">--}}
                                        {{--<div class="myico linkedin-wrapper">--}}
                                            {{--<i class="icon-instagram2" aria-hidden="true"></i>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                    {{--<a href="#" target="_blank">--}}
                                        {{--<div class="myico google-plus-wrapper">--}}
                                            {{--<i class="icon-google-plus" aria-hidden="true"></i>--}}
                                        {{--</div>--}}
                                    {{--</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>

                        <div class="text-center">
                            <div class="alert alert-dismissible fade show my-msg" role="alert" style="display: none">
                                <span class="in-msg"></span>
                                <button type="button" class="close close-msg" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <br/>
                            @if($row->bookedForUser(\Jlib\Auth\Auther::getUser()))
                                <button class="btn-theme-reg smallDesc">
                                    already booked
                                </button>
                            @else
                                <button class="btn-theme-reg smallDesc trigger-msg" data-id="{{$row->encyrptId()}}">
                                    Book Now
                                </button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="location" class="location-sections">
            <div id="map" style="height: 500px"></div>
        </section>
        <div class="empty-space"></div>
    </div>

@stop

@section("js")
    <script src="js/googleMapThem.js"></script>
    <script>

        (function () {



            //msg alert
            $('.trigger-msg').click(function (e) {
                e.preventDefault();
                Events.emit("bookTravelPackages", $(this))
            });


            let bookPackage = function (ele) {
                let msg;
                let showMsg = function (msg) {
                    $(".in-msg").html(msg)
                    $(".my-msg").fadeIn();
                    setTimeout(function () {
                        $(".my-msg").fadeOut();
                    }, 2000);
                }
                return {
                    init: function () {
                        ele.prop("disabled", true).text("please wait")
                    },
                    isSuccess: function () {
                        swal("Good job!", "Thanks for booking your event accommodation with SGS our customer experience team will contact you for your hotel reservation.", "success");
                        msg = `<strong>thank you</strong> Your submit has been received and we will call you soon`;
                        showMsg(msg);
                        ele.remove();
//                        ele.text("done!");
                    },
                    isFails: function () {
                        msg = `<strong>Oops!</strong> some thing wrong has occurred please book again or contact with site administrator`;
                        showMsg(msg);
                        ele.prop("disabled", false).text("Fail, try again")
                    }
                }
            }
            Events.on("bookTravelPackages", function (ele) {
                let Bok = new bookPackage(ele);
                isLogin(function (isLogin) {
                    Bok.init();
                    try {
                        $.ajax({
                            method: "POST",
                            url: baseUrl() + "/book-package",
                            data: {_token: _token(), id: ele.data("id")},
                            success: function (res) {
                                if (res.status) {
                                    Bok.isSuccess();
                                } else {
                                    Bok.isFails();
                                }
                            },
                            error: function () {
                                Bok.isFails();
                            }
                        });
                    } catch (e) {
                        Bok.isFails();
                    }

                });
            })

            Events.on("updateRate", function (ele) {
                var rateCount = ele.index() + 1;
                isLogin(function (isLogin) {
                    $.post("/rate-package", {
                            _token: "{{csrf_token()}}",
                            "rate": rateCount,
                            package_id: "{{$row->encyrptId()}}"
                        },
                        function (res) {
                            if (res.result)
                                ele.parent().data('rate-value', rateCount);
                        }
                    );
                })
            })
            $("#bookTrigger").click(function () {
                let btn = $(this);
                btn.prop("disabled", true)
                isLogin(function (isLogin) {
                    if (isLogin) {
                        $.post("{{url("/book-event")}}", {
                            _token: "{{csrf_token()}}",
                            event_id: $(this).data("item-id")
                        }, function (res) {
                            if (res.status) {
                                btn.text("booked successfully")
                            } else {
                                btn.prop("disabled", false)
                            }
                            ;
                        });
                    } else {

                    }
                });
            });

            $(document).ready(function () {
                $('.tab_trigger').on('click', function (e) {
                    e.preventDefault();
                    var tabItem = $($(this).data('target'));
                    $(this).parent().addClass('active').siblings().removeClass('active');
                    tabItem.fadeIn().addClass('active').siblings().hide().removeClass('active');
                });

                $('.datepicker').datepicker({
                    autoclose: true
                });
            });
            $('.item-viewer').click(function (e) {
                e.preventDefault();
                $(this).hide().siblings('.item-view').fadeIn();
            });
            $('.close-sec').click(function (e) {
                e.preventDefault();
                $(this).parents('.item-view').hide().siblings('.item-viewer').fadeIn();
            });


        })();

        function initMap() {
            var map;
            var place = {lat: 30.1184926, lng: 31.236824};
            map = new google.maps.Map(document.getElementById('map'), {
                center: place,
                zoom: 11,
                scrollwheel: false,
                animation: google.maps.Animation.DROP,
                styles: GoogleMapTheme()
            });
            map.addListener('click', function () {
                map.set('scrollwheel', true);
            });
            map.addListener('mouseout', function () {
                map.set('scrollwheel', false);
            });
            var image = 'cdn/mark.png';
            var beachMarker = new google.maps.Marker({
                position: place,
                map: map,
                icon: image
            });
        };
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{env("MAP_KEY")}}&callback=initMap">
    </script>


@stop




