@extends("front.layout")


@section("content")
    <section class="hero inner-hero">
        <div class="img-cont"
             style="background-size:cover;background:url('img/slider/01.jpg')  center center no-repeat;background-size: cover">
            <img src="img/inner-slider/01.jpg">
            <div class="overlay">
                <div class="dis-table">
                    <div class="cell-mid">
                        <div class="container">
                            <div class="hero-caption ">
                                <div class="smallHeading line-bottom">Plans List.</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="plans-wrapper">
        <section class="price-section">
            <div class="container">
                <div class="sec-header">
                    <div class="title-wrapper">
                        <h2 class="heading">Find the right plan for<br>your Business.</h2>
                        <div class="smallDesc disclaimer">Relax. You’re with us! We make it simple.</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 mx-auto">
                        <div class="price-table">
                            <div class="row">
                                <?php $first = true?>
                                @foreach($rows as $plan)
                                    <div data-target="#plan_{{$plan->id}}" class="col-md-4 single-offer {{($first)?"active":""}}">
                                        <div class="item-wrapper">
                                            <h2 class="item-title myDesc">{{$plan->name}}</h2>
                                            <div class="price-cont">
                                                <span class="currency">$</span>
                                                <span class="price-result">{{$plan->price}}</span>
                                            </div>
                                            <div class="item-desc">
                                                <ul class="desc-list smallDesc">
                                                    {!! $plan->description !!}

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <?php $first = false?>
                                @endforeach
                            </div>
                        </div>
                        <div id="details_wrapper" class="details-wrapper">
                            <?php $first = true?>
                            @foreach($rows as $plan)
                                <div id="plan_{{$plan->id}}" class="offer-detail {{($first)?"active":""}}">
                                    <h4>{{$plan->name}}</h4>
                                    <div class="smallDesc">

                                        <p>
                                       {{$plan->discription}}
                                        </p>
                                        <p>Price: ${{$plan->price}}</p>
                                        <p>Start date: {{$plan->start_date}}</p>
                                        <p>End date: {{$plan->end_date}}</p>
                                        <p>Package type: {{$plan->package_type}}</p>
                                        <p>Discount: {{$plan->discount}}%</p>
                                    </div>
                                    <div class="text-center mt-3">
                                        <a href="{{url("pay-plan/".$plan->encyrptId())}}" class="btn-pay smallDesc">pay now</a>
                                    </div>
                                </div>
                                <?php $first = false?>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>

@stop


@section("js")

    <script>
        $(document).ready(function () {
            $('.single-offer').click(function () {
                $($(this).data('target')).fadeIn().siblings().removeClass('active').hide();
                $(this).addClass('active').siblings().removeClass('active');
                $('html, body').animate({
                    scrollTop: $('#details_wrapper').offset().top - 120
                }, 600);
            })
        });
    </script>



@stop
