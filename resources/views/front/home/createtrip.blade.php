@extends("front.layout")


@section("content")
    <div class="trip-wrapper">
        <section class="creat-trip-section">
            <div class="container">
                <div class="form-wrapper">
                    <div class="section-label"><img src="cdn/fl-icon.png">Flights + Hotels</div>
                    <form id="tripForm" class="trip-form" data-parsley-validate="" method="post">
                        {!! csrf_field() !!}
                        <div id="groupsWrapper">

                        </div>
                        <div class="text-center submit-btn">
                            <button type="button" class=" btn-more btn-block duplicate_item">
                                ADD MORE <i class="icon-plus-circle"></i></button>
                        </div>
                        <div class="text-center submit-btn">
                            <button type="submit" class="btn btn-theme-second smallDesc getData">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>

        <section class="tips">
            <div class="container">
                <h2 class="myDesc line-bottom">How it works</h2>
                <div class="smallDesc">
                    <p>
                        Pick your destinations from the interactive world map. Don’t forget to include the cities you’re
                        DEPARTING FROM and also RETURNING TO or ENDING IN.
                    </p>
                    <p>
                        If necessary, rearrange the destination sequence to see if different routes or itineraries lower
                        the
                        cost. Add or remove stops.
                    </p>
                    <p>
                        If you want to travel overland on your own (i.e. by train, bus, boat, camel, or bike), click on
                        the
                        blue plane icon between the cities you want to overland -the plane icon will turn into a train
                        symbol.
                    </p>
                </div>
            </div>
        </section>
    </div>
@stop

@section("js")
    @parent
    <script src="js/create-trip/createTrip.js">

    </script>
@stop