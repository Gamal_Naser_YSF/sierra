<?php
return [
    "site map" => "site map",
    "home" => "home",
    "about us" => "about us",
    "conferences" => "conferences",
    "create trip" => "create trip",

    "packages" => "packages",
    "deals" => "deals",
    "offers" => "offers",
    "europe" => "europe",
    "blog" => "blog",

    "delivery" => "delivery",
    "catalog" => "catalog",

    "mission" => "mission",
    "vision" => "vision",
    "strategy" => "strategy",
    "updates" => "updates",
];



